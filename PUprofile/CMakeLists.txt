# SPDX-License-Identifier: GPLv3-or-later
#
# SPDX-FileCopyrightText: Louis Moureaux <louis.moureaux@cern.ch>

core_add_executable(applyBinnedPUprofCorrection LIBRARIES CommonTools Objects)
core_add_executable(applyPUprofCorrection LIBRARIES CommonTools Objects)
core_add_executable(getPUprofile LIBRARIES Objects)
install(PROGRAMS scripts/getDataDiffPUprofile.zsh
        DESTINATION "${CMAKE_INSTALL_BINDIR}"
        RENAME getDataDiffPUprofile)
install(PROGRAMS scripts/getDataPUprofile.zsh
        DESTINATION "${CMAKE_INSTALL_BINDIR}"
        RENAME getDataPUprofile)

#include <cmath>
#include <filesystem>
#include <iostream>
#include <vector>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Format.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/binnings.h"
#include "Core/CommonTools/interface/DASOptions.h"

#include <TH2.h>
#include <TString.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/info_parser.hpp>

#include "Core/CommonTools/interface/ControlPlots.h"

#include "PUcorrection.h"

#include <darwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::PUprofile {

////////////////////////////////////////////////////////////////////////////////
/// Apply the PU profile reweighting to the simulation.
void applyBinnedPUprofCorrection
            (const vector<fs::path> inputs, //!< input ROOT files (n-tuples)
             const fs::path output, //!< name of output root file containing the histograms
             const pt::ptree& config, //!< config file from `DT::Options`
             const int steering, //!< steering parameters from `DT::Options`
             const DT::Slice slice = {1,0} //!< slices for running
            )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto [fOut, tOut] = flow.GetOutput(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    if (!isMC) BOOST_THROW_EXCEPTION( DE::BadInput("Only MC may be used as input.",
                                      make_unique<TFile>(inputs.front().c_str() )) );

    auto genEvt = flow.GetBranchReadOnly<GenEvent>("genEvent");
    auto recEvt = flow.GetBranchReadOnly<RecEvent>("recEvent");
    auto pu     = flow.GetBranchReadOnly<PileUp  >("pileup"  );
    auto genJets = flow.GetBranchReadWrite<vector<GenJet>>("genJets");
    auto recJets = flow.GetBranchReadWrite<vector<RecJet>>("recJets");

    ControlPlots::isMC = isMC;
    ControlPlots raw("raw");
    bool applySyst = steering & DT::syst;
    vector<ControlPlots> calib { ControlPlots("nominal") };
    if (applySyst) {
        metainfo.Set<string>("variations", RecEvent::WeightVar, "PU" + SysUp);
        metainfo.Set<string>("variations", RecEvent::WeightVar, "PU" + SysDown);

        calib.push_back(ControlPlots("PU" + SysUp));
        calib.push_back(ControlPlots("PU" + SysDown));
    }

    auto turnon_file = config.get<fs::path>("corrections.normalisation.turnons");
    auto DataProf = config.get<fs::path>("corrections.PUprofile.Data");
    auto MCprof = config.get<fs::path>("corrections.PUprofile.MC");
    for (auto& file: {turnon_file, DataProf, MCprof})
        if (!fs::exists(file))
            BOOST_THROW_EXCEPTION( fs::filesystem_error("Input file could not be found",
                                file, make_error_code(errc::no_such_file_or_directory)));

    pt::ptree turnons;
    pt::read_info(turnon_file.c_str(), turnons);

    auto fMCprof = TFile::Open(MCprof.c_str(), "READ"),
         fDataProf = TFile::Open(DataProf.c_str(), "READ");
    map<float, PUprofile::Correction> corrections;
    auto maxWeight = config.get<float>("corrections.PUprofile.maxWeight");
    for (auto& turnon: turnons) {
        auto threshold = atoi(turnon.first.c_str());
        auto trigger = [threshold](const char * variation, bool global) {
            return global ? Form("%s/intPU"            , variation)
                          : Form("%s/HLT_PFJet%d/intPU", variation, threshold);
        };
        auto pt = turnon.second.get_value<float>();
        PUprofile::Correction correction(fMCprof, fDataProf, trigger, maxWeight);
        corrections.insert( {pt, correction} );
    }
    const bool hasZB = fDataProf->GetDirectory("nominal/ZB") && dynamic_cast<TH1*>(fDataProf->Get("nominal/ZB/intPU"))->Integral() > 0;
    assert(!hasZB);
    if (hasZB) {
        auto trigger = [](const char * variation, bool global) {
            return global ? Form("%s/intPU"   , variation)
                          : Form("%s/ZB/intPU", variation);
        };
        PUprofile::Correction correction(fMCprof, fDataProf, trigger, maxWeight);
        corrections.insert( {0, correction} );
    }
    fDataProf->Close();
    fMCprof->Close();

    metainfo.Set<fs::path>("corrections", "normalisation", "turnons", turnon_file);
    metainfo.Set<fs::path>("corrections", "PUprofile", "Data", DataProf);
    metainfo.Set<fs::path>("corrections", "PUprofile", "MC", MCprof);
    metainfo.Set<float>("corrections", "PUprofile", "maxWeight", maxWeight);

    PileUp::r3.SetSeed(metainfo.Seed<756347956>(slice));
    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        int intpu = pu->intpu;
        auto weight_raw = recEvt->weights.front(); // get current nominal weight before correction

        // 1) find leading jet pt in tracker acceptance (if it exists)
        auto pt0 = corrections.begin()->first; // if ZB, then by default ZB, otherwise it will be trig40
        auto InTk = [](const auto& jet) { return jet.AbsRap() < 3.0; };
        auto leadRecJetInTk = find_if(recJets->begin(), recJets->end(), InTk);
        if (leadRecJetInTk != recJets->end())
            pt0 = leadRecJetInTk->CorrPt();
        else if (!hasZB) {
            auto leadGenJetInTk = find_if(genJets->begin(), genJets->end(), InTk);
            if (leadGenJetInTk != genJets->end())
                pt0 = leadGenJetInTk->p4.Pt();
        }

        auto it = find_if(corrections.rbegin(), prev(corrections.rend()),
                [&pt0](const auto& th_corr) { return pt0 > th_corr.first; });
        const auto& correction = it->second;
        cout << pt0 << ' ' << it->first << endl;

        // loop over existing weights and correct them with the nominal value
        auto nominal_correction = correction(intpu, '0');
        recEvt->weights *= nominal_correction;

        auto weight_nominal = recEvt->weights.front();

        // filling at gen level
        raw          (*genJets, genEvt->weights.front());
        calib.front()(*genJets, genEvt->weights.front());

        // filling at rec level
        raw          (*recJets, genEvt->weights.front() * weight_raw    );
        calib.front()(*recJets, genEvt->weights.front() * weight_nominal);

        // systematics
        if (applySyst) {
            auto weight_upper   = weight_raw*correction(intpu, '+'),
                 weight_lower   = weight_raw*correction(intpu, '-');
            recEvt->weights.push_back(Weight{weight_upper});
            recEvt->weights.push_back(Weight{weight_lower});
            calib.at(1)(*genJets, genEvt->weights.front());
            calib.at(2)(*genJets, genEvt->weights.front());
            calib.at(1)(*recJets, genEvt->weights.front() * weight_upper);
            calib.at(2)(*recJets, genEvt->weights.front() * weight_lower);
        }

        // filling tree
        if (steering & DT::fill) tOut->Fill();
    }

    auto d = fOut->mkdir("corrections");
    for (auto& correction: corrections) {
        d->cd();
        int threshold = correction.first;
        auto it = turnons.begin();
        while (it != turnons.end()) {
            if (it->second.get_value<int>() == threshold) break;
            ++it;
        }
        //auto it = find_if(turnons.begin(), turnons.end(), [threshold](auto& turnon)
        //                    { return threshold == turnon.second.get_value<int>(); });
        if (it != turnons.end()) {
            auto dd = d->mkdir(Form("HLT_PFJet%d", atoi(it->first.c_str())));
            bool reset = slice.second > 0;
            correction.second.Write(dd, reset);
        }
        else { // assuming ZB
            auto dd = d->mkdir("ZB");
            bool reset = slice.second > 0;
            correction.second.Write(dd, reset);
        }
    }

    raw.Write(fOut);
    for (auto& c: calib)
        c.Write(fOut);

    metainfo.Set<bool>("git", "complete", true);

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of DAS::PUprofile namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options(
                            "Correct PU profile (by trigger).",
                            DT::config | DT::split | DT::Friend | DT::syst);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("MC", "corrections.PUprofile.MC", "profile obtained with `getPUprofile`")
               .arg<fs::path>("Data", "corrections.PUprofile.Data", "profile obtained with `getPUprofile`")
               .arg<fs::path>("turnons", "corrections.normalisation.turnons", "path to 2-column file with trigger thresholds")
               .arg<float>("maxWeight", "corrections.PUprofile.maxWeight", "highest allowed value for the weight");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::PUprofile::applyBinnedPUprofCorrection(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif

#include <cassert>
#include <cstdlib>

#include <filesystem>
#include <iostream>
#include <vector>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Lepton.h"
#include "Core/Objects/interface/Photon.h"

#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/CommonTools/interface/binnings.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <TFile.h>
#include <TString.h>

#include "Math/VectorUtil.h"

#include <darwin.h>
#include "Core/Objects/interface/Di.h"

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Muon {

////////////////////////////////////////////////////////////////////////////////
/// Obtain angular separation plots and fake/miss rates of photons/muons
void getZmmgControlPlots(
    const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
    const fs::path& output,         //!< output ROOT file (n-tuple)
    const int steering,             //!< parameters obtained from explicit options
    const DT::Slice slice = {1, 0}  //!< number and index of slice
)
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto [fOut,tOut] = flow.GetOutput(output);

    DT::MetaInfo metainfo(tOut);
    auto isMC = metainfo.Get<bool>("flags", "isMC");

    auto recEvt = flow.GetBranchReadOnly<RecEvent>("recEvent");
    auto genEvt = isMC ? flow.GetBranchReadOnly<GenEvent>("genEvent") : nullptr;

    auto recMuons = flow.GetBranchReadOnly<vector<RecMuon>>("recMuons");
    auto genMuons = isMC ? flow.GetBranchReadOnly<vector<GenMuon>>("genMuons") : nullptr;
    auto recPhotons = flow.GetBranchReadOnly<vector<RecPhoton>>("recPhotons");
    auto genPhotons = isMC ? flow.GetBranchReadOnly<vector<GenPhoton>>("genPhotons") : nullptr;

    array h_deltaR {
        make_unique<TH1F>("deltaR0", "", 100, 0, 5.0),
            make_unique<TH1F>("deltaR1", "", 100, 0, 5.0)
    };
    auto h_deltaRmin = make_unique<TH1F>("deltaRmin", "", 100, 0, 5.0);

    auto h_fake_mu = make_unique<TH1F>("fake_mu", "", 2, -0.5, 1.5);
    auto h_miss_mu = make_unique<TH1F>("miss_mu", "", 2, -0.5, 1.5);
    auto h_fake_ph = make_unique<TH1F>("fake_ph", "", 2, -0.5, 1.5);
    auto h_miss_ph = make_unique<TH1F>("miss_ph", "", 2, -0.5, 1.5);
    auto h_fake_mmg = make_unique<TH1F>("diff_fake_mmg", "", 50, 0, 100);
    auto h_miss_mmg = make_unique<TH1F>("diff_miss_mmg", "", 100, 0, 200);

    array h_gen_reco_mu_dR {
        make_unique<TH1F>("gen_reco_mu0_dR", "", 50, 0, 0.5),
            make_unique<TH1F>("gen_reco_mu1_dR", "", 50, 0, 0.5)
    };
    auto h_gen_reco_ph_dR = make_unique<TH1F>("gen_reco_ph_dR", "", 50, 0, 0.5);

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[maybe_unused]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        // muon selection
        if (recMuons->size() < 2) continue;
        if (recMuons->at(0).CorrPt() < 20) continue;
        if (recMuons->at(1).CorrPt() < 15) continue;

        // photon selection
        if (recPhotons->empty()) continue;
	auto rph = recPhotons->front().CorrP4();
        if (rph.Pt() < 20) continue;

        // \todo Use Di<RecMuon,RecMuon>
        // \todo Move phase space selection here
        auto recdimuon = recMuons->at(0) + recMuons->at(1);
        if (recdimuon.CorrP4().M() >= 76) continue;

        double w_Z = recEvt->weights.front() * recdimuon.Weight();
        if (isMC)
            w_Z *= genEvt->weights.front();

        bool match_mmg = true; // will use logical ands to check the matching to individual objects
        if (isMC) {
            const float maxDr = 0.1;

            if (genMuons->size() > 1) {
                for (size_t irec = 0; irec < 2; irec++) {
                // the leading and subleading muons may swap due to resolution effects
                float dR0 = DeltaR(recMuons->at(irec).CorrP4(), genMuons->at(0).CorrP4()),
                      dR1 = DeltaR(recMuons->at(irec).CorrP4(), genMuons->at(1).CorrP4());
                size_t igen = dR0 < dR1 ? 0 : 1;
                float dR = min(dR0, dR1);

                bool match_mu = dR < maxDr;
                double rW = recMuons->at(irec).weights.front(),
                       gW = genMuons->at(igen).weights.front();
                h_fake_mu->Fill(match_mu, rW);
                h_miss_mu->Fill(match_mu, gW);
                match_mmg &= match_mu;

                if (match_mu)
                    h_gen_reco_mu_dR[igen]->Fill(dR, gW*rW);
                }
            }
            else match_mmg = false;

            // \todo Is the leading photon the right photon? (or should we look for the photon closest to one of the two leading muons?)
            if (recPhotons->size() > 0) {
                bool match_ph = DeltaR(recPhotons->front().CorrP4(), genPhotons->front().CorrP4()) < maxDr;
                h_fake_ph->Fill(match_ph, recPhotons->front().weights.front());
                h_miss_ph->Fill(match_ph, genPhotons->front().weights.front());
                match_mmg &= match_ph;
            }
            else match_mmg = false;
        }

        // \todo Use Di<Di<RecMuon,RecMuon>, RecPhoton>
        double w_ph = recPhotons->front().weights.front();

        float dR0 = DeltaR(rph, recMuons->at(0).CorrP4()),
              dR1 = DeltaR(rph, recMuons->at(1).CorrP4());
        h_deltaR[0]->Fill(dR0, w_Z * w_ph);
        h_deltaR[1]->Fill(dR1, w_Z * w_ph);
        h_deltaRmin->Fill(min(dR0, dR1), w_Z * w_ph);

        if (isMC) {

            if (match_mmg) {
                FourVector gph = genPhotons->front().CorrP4();
                h_gen_reco_ph_dR->Fill(DeltaR(gph, rph), w_Z * w_ph);
            }
            else {
                auto mumugamma = recdimuon.CorrP4() + rph;
                h_fake_mmg->Fill(mumugamma.M(), w_Z * w_ph);
                h_miss_mmg->Fill(mumugamma.M(), w_Z * w_ph);
            }
        }
    }

    metainfo.Set<bool>("git", "complete", true);

    h_deltaRmin->SetDirectory(fOut);
    h_deltaRmin->Write();
    for (auto& h: h_deltaR) {
        h->SetDirectory(fOut);
        h->Write();
    }

    h_fake_mu->SetDirectory(fOut);
    h_miss_mu->SetDirectory(fOut);
    h_fake_ph->SetDirectory(fOut);
    h_miss_ph->SetDirectory(fOut);
    h_fake_mmg->SetDirectory(fOut);
    h_miss_mmg->SetDirectory(fOut);
    h_gen_reco_ph_dR->SetDirectory(fOut);
    h_fake_mu->Write();
    h_miss_mu->Write();
    h_fake_ph->Write();
    h_miss_ph->Write();
    h_fake_mmg->Write();
    h_miss_mmg->Write();
    h_gen_reco_ph_dR->Write();
    for (auto& h: h_gen_reco_mu_dR) {
        h->SetDirectory(fOut);
        h->Write();
    }

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of namespace DAS::Muon

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main(int argc, char* argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options("Obtain mu-mu-gamma control plots.", DT::split);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file");
        options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Muon::getZmmgControlPlots(inputs, output, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif

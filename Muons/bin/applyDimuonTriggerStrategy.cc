#include <cassert>
#include <cstdlib>

#include <iostream>
#include <filesystem>
#include <vector>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Lepton.h"

#include "Core/CommonTools/interface/binnings.h"
#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/CommonTools/interface/GenericSFApplier.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <TFile.h>
#include <TH1.h>
#include <TString.h>

#include "Math/VectorUtil.h"

#include <darwin.h>

#include "Core/Muons/interface/toolbox.h"

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Muon {

bool PassTriggerSelection (const Trigger& trg, const vector<RecMuon>& recMuons, int year)
{
    if (recMuons.size() < 2) return false;

    if (recMuons.at(0).CorrPt() < 19) return false;
    if (recMuons.at(1).CorrPt() <  9) return false;

    auto dimuon = recMuons.at(0).p4 + recMuons.at(1).p4;
    float m = dimuon.M();
    bool pass = false;
    switch (year) {
        case 2017:
            pass |= (m > 3.8 && trg.Bit.at(1));
            [[fallthrough]];
        case 2018:
            pass |= (m >   8 && trg.Bit.at(0));
            break;
        default:
            BOOST_THROW_EXCEPTION( std::runtime_error(to_string(year) + " has not been implemented yet"s) );
    }

    return pass;
}

////////////////////////////////////////////////////////////////////////////////
/// Modifies the weights of the reconstructed muons to apply the efficiency
/// correction.
///
/// Note: tuned for Run 2 UL
class TriggerApplier : public GenericSFApplier<RecEvent, Trigger, vector<RecMuon>>
{
    int m_year;

public:
    TriggerApplier (const fs::path& filePath, string& histname, int year,
                    bool correction, bool uncertainties)
        : GenericSFApplier(filePath, correction, uncertainties)
        , m_year(year)
    {
        loadNominal(histname);
        loadBinWiseUnc(histname + "_stat", histname + "_stat");
        loadGlobalUnc(histname + "_syst", histname + "_syst");
        loadGlobalUnc(histname + "_alt", histname + "_alt");
        loadGlobalUnc(histname + "_massBin", histname + "_massBin");
        loadGlobalUnc(histname + "_massRange", histname + "_massRange");
    }

    bool passes (const RecEvent&,
                 const Trigger& trigger,
                 const vector<RecMuon>& muons) const override
    {
        return PassTriggerSelection(trigger, muons, m_year);
    }

    int binIndex (const RecEvent&,
                  const Trigger&,
                  const vector<RecMuon>& muons,
                  const unique_ptr<TH1>& hist) const override
    {
        // We know that we have at least two muons because passes() requires this.
        return hist->FindBin(std::abs(muons.at(0).p4.Eta()),
                             std::abs(muons.at(1).p4.Eta()));
    }
};

////////////////////////////////////////////////////////////////////////////////
/// Code to apply the Double Muon trigger SFs documented at the following link:
/// https://indico.cern.ch/event/1106050/contributions/4653418/
//
/// Expecting one the following triggers:
/// - `HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass8_v`     <-- expected from n-tupliser
/// - `HLT_Mu19_TrkIsoVVL_Mu9_TrkIsoVVL_DZ_Mass8_v`
/// - `HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass3p8_v`   <-- expected from n-tupliser
/// - `HLT_Mu19_TrkIsoVVL_Mu9_TrkIsoVVL_DZ_Mass3p8_v`
///
/// The code was written assuming the calibration files found in
/// `/eos/cms/store/group/phys_muon/jmijusko/DoubleMuonTrigger_SF_UL/`
/// which contains the following `TH2D`s with axes corresponding to the
/// respective muon pseudorapidities and bin content to the efficiency.
/// - `ScaleFactor[WP]_UL[year]`
/// - `ScaleFactor[WP]_UL[year]_stat`
/// - `ScaleFactor[WP]_UL[year]_syst`
/// - `ScaleFactor[WP]_UL[year]_alt`             (except for 2018)
/// - `ScaleFactor[WP]_UL[year]_massBin`         (except for 2018)
/// - `ScaleFactor[WP]_UL[year]_massRange`       (except for 2018)
/// where `[WP]` = `Tight` or `Medium` (for both ID and ISO)
///       `[year]` = `2016_HIPM`, `2016`, `2017`, or `2018`
void applyDimuonTriggerStrategy
           (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
            const fs::path& output, //!< output ROOT file (n-tuple)
            const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
            const int steering, //!< parameters obtained from explicit options
            const DT::Slice slice = {1,0} //!< number and index of slice
            )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto tOut = flow.GetOutputTree(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    auto year = metainfo.Get<int>("flags", "year");

    auto efficiency = config.get<string>("corrections.muons.trigger_eff");
    auto [filename, histname] = getLocation(efficiency);

    TriggerApplier applier(filename, histname, year,
                           isMC && filename != "/dev/null", steering & DT::syst);
    metainfo.Set<string>("corrections", "muons", "trigger_eff", efficiency);
    for (const auto& name: applier.weightNames())
        metainfo.Set<string>("variations", RecEvent::WeightVar, name);

    auto trg = flow.GetBranchReadOnly<Trigger>("muonTrigger");
    auto recEvent = flow.GetBranchReadWrite<RecEvent>("recEvent");
    auto recMuons = flow.GetBranchReadOnly<vector<RecMuon>>("recMuons");

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        applier(*recEvent, *trg, *recMuons);

        if (steering & DT::fill) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::Muon namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options(
                "Apply scale factors for the `HLT_Mu1?_TrkIsoVVL_Mu?_TrkIsoVVL_DZ_Mass*` "
                "triggers. For UL, two working points are available: choose the tight WP "
                "for tight ID & ISO; or medium WP for medium ID & ISO; else do not use any.",
                DT::config | DT::split | DT::Friend | DT::syst);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<string>("efficiency", "corrections.muons.trigger_eff",
                            "`/path/to/file.root:histname` "
                            "(`/dev/null` for the filename to only apply selection)");
        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Muon::applyDimuonTriggerStrategy(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif

#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>

#include <TFile.h>
#include <TH2.h>
#include <TProfile.h>

#include "Core/CommonTools/interface/DASOptions.h"

#include "Math/VectorUtil.h"

#include <darwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::JetVeto {

////////////////////////////////////////////////////////////////////////////////
/// Maps the JetMET veto maps onto maps containing weights
void getConservativeMap
             (const fs::path& input,  //!< name of input root file
              const fs::path& output, //!< name of output root file
              const pt::ptree& config, //!< config handled with `Darwin::Tools::Options`
              const int steering //!< parameters obtained from explicit options
             )
{
    cout << __func__ << " start" << endl;

    if (!fs::exists(input))
        BOOST_THROW_EXCEPTION( fs::filesystem_error("Jet veta map file does not exists.",
                               input, make_error_code(errc::no_such_file_or_directory)) );
    auto fIn = make_unique<TFile>(input.c_str(), "READ");
    auto histname = config.get<string>("corrections.jetvetomap.histname");
    auto h = fIn->Get<TH2>(histname.c_str()); /// \todo make smart pointer? (last attempts led to seg)
    if (h == nullptr)
        BOOST_THROW_EXCEPTION( DE::BadInput("Can't find the efficiency map", fIn) );
    h->SetDirectory(0);
    fIn->Close();

    auto fOut = make_unique<TFile>(output.c_str(), "RECREATE"); // TODO

    for (int x = 1; x <= h->GetNbinsX(); ++x)
    for (int y = 1; y <= h->GetNbinsY(); ++y) {
        auto content = h->GetBinContent(x,y);
        static const auto deps = numeric_limits<float>::epsilon();
        content = (content < deps); // 0 or 1, can then be used as an efficiency
        h->SetBinContent(x,y,content);
    }
    h->GetXaxis()->SetTitle("#eta");
    h->GetYaxis()->SetTitle("#phi");
    h->GetZaxis()->SetRangeUser(0,1);

    h->SetDirectory(fOut.get());
    h->Write("h2hot2");

    cout << __func__ << " end" << endl;
}

} // end of DAS::JetVeto namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        fs::path input, output;

        auto options = DAS::Options("Make efficiency map from the original JetMET veto map.", DT::config);

        options.input ("input" , &input , "input ROOT file")
               .output("output", &output, "output ROOT file")
               .arg<string>("histname", "corrections.jetvetomap.histname", "name of the histogram in the inputs");

        const auto& config = options(argc, argv);
        const int steering = options.steering();

        DAS::JetVeto::getConservativeMap(input, output, config, steering);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif


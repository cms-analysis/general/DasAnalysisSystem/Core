#include <cmath>
#include <TMath.h>
#include <Math/VectorUtil.h>
#include <math.h>
#include <numeric>
#include <optional>

#include "Core/JetObservables/interface/MuellerNavelet.h"

using namespace std;
using namespace DAS;
using namespace MN;

Obs2Jets::Obs2Jets (const RecJet & Fwd, const RecJet & Bwd) :
    weightFWD( Fwd.weights.front() ), weightBWD( Bwd.weights.front() ),
    weight(Fwd.weights.front() * Bwd.weights.front()),
    LeadingJets{Fwd.p4, Bwd.p4}
{
    LeadingJets.front() *= Fwd.scales.front();
    LeadingJets.back () *= Bwd.scales.front();
}

Obs2Jets::Obs2Jets (const GenJet & Fwd, const GenJet & Bwd) :
    weightFWD( Fwd.weights.front() ), weightBWD( Bwd.weights.front() ),
    weight(Fwd.weights.front() * Bwd.weights.front()),
    LeadingJets{Fwd.p4, Bwd.p4}
{ }

float Obs2Jets::PtRatMN () const
{
    float pt1 = LeadingJets.front().Pt();
    float pt2 = LeadingJets.back ().Pt();
    if (pt1 >= pt2) swap(pt1,pt2);
    assert( pt2 > 0 );
    return pt1/pt2;
}

float Obs2Jets::PtAve () const
{
    return .5*( LeadingJets.front().Pt() + LeadingJets.back().Pt() );
}

float Obs2Jets::PtFWD () const
{
    return LeadingJets.front().Pt();
}

float Obs2Jets::PtBWD () const
{
    return LeadingJets.front().Pt();
}

float Obs2Jets::DEta () const
{
    auto deta =  std::abs(LeadingJets.front().Eta() - LeadingJets.back().Eta());
    assert( deta > numeric_limits<float>::epsilon() );
    return deta;
}

float Obs2Jets::DPhi () const
{
    FourVector jet1 = LeadingJets.front();
    FourVector jet2 = LeadingJets.back ();
    using ROOT::Math::VectorUtil::DeltaPhi;
    float dphi = DeltaPhi(jet1,jet2);
    float sign = std::abs(dphi) / dphi;
    dphi -= sign * M_PI;
    return dphi;
}

float Obs2Jets::CosDPhi (int n) const
{
    return TMath::Cos(n*DPhi());
}

size_t Obs2Jets::size () const { return 2; }

ObsMiniJets::ObsMiniJets ( const vector<RecJet> & recJets ) :
     weight( (recJets.size()<1) ? 0 :
             accumulate( recJets.begin(), recJets.end(), 1., 
                [] ( double w, const RecJet& j ) { return w * j.weights.front(); } ) )
{
    for (auto it = recJets.begin() ; it != recJets.end() ; ++it) {

        FourVector p4 = it->p4;
        p4 *= it->scales.front();
        MiniJets.push_back( p4 );
    }
}

ObsMiniJets::ObsMiniJets ( const vector<GenJet> & genJets ) :
    weight( (genJets.size() < 1) ? 0 :
            accumulate( genJets.begin(), genJets.end(), 1., 
                [] ( double w, const GenJet& j ) { return w * j.weights.front(); } ) )
{
    for (auto it = genJets.begin() ; it != genJets.end() ; ++it)
        MiniJets.push_back( it->p4 );
}

float ObsMiniJets::PtAveMini () const
{
    assert(MiniJets.size()>0);
    auto AddPt = [] ( float sumpt, const FourVector& v) { return sumpt + v.Pt(); };
    float ptavemini = accumulate(MiniJets.begin(), MiniJets.end(), 0., AddPt);
    return ptavemini/MiniJets.size();
}

float ObsMiniJets::DEtaAveMini () const
{
    assert(MiniJets.size() >= 2);
    float detaavemini = 0;
    for (auto it=MiniJets.begin() ; it != prev(MiniJets.end()) ; ++it)
        detaavemini += it->Eta() - next(it)->Eta();
    return detaavemini/MiniJets.size();
}

float ObsMiniJets::REtaAveMini () const
{
    assert(MiniJets.size() >= 2); 
    float retaavemini = 0;
    for (auto it=MiniJets.begin() ; it != prev(MiniJets.end()) ; ++it)
        retaavemini += it->Eta()/next(it)->Eta();
    return retaavemini/MiniJets.size();
}

float ObsMiniJets::RPtExpDEta () const
{
    assert(MiniJets.size() >= 2);
    float rptexpdeta = 0;
    for (auto it=MiniJets.begin() ; it != prev(MiniJets.end()) ; ++it) {
        auto it2 = next(it);
        auto Eta = it->Eta(), Eta2 = it2->Eta();
        auto expo = exp( Eta - Eta2 );
        auto Pt = it->Pt(), Pt2 = it2->Pt();
        assert( Pt2 > 0 );
        rptexpdeta += Pt / Pt2 * expo;
    }
    return rptexpdeta/MiniJets.size();
}

size_t ObsMiniJets::size () const { return MiniJets.size(); }

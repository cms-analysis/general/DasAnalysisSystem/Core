#pragma once

#include "Core/Objects/interface/Jet.h"

#include <algorithm>
#include <list>
#include <optional>
#include <vector>

////////////////////////////////////////////////////////////////////////////////
/// The namespace MN_Helper contains the classes that are needed by getMNobservables.cc
/// to build the histograms.
/// Two types of objects are present here: *bs2Jets* and *ObsMiniJets* that contains structures to
/// store the observables themselves (they are higher level physics objects)
/// and *Hist* that contains histograms.
namespace DAS::MN {

////////////////////////////////////////////////////////////////////////////////
/// Implements MN jets observables depending on the 2 Mueller-Navelet jets of the event.
/// Definition of these observables:
/// - $p_{T,ave} = \frac{p_{T,FWD}+p_{T,BWD}}{2}$
/// - $p_{T,FWD}$ transverse momentum of the forward jet
/// - $p_{T,BWD}$ transverse momentum of the backward jet
/// - $p_{T,RatMN} = min \left( \frac{P_{T,FWD}}{P{T,BWD}} , \frac{P_{T,BWD}}{P{T,FWD}} \right)$
/// - $\pi - \Dela \phi$ is the azimuthal distance between the two Mueller-Navelet Jets
///   (mind that the function Obs2Jets::DPhi() returns in fact $\pi - \Dela \phi$)
/// - $\cos\left( n \left( \pi - \Delta \phi \right) \right)$ with $n = 1,2,3,4,...$
///   (implemented in CosDPhi() which directly returns this observable)
struct Obs2Jets {

    Obs2Jets ( const DAS::RecJet & Fwd, const DAS::RecJet & Bwd );
    Obs2Jets ( const DAS::GenJet & Fwd, const DAS::GenJet & Bwd );

    float PtAve () const;
    float PtFWD () const;
    float PtBWD () const;
    float PtRatMN () const;
    float DEta () const;
    float DPhi () const;
    float CosDPhi (int n //!< order
            ) const;
    size_t size () const;

    const float weightFWD, //!< weight of the most forward jet
                weightBWD; //!< weight of the most backward jet
    const float weight;    //!< product of weightFWD and weightBWD

private:
    std::vector<DAS::FourVector> LeadingJets; //!< The most forward and the most backward jets.
                                              //!< The size is always 2 and and the type is taken
                                              //!< in consistency with ObsMiniJets.
};

////////////////////////////////
/// MN_Helper::ObsMiniJets is a structure that implements observables
/// based on the jets that are emmited in-between the Mueller-Navelet jets.
/// These jets are called "mini jets"
/// Definition of these observavbles ($N$ is the number of jets in the event,
/// and the jets are labeled $i \in (0,N-1)$, including the two Mueller-Navelet jets,
/// the jets are assumed ordred in rapidity from highest rapidity to lowest rapidity (signed rapidity).
/// - PtAveMini --> $ \left\langle P_{T,Mini} \right\rangle = \frac{1}{N-2} \sum_{i=1}^{N-2} p_{T,i} $
/// - DEtaAveMini --> &  \left\langle \Delta \eta_{Mini} \right\rangle =  \frac{1}{N-3} \sum_{i=1}^{N-3} \left\vert \eta_{i} - \eta_{i+1} \right\vert$
/// - REtaAveMini --> $ R_{k} = \frac{1}{N-3} \sum_{i=1}^{N-3} \frac{\eta_{i}}{\eta_{i+1}} $
///   \todo Check this and compare with the standard definition.
/// - RPtExpDEta --> $ R_{ky} = \frac{1}{N-3} \sum_{i=1}^{N-3} min \left( \frac{P_{T,i}}{P_{T,i+1}}, \frac{P_{T,i+1}}{P_{T,i}} \right) \exp \left( \eta_{i} - \eta_{i+1} \right) $
struct ObsMiniJets {

    ObsMiniJets ( const std::vector<DAS::RecJet> & recJets );
    ObsMiniJets ( const std::vector<DAS::GenJet> & genJets );

    float PtAveMini () const;
    float DEtaAveMini () const;
    float REtaAveMini () const;
    float RPtExpDEta () const;
    size_t size () const;

    const float weight;

private:
    std::vector<DAS::FourVector> MiniJets; //!< The jets that are between the Mueller-navelet Jets.
                                         //!< The size should be >1.
};// end of class ObsMiniJets

////////////////////////////////////////////////////////////////////////////////
/// This function takes as an input a vector of jet and a function<bool(Jet&)>.
/// It finds the two jets that are th most bakward and most forward in the event
/// and satisfies the cut implemented in the function<bool(Jet&)> provided as argument.
/// This function is apllied to all the jets. If it return false, the jets are discarded.
/// The out is an optional. So that if there are no jets satifying the cuts in the event, it
/// returns `nullopt`.
/// \note As this function is a template function, it is declared and defined in the
///       `common.h`. Definitions of template functions are not compiled until they are
///       instantiated with a specific type instance.
template<typename Jet>
std::optional<std::pair<Jet,Jet>> GetMNJet (std::vector<Jet> jets, std::function<bool(Jet&)> ptcut=[](Jet& jet) {return jet.p4.Pt() < 35 ;});

template<typename Jet>
std::optional<std::pair<Jet, Jet>> GetMNJet (std::vector <Jet> jets, std::function<bool(Jet&)> ptcut)
{
    jets.erase(remove_if(jets.begin(), jets.end(), ptcut), jets.end());
    if (jets.size() < 2) return std::nullopt;
    auto result = minmax_element( jets.begin(), jets.end() , [](Jet& j1, Jet& j2)
            {return j1.p4.Eta() > j2.p4.Eta();} );  /// \todo check the consistency of this condition

    if (result.first->p4.Eta() <= 0 || result.second->p4.Eta() >= 0) return std::nullopt;
    return std::make_optional<std::pair<Jet,Jet>>(*result.first, *result.second);
}

////////////////////////////////////////////////////////////////////////////////
/// This function takes as an input a vector of jet, a pair of Mueller-Navelet jets in this event
/// and a selection fuction. All the jet for which the selection function is false are dicarded.
/// Remove all the jets that are at higher-or-equal rapidity than the first member of the pair (considered
/// as the 'leading MN jet') or lower-or-equal rapidity than the second member of the pair (considered a the
/// 'subleading MN jet'). The function is build assuming that the two Mueller-Navelet jets
/// that are provided are also present in the vector of jets of the event. The two Mueller-Navelet jets
/// are removed by the lower-or-equal / higher-or-equal logic of the sample.
/// \note there is no consistency check. So if the two MNJets that are given are not present in the vector of
///       jets, it will not throw any error.
/// \note if the paire provided is trivial, it will not throw any error. The non-triviallity
///       must be check before hand.
/// \note As this function is a template function, it is declared and defined in the
///       `common.h`. Definitions of template functions are not compiled until they are instantiated
///       with a specific type instance.
template<typename Jet>
std::vector<Jet> GetMiniJets (std::vector<Jet> jets, const std::pair<Jet,Jet>& MNJets, std::function<bool(Jet&)> ptcut=[](Jet& jet) {return jet.p4.Pt()<20;});

template<typename Jet>
std::vector<Jet> GetMiniJets (std::vector<Jet> jets, const std::pair<Jet,Jet>& MNJets, std::function<bool(Jet&)> ptcut)
{
    jets.erase(std::remove_if(jets.begin(), jets.end(), ptcut), jets.end());
    jets.erase(std::remove_if(jets.begin(), jets.end(),
               [MNJets](Jet& jet){return jet.p4.Eta() >= MNJets.first.p4.Eta();}), jets.end());

    jets.erase(std::remove_if(jets.begin(), jets.end(),
               [MNJets](Jet& jet){return jet.p4.Eta() <= MNJets.second.p4.Eta();}), jets.end());

    std::sort( jets.begin(), jets.end() , [](Jet& j1, Jet& j2)
            {return j1.p4.Eta() > j2.p4.Eta();} );  /// \todo check the consistency of this condition

    return jets;
}

} // end of namespace DAS::MN

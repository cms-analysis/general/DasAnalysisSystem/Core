#include <cstdlib>
#include <cassert>

#include <iostream>
#include <filesystem>
#include <vector>

#include "Core/Objects/interface/Di.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/binnings.h"
#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <TFile.h>
#include <TH1.h>
#include <TString.h>

#include "Math/VectorUtil.h"

#include <darwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Jets {

////////////////////////////////////////////////////////////////////////////////
/// Checks whether an event should be kept
template<class Jet, class Dimuon>
bool keepEvent(const vector<Jet>& jets, const Dimuon& dimuon, float zpt, float jpt)
{
    return jets.size() >= 2 && jets[0].p4.Pt() > jpt &&
            dimuon && dimuon.CorrP4().Pt() > zpt;
}

////////////////////////////////////////////////////////////////////////////////
/// Skims the input tree to keep only events with at least two jets, with
/// configurable pT thresholds.
void applyZJetSkim
           (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
            const fs::path& output, //!< output ROOT file (n-tuple)
            const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
            const int steering, //!< parameters obtained from explicit options
            const DT::Slice slice = {1,0} //!< number and index of slice
            )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto tOut = flow.GetOutputTree(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);

    const auto zpt = config.get<float>("skims.ZJet.zpt");
    metainfo.Set<float>("skims", "ZJet", "zpt", zpt);

    const auto jpt = config.get<float>("skims.ZJet.jpt");
    metainfo.Set<float>("skims", "ZJet", "jpt", jpt);

    cout << "Skimming events: pt(Z) > " << zpt << "\t pt(jet) > " << jpt << endl;

    auto recJets = flow.GetBranchReadOnly<vector<RecJet>>("recJets", DT::facultative);
    auto genJets = flow.GetBranchReadOnly<vector<GenJet>>("genJets", DT::facultative);
    if (!recJets && !genJets)
        BOOST_THROW_EXCEPTION( DE::BadInput("No jets in input tree", tIn) );

    auto recDimuon = flow.GetBranchReadOnly<RecDimuon>("recDimuon", DT::facultative);
    auto genDimuon = flow.GetBranchReadOnly<GenDimuon>("genDimuon", DT::facultative);
    if (!recDimuon && !genDimuon)
        BOOST_THROW_EXCEPTION( DE::BadInput("No dimuon in input tree", tIn) );

    RecZJet * recZJet = nullptr;
    if (recJets && recDimuon)
        recZJet = flow.GetBranchWriteOnly<RecZJet>("recZJet");
    GenZJet * genZJet = nullptr;
    if (genJets && genDimuon)
        genZJet = flow.GetBranchWriteOnly<GenZJet>("genZJet");

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        bool passesRec = true, passesGen = true;

        if (recJets) {
            passesRec = keepEvent(*recJets, *recDimuon, zpt, jpt);
            if (passesRec) *recZJet = *recDimuon + recJets->at(0);
            else            recZJet->clear();
        }
        if (genJets) {
            passesGen = keepEvent(*genJets, *genDimuon, zpt, jpt);
            if (passesGen) *genZJet = *genDimuon + genJets->at(0);
            else            genZJet->clear();
        }

        if ((steering & DT::fill) && (passesRec || passesGen)) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::Jets namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options(
                            "Selects events with at least a Z and a jet passing pT cuts "
                            "and creates dijet objects directly in the tree.",
                            DT::config | DT::split | DT::fill);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<float>("zpt", "skims.ZJet.zpt", "Minimum pT of the first jet")
               .arg<float>("jpt", "skims.ZJet.jpt", "Minimum pT of the second jet");
        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Jets::applyZJetSkim(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif

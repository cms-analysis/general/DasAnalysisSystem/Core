#include <cassert>
#include <iostream>
#include <limits>

#include <TString.h>
#include <TChain.h>
#include <TFile.h>

#include "Math/VectorUtil.h"

#include "Core/CommonTools/interface/ControlPlots.h"
#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/Prefiring/interface/applyPrefiringWeights.h"
#include "Core/Objects/interface/Format.h"

#include <darwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Prefiring {

////////////////////////////////////////////////////////////////////////////////
/// Application of prefiring weights in data or simulation.
///
/// Calls a functor
void applyPrefiringWeights
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuple)
         const fs::path& output, //!< output ROOT file (n-tuple)
         const pt::ptree& config, //!< config handled with `Darwin::Tools::Options`
         const int steering, //!< parameters obtained from explicit options
         const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto [fOut, tOut] = flow.GetOutput(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);

    auto isMC = metainfo.Get<bool>("flags", "isMC");
    if (isMC)
        BOOST_THROW_EXCEPTION( DE::BadInput("Only real data may be used as input.", metainfo) );

    auto year = metainfo.Get<int>("flags", "year");
    if (year != 2016 && year != 2017)
        BOOST_THROW_EXCEPTION( DE::BadInput("Only 2016 and 2017 data may be used as input.", metainfo) );

    auto genEvt = flow.GetBranchReadOnly<GenEvent>("genEvent");
    auto recEvt = flow.GetBranchReadWrite<RecEvent>("recEvent");
    auto recJets = flow.GetBranchReadOnly<vector<RecJet>>("recJets");

    ControlPlots raw("raw");
    vector<ControlPlots> calib { ControlPlots("nominal") };
    if (steering & DT::syst) {
        metainfo.Set<string>("variations", RecEvent::WeightVar, "Pref" + SysDown);
        metainfo.Set<string>("variations", RecEvent::WeightVar, "Pref" + SysUp);

        calib.push_back(ControlPlots("Pref" + SysDown));
        calib.push_back(ControlPlots("Pref" + SysUp));
    }

    auto file = config.get<fs::path>("corrections.prefiring.file");
    auto name = config.get<string>  ("corrections.prefiring.name");
    auto type = config.get<string>  ("corrections.prefiring.type");
    Prefiring::Functor apply(file, name, type, steering & DT::syst);
    metainfo.Set<fs::path>("corrections", "prefiring", "file", file);
    metainfo.Set<string>  ("corrections", "prefiring", "name", name);
    metainfo.Set<string>  ("corrections", "prefiring", "type", type);

    auto totRecWgt = [&](size_t i) {
        return (isMC ? genEvt->weights.front().v : 1.) * recEvt->weights.at(i).v;
    };

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        raw(*recJets, totRecWgt(0));
        apply(*recEvt, *recJets);
        for (size_t i = 0; i < calib.size(); ++i)
            calib.at(i)(*recJets, totRecWgt(i));

        if (steering & DT::fill) tOut->Fill();
    }

    raw.Write(fOut);
    for (size_t i = 0; i < calib.size(); ++i)
        calib.at(i).Write(fOut);

    TDirectory * controlplots = fOut->mkdir("Prefiring");
    apply.Write(controlplots);

    metainfo.Set<bool>("git", "complete", true);

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::Prefiring namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options(
                            "Apply efficiency correction for ECAL prefiring.",
                            DT::config | DT::split | DT::Friend | DT::syst);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("file", "corrections.prefiring.file", "location of ROOT file with efficiency correction")
               .arg<string>  ("name", "corrections.prefiring.name", "name of the efficiency map of function in the ROOT file")
               .arg<string>  ("type", "corrections.prefiring.type", "'binned' or 'smooth'");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Prefiring::applyPrefiringWeights(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif

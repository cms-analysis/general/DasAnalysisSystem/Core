#ifndef DOXYGEN_SHOULD_SKIP_THIS
#define DOXYGEN_SHOULD_SKIP_THIS
#include "Core/JEC/bin/common.h"

#define BOOST_TEST_MODULE applyJEScorrections
#include <boost/test/included/unit_test.hpp>

#include <iostream>

using namespace DAS;
using namespace std;

using namespace JetEnergy;
namespace DT = Darwin::Tools;

BOOST_AUTO_TEST_CASE( get_short_campaign )
{
    BOOST_TEST( GetShortCampaign("Summer19UL18_") == "Summer19UL18"s );
    BOOST_TEST( GetShortCampaign("Summer19UL18_RunA_") == "Summer19UL18"s );
    BOOST_REQUIRE_THROW( GetShortCampaign("BOOOM"), boost::wrapexcept<invalid_argument> );
}

BOOST_AUTO_TEST_CASE( get_algo )
{
    DT::UserInfo userinfo;

    userinfo.Set<int>("flags", "year", 2018);
    BOOST_TEST( GetAlgo(userinfo) == "chs" );

    userinfo.Set<const char *>("flags", "labels", "PUPPI");
    BOOST_TEST( GetAlgo(userinfo) == "Puppi" );
}

BOOST_AUTO_TEST_CASE( get_R )
{
    DT::UserInfo userinfo;

    userinfo.Set<int>("flags", "year", 2018);
    userinfo.Set<int>("flags", "R", 7);

    BOOST_TEST( GetR(userinfo) == 8 );
}




#endif

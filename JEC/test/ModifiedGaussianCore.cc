#include <cstdlib>
#include "Core/JEC/interface/resolution.h"

#include <TROOT.h>
#include <TCanvas.h>
#include <TLine.h>
#include <TLegend.h>
#include <TF1.h>

double difference (double * x, double * p)
{
    double N = p[0], mu = p[1], sigma = p[2];
    double gauss = N*exp(-0.5*pow((*x-mu)/sigma,2));
    return ModifiedGaussianCore(x, p) - gauss;
}

int main (int argc, char * argv[])
{
    gROOT->SetBatch();
    auto c = new TCanvas("ModifiedGaussianCore", "ModifiedGaussianCore", 600, 800);

    double x1 = -0.8, 
           x2 = 0.8,
           y1 = 1e-6,
           y2 = 1;
    c->DrawFrame(x1, y1, x2, y2, ";#Delta;");
    c->SetTicks(1,1);
    c->SetLogy();

    double N = 0.1,
           mu = 0.01,
           sigma = 0.13,
           B = -10,
           C = 10;

    TF1 * f = new TF1("ModifiedGaussianCore", ModifiedGaussianCore, x1, x2, 5);
    double p[] = { N, mu, sigma, B, C };
    f->SetParameters(p);
    f->Draw("same");

    TF1 * g = new TF1("gaus", "gaus(0)", x1, x2);
    g->SetParameters(p);
    g->SetLineColor(kBlue);
    g->SetLineStyle(2);
    g->SetLineWidth(1);
    g->Draw("same");
    
    TF1 * h = new TF1("diff", difference, x1, x2, 5);
    h->SetParameters(p);
    h->SetLineColor(kGreen+2);
    h->SetLineWidth(1);
    h->Draw("same");

    TLegend * legend = new TLegend(0.6, 0.7, 0.89, 0.85);
    legend->SetBorderSize(0);
    legend->AddEntry(f, "global function", "l");
    legend->AddEntry(g, "Gaussian core", "l");
    legend->AddEntry(h, "difference", "l");

    auto line = [&](Style_t s, double x, TString title = "") {
        TLine * line = new TLine;
        line->SetLineStyle(s);
        line->DrawLine(x, y1, x, f->Eval(x));
        if (title != "") legend->AddEntry(line, title, "l");
    };
    line(1, mu, Form("mean (#mu = %.2f)", mu));
    line(2, mu+sigma, Form("mean #pm width (#sigma = %.2f)", sigma));
    line(2, mu-sigma);

    legend->Draw();
    c->RedrawAxis();
    c->Print("ModifiedGaussianCore.pdf");

    return EXIT_SUCCESS;
}

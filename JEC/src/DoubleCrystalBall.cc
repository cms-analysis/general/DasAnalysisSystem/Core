#include "Core/JEC/interface/DoubleCrystalBall.h"

#include <cassert>
#include <cmath>

#include <iostream>
#include <iomanip>
#include <limits>

#include <TF1.h>

using namespace std;

static const double dinf = numeric_limits<double>::infinity();
static const double deps = numeric_limits<double>::epsilon();

namespace DAS::DoubleCrystalBall {

void Base::SetParameters (double * p)
{
    N = p[0];
    mu = p[1];
    sigma = max(1e-8,abs(p[2]));
    assert(sigma > 0);
    kL = min(mu-1e-8,p[3]);
    nL = max(1+deps,p[4]);
    kR = max(mu+1e-8,p[5]);
    nR = max(1+deps,p[6]);

    aR = abs(Z(kR));
    aL = abs(Z(kL));

    expaR2 = exp(-pow(aR,2)/2.);
    expaL2 = exp(-pow(aL,2)/2.);

    AR = pow(nR/aR,nR) * expaR2;
    AL = pow(nL/aL,nL) * expaL2;
    BR =     nR/aR    - aR;
    BL =     nL/aL    + aL;
    // these parameters are usually used in the analytical formula
    // e.g. on Wikipedia
    // but in practice the n^n factor can get too large
    // therefore we actually do not use them in practice
    // but we still calculate them 

    CR = nR * expaR2 / (aR*(nR-1));
    CL = nL * expaL2 / (aL*(nL-1));
    DD = sqrt(M_PI/2.)*(erf(aR/sqrt(2)) + erf(aL/sqrt(2)));
    K = CR + CL + DD;
}

double Base::operator() (double * x, double *p)
{
    SetParameters(p); // store all parameters (also for external use)
    z = Z(*x);
    Eval(x); // this changes according to the derived class
    return result;
}

Base::Base (TF1 * f) :
    N(1), mu(0), sigma(1), kL(-dinf), nL(2), kR(dinf), nR(2), // basic parameters
    z(0), aR(dinf), aL(-dinf), // normalised parameters
    expaR2(0), expaL2(0), // values of core at transition points
    CR(0), CL(0), DD(sqrt(2*M_PI)), // normalisation
    result(0)
{
    if (f == nullptr) return;
    assert(f->GetNpar() == 7);
    double * p = f->GetParameters();
    SetParameters(p);
}

void Base::dump ()
{
    cout << setw(10) << N
         << setw(10) << mu
         << setw(10) << sigma
         << setw(10) << kL
         << setw(10) << nL
         << setw(10) << kR
         << setw(10) << nR

         << setw(10) << z
         << setw(10) << aR
         << setw(10) << aL

         << setw(10) << expaR2
         << setw(10) << expaL2

         << setw(10) << CR
         << setw(10) << CL
         << setw(10) << DD

         << setw(10) << result

         << endl;
}

void Distribution::Eval (double * x)
{
    if (*x < kL) {
        double R = aL/nL;
        result = (N/K) * expaL2*pow(1-R*(aL+z),-nL)/sigma;
    }
    else if (*x < kR) {
        result = (N/K) * exp(-pow(z,2)/2.)/sigma;
    }
    else {
        double R = aR/nR;
        result = (N/K) * expaR2*pow(1-R*(aR-z),-nR)/sigma;
    }
}

void Integral::Eval (double * x)
{
    if (*x < kL) {
        double R = aL/nL;
        result = (N/K) * expaL2*pow(1-R*(aL+z),1-nL) / (R* (nL-1));
    }
    else if (*x < kR) {
        result = (N/K) * (CL + sqrt(M_PI/2.)*(erf(z/sqrt(2)) + erf(aL/sqrt(2))));
    }
    else {
        double R = aR/nR;
        result = (N/K) * (CL + DD + expaR2 * ( 1. - pow(1-R*(aR-z),1-nR) ) / ( R* (nR-1) ) );
    }
}

void DLog::Eval (double * x)
{
    if (*x < kL) {
        double B = nL/aL + aL;
        result = (nL/sigma) / (B - z);
    }
    else if (*x < kR) {
        result = -z/sigma;
    }
    else {
        double B = nR/aR - aR;
        result = -(nR/sigma) / (B + z);
    }
}

}


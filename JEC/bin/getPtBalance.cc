#include <cassert>
#include <cstdlib>

#include <algorithm>
#include <iostream>
#include <fstream>
#include <functional>
#include <type_traits>
#include <vector>

#include <TFile.h>
#include <TRandom.h>
#include <TH3.h>
#include <TString.h>
#include <TDirectory.h>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Format.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/CommonTools/interface/toolbox.h"

#include "Math/VectorUtil.h"

#include <darwin.h>

using namespace std;
namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::JetEnergy {

static const auto deps = numeric_limits<double>::epsilon();

vector<double> GetBinning (TAxis * axis)
{
    int n = axis->GetNbins();
    vector<double> bins(n+1);
    for (int i = 0; i <= n; ++i)
        bins[i] = axis->GetBinLowEdge(i+1);
    bins[n] = axis->GetBinUpEdge(n);
    return bins;
}

////////////////////////////////////////////////////////////////////////////////
/// This function is called by `loopDirsFromGetBalance()`. It takes as argument
/// a vector of TH3 taht presumably represents the transverse momentum balance
/// histograms in bins of (\f$p_{T}^{bal}\f$, \f$p_{T}^{ave}\f$, \f$y^{probe}\f$)
/// for different variations of the JES. This function project the pt balance
/// histogram in 1D for each bin of $p_{T}^{ave}$ and $y^{probe}$. This
/// function calculates the histogram of the mean value of the balance in bins
/// of \f$p_{T}^{ave}\f$ for each bin, it calculates the incertainty bands deduced
/// from the statistical variations of the mean and the systematics of the JES
/// variations.
///
/// \note for an explicit definition of the variables, see the additional
/// docstring in the core of the code.
/// 
/// The second arguments of this function is the directory in which the
/// histograms are written. This function creates the subdirectory structure
/// necessary to store each series of variations corresponding to each
/// $y^{probe}$ and $p_{T}^{ave}$ bins in a given directory.
///
/// The structure of the output is:
/// ~~~
///   file/ > dOut/ > eta1/ > nominal (TH1, mean value of the \f$p_{T}^{bal}\f$ w. stat. unc.)
///                         > upper (upper limit of the variations stat+syst)
///                         > lower (lower limit of the variations stat+syst)
///                         > ptbin1 > rec
///                         > ptbin2
///                         > ...
///                         > ptbin${nPtbins}
///                 > ...
///                 > eta${nYbins}
/// ~~~
/// The structure at `file/dOut/` level is identical to the structure of the input file.
void ProcessVariations (const vector<TH3*>& variations, TDirectory * dOut)
{
    vector<double> YBins = GetBinning(variations.front()->GetZaxis()),
                   PtBins = GetBinning(variations.front()->GetYaxis());
    const int nPtBins = PtBins.size() - 1;

    // Iterating over the rapidity bins
    for (int y = 1 ; y <= variations.front()->GetNbinsZ() ; ++y) {

        // Create the directory "y_bin"
        TDirectory * diry = dOut->mkdir( Form("ybin%d", y),
                                         Form("%.1f < |y| < %.1f", YBins[y-1],
                                                                   YBins[y  ]) );
        cout << bold << diry->GetPath() << '\t' << diry->GetTitle() << normal << endl;
        diry->cd();

        // `nominal`, `upper` and `lower` contains average value of $p_{T,1}/p_{T,2}$ in bins of $p_{T}$
        auto nominal = make_unique<TH1F>("nominal", "", nPtBins, PtBins.data()),
             upper   = make_unique<TH1F>("upper"  , "", nPtBins, PtBins.data()),
             lower   = make_unique<TH1F>("lower"  , "", nPtBins, PtBins.data());

        // Setting the directory in which to write the histograms
        nominal->SetDirectory(diry);
        upper->SetDirectory(diry);
        lower->SetDirectory(diry);

        // Setting the title of the axis
        nominal->SetXTitle("p_{T,ave}");
        nominal->SetYTitle("#frac{1 - #LT #frac{#Delta p_{T}}{p_{T,ave}} #GT}{1 + #LT #frac{#Delta p_{T}}{p_{T,ave}} #GT }");
        upper->SetXTitle("p_{T,ave}");
        upper->SetYTitle("#frac{1 - #LT #frac{#Delta p_{T}}{p_{T,ave}} #GT}{1 + #LT #frac{#Delta p_{T}}{p_{T,ave}} #GT }");
        lower->SetXTitle("p_{T,ave}");
        lower->SetYTitle("#frac{1 - #LT #frac{#Delta p_{T}}{p_{T,ave}} #GT}{1 + #LT #frac{#Delta p_{T}}{p_{T,ave}} #GT }");

        // Iterating over the pT bins.
        for (int ipt = 1 ; ipt <= nPtBins ; ++ipt) {

            // `errUp` (`errDn`) contains the error coming from the upper (lower) variations.
            auto errUp = variations.front()->GetBinError(ipt),
                 errDn = errUp,
                 content = 0.; TDirectory * dirpt = diry->mkdir( Form("ptbin%d",ipt),
                                              Form("%.0f < p_{T} < %.0f", PtBins[ipt-1],
                                                                          PtBins[ipt  ]) );
            dirpt->cd();
            cout << bold << dirpt->GetPath() << '\t' << dirpt->GetTitle() << normal << endl;

            // Iterating over the variations.
            for (TH3 * v: variations) {
                auto balance = v->ProjectionX("h", ipt, ipt, y, y, "e");
                balance->SetTitle(v->GetTitle());
                balance->SetDirectory(dirpt);
                auto I = balance->Integral();

                if (std::abs(I) < deps) continue;
                cout << "variation " << v->GetName() << ": integral = " << I << endl;
                balance->Scale(1./I);

                auto mean = balance->GetMean();
                auto meanErr = balance->GetMeanError();

                if (v == variations.front()) {
                    //////////////////////////////////////////////////////////////////////////////// 
                    /// - The asymmetry is
                    ///   \f$A = 2 \frac{p_{T,1}-p_{T,2}}{p_{T,1} + p_{T,2}}\f$
                    /// - The balance is
                    ///   \f$B = \frac{1 - \frac{ \left\langle A \right\rangle }{2}}
                    ///               {1 + \frac{ \left\langle A \right\rangle }{2}}\f$
                    auto B = (1.-mean/2.)/(1.+mean/2.),
                         Bup = (1.-(mean+meanErr)/2.)/(1.+(mean+meanErr)/2.),
                         Bdn = (1.-(mean-meanErr)/2.)/(1.+(mean-meanErr)/2.);
                    /// The nominal value is set once for each bins of \f$p_{T,ave}\f$
                    // while iterating over the variations.
                    nominal->SetBinContent(ipt, B);
                    nominal->SetBinError(ipt, std::abs(Bup-Bdn)/2.);

                    // the nominal content is set once while iterating over the variations
                    content = mean;
                }
                else {
                    // For each variation, if the variation is larger (smaller) then the
                    // nominal content, `var-content` is added to `errUp` (`errDn`).
                    double& err = mean > content ? errUp : errDn;
                    err = hypot(err, mean - content);
                }
                balance->Write(v->GetName());
            }
            // See comment l.136 (close to the filling of `nominal`).
            // Contains the nominal mean value of balance in each bins of pt.
            upper->SetBinContent(ipt, (1.-(content+errUp)/2)/(1.+(content+errUp)/2) );
            lower->SetBinContent(ipt, (1.-(content-errDn)/2)/(1.+(content-errDn)/2) );
        }
        diry->cd();
        nominal->Write();
        upper->Write();
        lower->Write();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// This function is called by `getPtBalance()`.
/// Recusively iterates over the internal structure of a file/directory and each
/// it fins a serie of TH3 in the file, it interpret it as the many variations
/// of the jet energy balance and us it to calculate the mean of the balance
/// and also the balance curves in bins of pt and eta.
void loopDirsFromGetBalance (TDirectory *dIn, TDirectory * dOut)
{
    // Recursively explore the directory present in the file.
    // At each level where there are TH3, they are stored in a vector.
    vector<TH3*> variations;
    for (const auto&& obj: *(dIn->GetListOfKeys())) {
        auto const key = dynamic_cast<TKey*>(obj);
        if ( key->IsFolder() ) {
            auto ddIn = dynamic_cast<TDirectory*>( key->ReadObj() );
            if (ddIn == nullptr) {
                cerr << red << "ddIn = " << ddIn << '\n' << def;
                continue;
            }
            TDirectory * ddOut = dOut->mkdir(ddIn->GetName(), ddIn->GetTitle());
            ddOut->cd();
            cout << "creating directory " << bold << ddIn->GetPath() << normal << endl;

            loopDirsFromGetBalance(ddIn, ddOut);
        }
        else if ( ( TString( key->ReadObj()->ClassName() ).Contains("TH3") ) ) {
            auto bal3D = dynamic_cast<TH3*>( key->ReadObj() );
            bal3D->SetDirectory(0);
            variations.push_back(bal3D);
        }
        else cout << "Found " << orange << key->ReadObj()->GetName() << normal << ".. Is of "
                 << underline << key->ReadObj()->ClassName() << normal << " type, ignoring it" << endl;
    } // End of for (listof keys) loop

    ////////////////////////////////////////////////////////////////////////////////
    /// NOTE: additional remark on the implementation:
    /// Once the TH3 representing the binned in pt_balance, pt_ave, y (presumably in the order (x, y, z),
    /// the pt_balance is projected into a 1-D histogram for each bin of rapidity and pt_ave
    /// (this done for each of the variations). For each bin of rapidity, the mean of the pt_balance is
    /// computed (see formula in th comments of `ProcessVariations()` the standard deviations from the
    /// balance are calculated from the statistical uncertainities and from the variations (presumably
    /// the variations of the JES).
    if (variations.size() > 0)
        ProcessVariations(variations, dOut);
}

////////////////////////////////////////////////////////////////////////////////
/// This function iterate over the histograms produced by ```getPtBalance``` and produces the
/// the momentum balance histograms in bins of ptave and eta. It calls the `loopDir()` function defined above.
void getPtBalance 
            (const fs::path& input, //!< input ROOT file (histograms)
             const fs::path& output, //!< name of output ROOT (histograms)
             const pt::ptree& config, //!< config file from `DT::Options`
             const int steering //!< steering parameters from `DT::Options`
            )
{
    /// \todo use config and steering (e.g. for verbosity)
    auto fIn  = make_unique<TFile>(input .c_str(), "READ"),
         fOut = make_unique<TFile>(output.c_str(), "RECREATE");
    JetEnergy::loopDirsFromGetBalance(fIn.get(), fOut.get());
}

} // end of DAS::JetEnergy namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        fs::path input, output;

        auto options = DAS::Options("Get pt average balance in bins of pt and eta.");
        options.input ("input" , &input , "input ROOT file (from `get*Balance`)")
               .output("output", &output, "output ROOT file");

        const auto& config = options(argc, argv);
        const int steering = options.steering();

        DAS::JetEnergy::getPtBalance(input, output, config, steering);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
#endif
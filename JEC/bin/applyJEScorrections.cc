#include <cstdlib>
#include <iostream>
#include <memory>
#include <string>

#include <TChain.h>
#include <TFile.h>
#include <TH2.h>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Format.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/ControlPlots.h"
#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <correction.h>
#include <darwin.h>

#include "common.h"

using namespace std;
namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::JetEnergy {

////////////////////////////////////////////////////////////////////////////////
/// Apply JES corrections to data and MC *n*tuples, as well as uncertainties
/// in data (all sources are considered separately)
void applyJEScorrections
            (const vector<fs::path> inputs, //!< input ROOT files (n-tuples)
             const fs::path output, //!< name of output root file containing the histograms
             const pt::ptree& config, //!< config file from `DT::Options`
             const int steering, //!< steering parameters from `DT::Options`
             const DT::Slice slice = {1,0} //!< slices for running
            )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto [fOut, tOut] = flow.GetOutput(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    auto R = GetR(metainfo);

    auto genEvt = isMC ? flow.GetBranchReadOnly<GenEvent>("genEvent") : nullptr;
    auto recEvt = flow.GetBranchReadOnly<RecEvent>("recEvent");
    auto pu     = flow.GetBranchReadOnly<PileUp  >("pileup"  );
    auto recJets = flow.GetBranchReadWrite<vector<RecJet>>("recJets");

    const auto tables = config.get<fs::path>("corrections.JES.tables"); // path to directory containing corrections in JSON format
    metainfo.Set<fs::path>("corrections", "JES", "tables", tables);
    if (!fs::exists(tables))
        BOOST_THROW_EXCEPTION( fs::filesystem_error("File does not exists.",
                    tables, make_error_code(errc::no_such_file_or_directory)) );
    auto campaign = config.get<string>("corrections.JES.campaign"); // calibration campaign
    metainfo.Set<string>("corrections", "JES", "campaign", campaign); // e.g. `Summer19UL18_RunA` (everything before "MC" or "DATA")
    string algo = GetAlgo(metainfo);

    ControlPlots::isMC = isMC;
    ControlPlots::verbose = steering & DT::verbose;
    ControlPlots raw("raw");
    vector<ControlPlots> calib { ControlPlots("nominal") };

    const bool applySyst = steering & DT::syst;

    if (applySyst)
    for (string source: JES_variations) {
        metainfo.Set<string>("variations", RecJet::ScaleVar, source + SysDown);
        metainfo.Set<string>("variations", RecJet::ScaleVar, source + SysUp);

        calib.push_back(ControlPlots(source + SysDown));
        calib.push_back(ControlPlots(source + SysUp));
    }

    // prepare correctionlib
    auto cset = correction::CorrectionSet::from_file(tables.string());

    string suffix = "AK" + to_string(R) + "PF" + algo;
    auto nomSF = GetCorrection<correction::CompoundCorrection::Ref>(
            cset->compound(), campaign, isMC ? "MC" : "DATA", "L1L2L3Res", suffix);
    cout << ScanCorrections(cset->compound(), nomSF->name()) << endl;
    // Example:
    // - Summer19UL18_RunA_V6_DATA_L1L2L3Res_AK4PFchs
    // - Summer19UL18_RunB_V6_DATA_L1L2L3Res_AK4PFchs
    // - Summer19UL18_RunC_V6_DATA_L1L2L3Res_AK4PFchs
    // - Summer19UL18_RunD_V6_DATA_L1L2L3Res_AK4PFchs
    // - Summer19UL18_V5_MC_L1L2L3Res_AK4PFchs

    if (steering & DT::verbose)
        cout << ScanCorrections(*cset) << endl;

    vector<correction::Correction::Ref> varSFs;
    varSFs.reserve(JES_variations.size());

    string short_campaign = GetShortCampaign(campaign);
    if (applySyst)
    for (string source: JES_variations)
        varSFs.push_back(GetCorrection<correction::Correction::Ref>(
                        *cset, short_campaign, "MC", source, suffix));

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        double weight = recEvt->weights.front();
        if (isMC) weight *= genEvt->weights.front();

        // CP before JES
        raw(*recJets, weight);

        for (auto& recJet: *recJets) {

            // sanity check
            if (recJet.scales.size() != 1 || recJet.scales.front() != 1)
                BOOST_THROW_EXCEPTION( DE::AnomalousEvent("Unexpected JES corrections", tIn) );

            // nominal value
            auto corr = nomSF->evaluate({recJet.area, recJet.p4.Eta(), recJet.p4.Pt(), pu->rho});
            cout << recJet.p4.Pt() << ' ' << corr << '\n';
            recJet.scales.front() = corr;

            if (!applySyst) continue;

            // load uncertainties in JECs
            recJet.scales.reserve(JES_variations.size()*2+1);
            for (const correction::Correction::Ref& varSF: varSFs) {
                auto var = varSF->evaluate({recJet.p4.Eta(), recJet.p4.Pt()}); /// \todo CorrPt?
                recJet.scales.push_back(corr*(1-var));
                recJet.scales.push_back(corr*(1+var));
            }
        }
        cout << flush;

        // CP after corrections
        for (size_t i = 0; i < calib.size(); ++i)
            calib.at(i)(*recJets, weight, i);

        sort(recJets->begin(), recJets->end(), pt_sort); // sort again the jets by pt
        if (steering & DT::fill) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);
    raw.Write(fOut);
    for (auto& c: calib)
        c.Write(fOut);

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of DAS namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options(
                            "Apply jet energy scale corrections. The correction factors "
                            "are stored in a dedicated vector in the reconstructed jets. "
                            "Note: systematic uncertainties should be applied either "
                            "to the data samples or to the MC samples, but not to both.",
                            DT::config | DT::split | DT::Friend | DT::syst);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("tables", "corrections.JES.tables",
                              "JSON file containing the corrections")
               .arg<string>("campaign", "corrections.JES.campaign",
                            "campaign (e.g. `Summer19UL18_RunA`)");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::JetEnergy::applyJEScorrections(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif

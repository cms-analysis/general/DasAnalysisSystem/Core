#include <cstdlib>
#include <cassert>
#include <iostream>
#include <fstream>
#include <vector>
#include <map>
#include <algorithm>
#include <functional>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/binnings.h"
#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <TTree.h>
#include <TFile.h>
#include <TString.h>
#include <TH1.h>
#include <TH3.h>

#include "common.h"
#include "Core/JEC/interface/JMEmatching.h"
#include "Core/JEC/interface/MakeResponseHistos.h"

#include <darwin.h>

using namespace std;
using namespace DAS::Normalisation;
namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::JetEnergy {

////////////////////////////////////////////////////////////////////////////////
/// Get JER differential resolution and balance.
void getJetResponse
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
         const fs::path& output, //!< name of output ROOT file (histograms)
         const pt::ptree& config, //!< config file from `DT::Options`
         const int steering, //!< steering parameters from `DT::Options`
         const DT::Slice slice = {1,0} //!< slices for running
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto [fOut, tOut] = flow.GetOutput(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    if (!isMC)
        BOOST_THROW_EXCEPTION( DE::BadInput("Only MC may be used as input.", metainfo) );

    const auto R = metainfo.Get<int>("flags", "R");

    JMEmatching<>::maxDR = R/10./2.;
    cout << "Radius for matching: " << JMEmatching<>::maxDR << endl;

    auto rEv = flow.GetBranchReadOnly<RecEvent>("recEvent");
    const auto hiPU = config.get<bool>("skims.pileup");
    auto pileUp = hiPU ? flow.GetBranchReadOnly<PileUp>("pileup") : nullptr;

    auto recjets = flow.GetBranchReadOnly<vector<RecJet>>("recJets");
    auto genjets = flow.GetBranchReadOnly<vector<GenJet>>("genJets");

    unique_ptr<TH3> inclResp = makeRespHist("inclusive", "JER"); // Response inclusive in rho
    vector<unique_ptr<TH3>> rhobinsResp, // Response in bins of rho
                            mubinsResp;  // Response in bins of mu (true pileup)

    const auto year = metainfo.Get<int>("flags", "year");
    if (hiPU) {
        rhobinsResp.reserve(nRhoBins.at(year));
         mubinsResp.reserve(nRhoBins.at(year));
        for (int rhobin = 1; rhobin <= nRhoBins.at(year); ++rhobin) {
            rhobinsResp.push_back( makeRespHist( Form("rhobin%d", rhobin) , "JER") );
             mubinsResp.push_back( makeRespHist( Form( "mubin%d", rhobin) , "JER") );
        }
    }

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        auto recWgt = rEv->weights.front();
        optional<size_t> irho, imu;
        if (hiPU) {
            static const size_t nrho = static_cast<size_t>(nRhoBins.at(year));
            for (irho = 0; pileUp->rho > rho_edges.at(year).at(*irho+1) && *irho < nrho; ++*irho);
            if (*irho >= nrho)
                BOOST_THROW_EXCEPTION( DE::AnomalousEvent(Form("irho = %lu > nRhoBins.at(%d) = %d",
                                                            *irho, year, nRhoBins.at(year)), tIn) );
            static const size_t imuMax = mubinsResp.size()-1;
            imu = static_cast<size_t>(pileUp->GetTrPU())/10;
            *imu = min(*imu,imuMax);
        }

        // Matching
        if (genjets->size() > 3) genjets->resize(3); // Keep only three leading gen jets
        JMEmatching matching(*recjets, *genjets);
        matching.match_its.resize(min(matching.match_its.size(),3lu));

        for (auto& [rec_it,gen_it]: matching.match_its) {
            auto ptrec = rec_it->CorrPt();
            auto ptgen = gen_it->p4.Pt();

            // Fill resolution from smearing
            auto response = ptrec/ptgen;
            auto etarec   = abs( rec_it->p4.Eta() );

            inclResp->Fill(ptgen, etarec, response, recWgt);
            if (irho) rhobinsResp.at(*irho)->Fill(ptgen, etarec, response, recWgt);
            if (imu )  mubinsResp.at(*imu )->Fill(ptgen, etarec, response, recWgt);

             /// \todo with gen level weight too?
        } // End of for (pairs) loop
    } // End of while (event) loop

    // Creating directory hierarchy and saving histograms
    fOut->cd();
    inclResp->SetDirectory(fOut);
    inclResp->SetTitle("Response (inclusive)");
    inclResp->Write("Response");
    if (hiPU)
    for (int irho = 0; irho < nRhoBins.at(year); ++irho) {

        /// \todo remove upper boundaries from titles of last bins

        TString title = Form("%.2f < #rho < %.2f", rho_edges.at(year).at(irho), rho_edges.at(year).at(irho+1));
        fOut->cd();
        TDirectory * d_rho = fOut->mkdir(Form("rhobin%d", irho+1), title);
        d_rho->cd();
        rhobinsResp.at(irho)->SetDirectory(fOut);
        rhobinsResp.at(irho)->SetTitle("Response (" + title + ")");
        rhobinsResp.at(irho)->Write("Response");

        title = Form("%d < #mu < %d", irho*10, (irho+1)*10);
        fOut->cd();
        TDirectory * d_mu = fOut->mkdir(Form("mubin%d", irho+1), title);
        d_mu->cd();
        mubinsResp.at(irho)->SetDirectory(fOut);
        mubinsResp.at(irho)->SetTitle("Response (" + title + ")");
        mubinsResp.at(irho)->Write("Response");
    }

    metainfo.Set<bool>("git", "complete", true);

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of DAS::JetEnergy namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options("Get response in bins of pt and eta.", DT::split | DT::config);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<bool>("pileup", "skims.pileup", "flag to activate the binning in mu and rho");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::JetEnergy::getJetResponse(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif

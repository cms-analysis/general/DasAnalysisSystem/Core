#pragma once

#include "common.h"

#include <map>
#include <memory>
#include <vector>

#include <TDirectory.h>
#include <TH3.h>
#include <TList.h>
#include <TString.h>

#include <darwin.h>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Variation.h"

namespace DAS::JetEnergy {

using namespace std;
namespace DT = Darwin::Tools;

////////////////////////////////////////////////////////////////////////////////
/// Get scale variations
///
/// \note one may want to get the Rochester variations as well for Z+jet balance
/// 
/// \note this function may be redundant with `DAS::Unfolding::GetVariations()`
vector<DAS::Uncertainties::Variation> GetScaleVariations (const DT::MetaInfo& metainfo,
                                                          bool applySyst)
{
    vector<DAS::Uncertainties::Variation> variations;
    variations.emplace_back("nominal","nominal");
    if (!applySyst) return variations;

    const TList * groupContents = metainfo.List("variations", DAS::RecJet::ScaleVar);
    if (!groupContents)
        BOOST_THROW_EXCEPTION( invalid_argument(groupContents->GetName()) );

    size_t i = 0;
    for (const TObject * obj: *groupContents) {
        const auto os = dynamic_cast<const TObjString*>(obj);
        if (!os) BOOST_THROW_EXCEPTION( invalid_argument(obj->GetName()) );
        TString s = os->GetString();
        s.ReplaceAll(SysUp, "");
        s.ReplaceAll(SysDown, "");
        if (find(begin(JES_variations), end(JES_variations), s) != end(JES_variations))
            variations.emplace_back(DAS::RecJet::ScaleVar, os->GetString(), ++i);
    }
    return variations;
}

////////////////////////////////////////////////////////////////////////////////
/// Balance histogram for a given variation
class Balance {

    TDirectory * d; //!< directory in output ROOT file
    unique_ptr<TH3D> asymmetry; //!< asymmetry histogram
    unique_ptr<TH2D> response; //!< response

public:
    DAS::Uncertainties::Variation v; //!< systematic variation

    Balance (TDirectory * d,
             const DAS::Uncertainties::Variation& v,
             const vector<double>& pt_edges,
             const vector<double>& y_edges) :
        d(d), v(v)
    {
        int nBins = 120;
        auto balBins = getBinning(nBins, -1.2, 1.2);
        assertValidBinning(balBins);

        int nPtBins = pt_edges.size()-1,
            nYbins  =  y_edges.size()-1;

        TString dirname = d->GetName();
        TString hname = dirname + '_' + v.group + '_' + v.name;
        asymmetry = make_unique<TH3D>(hname, v.name, nBins, balBins.data(),
                                                     nPtBins, pt_edges.data(),
                                                     nYbins, y_edges.data());

        balBins = getBinning(nBins, 0, 1.2);
        response = make_unique<TH2D>("response_" + hname, v.name, nBins, balBins.data(),
                                                                  nYbins, y_edges.data());
    }

    ////////////////////////////////////////////////////////////////////////////
    /// Calculates balance for two objects following the definition of the Run 1
    /// JetMET reference publication for dijet systems.
    void operator() (const DAS::AbstractPhysicsObject& tag,
                     const DAS::AbstractPhysicsObject& probe,
                     const double ev_w)
    {
        float absy_probe = probe.AbsRap(v);

        float pt_tag   = tag  .CorrPt(v),
              pt_probe = probe.CorrPt(v);

        double jw = tag  .Weight(v)
                  * probe.Weight(v);

        response->Fill(pt_probe/pt_tag, absy_probe, ev_w*jw);

        // \todo does this also apply to Z+jet balance?
        //       (e.g. not in 1804.05252)
        float pt_dif = (pt_tag - pt_probe),
              pt_ave = (pt_tag + pt_probe) * 0.5;

        float asym = pt_dif / pt_ave;

        cout << asym << endl;

        asymmetry->Fill(asym, pt_ave, absy_probe, ev_w*jw);
    }

    void Write ()
    {
        d->cd();
        TString dirname = d->GetName();

        asymmetry->SetDirectory(d);
        TString hname = asymmetry->GetName();
        hname.ReplaceAll(dirname + '_', "");
        asymmetry->Write(hname);

        response->SetDirectory(d);
        hname = response->GetName();
        hname.ReplaceAll(dirname + '_', "");
        response->Write(hname);
    }
};

} // end of DAS::JetEnergy namespace

#include <cassert>
#include <cstdlib>

#include <algorithm>
#include <functional>
#include <filesystem>
#include <iostream>
#include <limits>
#include <random>
#include <vector>

#include <TChain.h>
#include <TDirectory.h>
#include <TH2.h>
#include <TFile.h>

#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/ControlPlots.h"

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Format.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/JEC/interface/JMEmatching.h"

#include <correction.h>
#include <darwin.h>

#include "common.h"

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::JetEnergy {

////////////////////////////////////////////////////////////////////////////////
/// Get JER smearing corrections in the form of factors to store in the `scales`
/// member of `RecJet`.
void applyJERsmearing
            (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
             const fs::path& output, //!< name of output ROOT files (n-tuples)
             const pt::ptree& config, //!< config file from `DT::Options`
             const int steering, //!< steering parameters from `DT::Options`
             const DT::Slice slice = {1,0} //!< slices for running
            )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto [fOut, tOut] = flow.GetOutput(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    auto R = GetR(metainfo);
    if (!isMC) BOOST_THROW_EXCEPTION( DE::BadInput("Only MC may be used as input.",
                                      make_unique<TFile>(inputs.front().c_str() )) );
    JMEmatching<vector<RecJet>, vector<GenJet>>::maxDR = (R/10.)/2;

    auto genEvt = flow.GetBranchReadOnly<GenEvent>("genEvent");
    auto recEvt = flow.GetBranchReadOnly<RecEvent>("recEvent");
    auto pu     = flow.GetBranchReadOnly<PileUp  >("pileup"  );
    auto genJets = flow.GetBranchReadOnly <vector<GenJet>>("genJets");
    auto recJets = flow.GetBranchReadWrite<vector<RecJet>>("recJets");

    const auto& tables = config.get<fs::path>("corrections.JER.tables");
    metainfo.Set<fs::path>("corrections", "JER", "tables", tables);
    if (!fs::exists(tables))
        BOOST_THROW_EXCEPTION( fs::filesystem_error("File does not exists.",
                    tables, make_error_code(errc::no_such_file_or_directory)) );
    const auto campaign = config.get<string>("corrections.JER.campaign"); // correction campaign
    metainfo.Set<string>("corrections", "JER", "campaign", campaign);
    const auto& core_width = config.get<float>("corrections.JER.core_width");

    metainfo.Set<float>("corrections", "JER", "core_width", core_width);
    string algo = GetAlgo(metainfo);

    ControlPlots::isMC = isMC;
    ControlPlots raw("raw");
    vector<ControlPlots> calib { ControlPlots("nominal") };

    const bool applySyst = steering & DT::syst;
    vector<string> vars {"nom"}; // keyword for correctionlib
    /// \todo investigate JER uncertainty breakdown (see [Core#35](https://gitlab.cern.ch/cms-analysis/general/DasAnalysisSystem/Core/-/issues/35))
    if (applySyst) {
        metainfo.Set<string>("variations", RecJet::ScaleVar, "JER" + SysDown);
        metainfo.Set<string>("variations", RecJet::ScaleVar, "JER" + SysUp);

        calib.push_back(ControlPlots("JER" + SysDown));
        calib.push_back(ControlPlots("JER" + SysUp));

        vars.push_back("up");
        vars.push_back("down");
    }

    // preparing correctionlib object
    auto cset = correction::CorrectionSet::from_file(tables.string());
    string suffix = "AK" + to_string(R) + "PF" + algo;

    auto SF = GetCorrection<correction::Correction::Ref>(
                            *cset, campaign, "MC", "ScaleFactor", suffix),
         JER = GetCorrection<correction::Correction::Ref>(
                            *cset, campaign, "MC", "PtResolution", suffix);

    // a few lambda functions
    auto getSFs = [&SF,&vars](const RecJet& recjet) {
        vector<float> sfs;
        for (const string var: vars)
            sfs.push_back(SF->evaluate({recjet.p4.Eta(), var}));
        return sfs;
    };
    auto applySFs = [applySyst](RecJet& recjet, const vector<float>& sfs) {
        float nom_scale = recjet.scales.front();
        for (float& scale: recjet.scales)
            scale *= sfs.front();

        if (applySyst) {
            recjet.scales.push_back(nom_scale*sfs[1]);
            recjet.scales.push_back(nom_scale*sfs[2]);
        }
    };
    auto getJER = [pu,&JER](const RecJet& recjet) {
        return JER->evaluate({recjet.p4.Eta(), recjet.CorrPt(), pu->rho});
    };

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;
        cout << "===============" << endl;

        raw(*genJets, genEvt->weights.front());
        raw(*recJets, genEvt->weights.front() * recEvt->weights.front());

        cout << "Running matching" << endl;
        JMEmatching matching(*recJets, *genJets);
        auto fake_its = matching.fake_its; // we extract it because we will modify it

        cout << "Applying scaling smearing for jets matched in the core of the response" << endl;
        for (const auto& [rec_it, gen_it]: matching.match_its) {
            cout << "---------------" << endl;

            float genpt = gen_it->p4.Pt(),
                  recpt = rec_it->CorrPt();

            float response = recpt / genpt,
                  resolution = getJER(*rec_it);

            cout << "|- genpt = " << genpt << '\n'
                 << "|- recpt = " << recpt << '\n'
                 << "|- response = " << response << '\n'
                 << "|- resolution = " << resolution << endl;

            vector<float> sfs = getSFs(*rec_it);
            if (abs(response - 1) > core_width * resolution) {
                cout << "|- match in the tail" << endl;
                fake_its.push_back(rec_it); // left for later, with stochastic smearing
                continue;
            }
            cout << "|- match in the core" << endl;

            for (float& sf: sfs) {
                cout << "|- " << sf;
                sf = max(0.f, 1+(sf-1)*(recpt-genpt)/recpt);
                cout << " -> " << sf << endl;
            }

            cout << "|- applying SFs" << endl;
            applySFs(*rec_it, sfs);
        }

        cout << "Applying stochastic smearing for all other jets "
                "(either unmatched, or matched in the tails of the response)" << endl;
        for (auto& fake_it: fake_its) {

            float resolution = getJER(*fake_it);
            normal_distribution<float> d{0,resolution};
            static mt19937 m_random_generator(metainfo.Seed<20394242>(slice));
            float response = d(m_random_generator);

            cout << "|- recpt = " << fake_it->CorrPt() << '\n'
                 << "|- response = " << response << '\n'
                 << "|- resolution = " << resolution << endl;

            vector<float> sfs = getSFs(*fake_it);
            for (float& sf: sfs) {
                cout << "|- " << sf;
                sf = max(0.f, 1+response*sqrt(max(0.f, sf*sf-1)));
                cout << " -> " << sf << endl;
            }

            cout << "|- applying SFs" << endl;

            applySFs(*fake_it, sfs);
        }

        cout << "Sorting by descending pt" << endl;
        sort(recJets->begin(), recJets->end(), pt_sort);

        for (size_t i = 0; i < calib.size(); ++i) {
            calib.at(i)(*genJets, genEvt->weights.front());
            calib.at(i)(*recJets, genEvt->weights.front() * recEvt->weights.front(), i); /// \todo fix in case of offset (e.g. JES applied on MC)
        }

        cout << "Filling" << endl;
        if (steering & DT::fill) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);
    raw.Write(fOut);
    for (auto& c: calib)
        c.Write(fOut);

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of DAS::JetEnergy namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options(
            "Apply jet energy smearing correction. In practice, the correction factors "
            "are stored in a dedicated vector in the reconstructed jets. To apply the "
            "pure stochastic (scaling) smearing, use a null (infinite) core size; hybrid "
            "smearing (scaling in core and stochastic in tails) is recommended. Note: JME "
            "recommends 3 sigmas, but Run 2 studies suggest not more than 2 sigmas.",
            DT::config | DT::split | DT::Friend | DT::syst);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("tables", "corrections.JER.tables",
                              "JSON file containing the corrections")
               .arg<string>("campaign", "corrections.JER.campaign",
                            "campaign (e.g. `Summer19UL18_RunA`)")
               .arg<float>("core_width", "corrections.JER.core_width",
                           "width of the Gaussian core in units of sigma "
                           "(for the matching algorithm)");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::JetEnergy::applyJERsmearing(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif

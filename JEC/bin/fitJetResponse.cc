#include <cmath>
#include <cassert>
#include <iostream>
#include <map>
#include <filesystem>

#include <TString.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TF1.h>
#include <TKey.h>

#include "Core/CommonTools/interface/DASOptions.h"

#include "Core/JEC/interface/resolution.h"
#include "Core/JEC/interface/DoubleCrystalBall.h"

#include "common.h"
#include "fit.h"

#include <darwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::JetEnergy {

////////////////////////////////////////////////////////////////////////////////
/// Provide a new binning ensuring a sufficient number of entries in each bin
/// to assume Gaussian uncertainties.
///
/// For a gaussian distribution, as it is assumed for the number of events in
/// each bin, the error is given as sqrt(n) where n the number of
/// events in the bin. Assuming both the bin content and error have been
/// normalised, by taking the division of the (BinContent/BinError)^2 ~ n,
/// one can estimate the number of events in that bin.
///
/// There is no special treatment for negative weights, as the samples used to
/// develop the code have no such weight. A warning is however displayed in
/// case negative bins are found.
vector<double> GetMergedBinning (TH1 * h, const int threshold = 30)
{
    const int N = h->GetNbinsX();
    vector<double> edges(1,0);
    edges.reserve(N);
    int sum = 0;
    for (int i = 1; i <= N; ++i) {
        auto content = h->GetBinContent(i);
        if (content == 0) continue;
        if (content < 0)
            cerr << orange << "Found negative bins in "
                           << h->GetName() << def << '\n';
        auto error = h->GetBinError(i);
        sum += pow(content/error,2);
        if (sum < threshold) continue;
        sum = 0;
        edges.push_back(h->GetBinLowEdge(i+1));
    }
    if (edges.back() != h->GetBinLowEdge(N+1))
        edges.push_back(h->GetBinLowEdge(N+1));

    return edges;
}

////////////////////////////////////////////////////////////////////////////////
/// Find the positions of the extrema, starting from the mean and within a
/// given range (to avoid regions with too large fluctuations).
pair<float,float> findDLogExtrema (const unique_ptr<TH1>& DLog,
                                   const double mu,
                                   const pair<float,float> Range)
{
    const int m = DLog->FindBin(mu);

    // find maximum on the LHS
    int imax = m;
    for (int i = m; i >= DLog->FindBin(Range.first); --i)
        if (DLog->GetBinContent(i) > DLog->GetBinContent(imax)) imax = i;
    float x1 = DLog->GetBinLowEdge(imax+1);

    // find minimum on the RHS
    int imin = m;
    for (int i = m; i <= DLog->FindBin(Range.second); ++i)
        if (DLog->GetBinContent(i) < DLog->GetBinContent(imin)) imin = i;
    float x2 = DLog->GetBinLowEdge(imin);

    return {x1,x2};
}

////////////////////////////////////////////////////////////////////////////////
/// Round number value to precision.
/// Recover "lost" precision.
inline double round_to(double value, double precision = 1.0)
{
    return round(value / precision) * precision;
}

////////////////////////////////////////////////////////////////////////////////
/// Performs a double-sided Crystal-Ball fit of the given response step by step.
/// Three methods are provided to fit the gaussian core and each of the power-
/// law tails.
struct ResponseFit : public AbstractFit {

    enum {
        N = 0,
        MU, SIGMA,
        KL, NL, KR, NR,
        NPARS
    }; //!< parameter indices

    enum Status {
        failed     = 0b0000,
        core       = 0b0001,
        LHStail    = 0b0010,
        RHStail    = 0b0100,
    };

    ////////////////////////////////////////////////////////////////////////////
    /// Preparing for fit, setting ranges.
    /// Assuming response is centered around 1
    ResponseFit (const unique_ptr<TH1>& h, ostream& cout) :
        AbstractFit(h, cout, NPARS)
    {
        // Calculate 1st & 2nd derivatives of the log(h)
        unique_ptr<TH1> logd  = DerivativeFivePointStencil(h, safelog), // 1st derivative of log(h)
                        logdd = DerivativeFivePointStencil(logd      ); // 2nd derivative of log(h)
        logd->Smooth();
        logd->Write("logd");
        logdd->Write("logdd");

        double mu      = h->GetMean(),
               sigma   = h->GetStdDev();

        float rmin = 0.65, rmax = 1.5; /// \todo tune range?
        interval = findDLogExtrema(logd, mu, {rmin,rmax});

        // Initial estimate on the tail parameters
        static auto LHS = make_unique<TF1>("tail", "[0]*exp( [1]*x)", 0., rmin),
                    RHS = make_unique<TF1>("tail", "[0]*exp(-[1]*x)", rmax, 2.);
        h->Fit(LHS.get(), "N0QR");
        h->Fit(RHS.get(), "N0QR");

        f = make_unique<TF1>("f", DAS::DoubleCrystalBall::Distribution(),
                                interval.first, interval.second, NPARS);
        p[N    ] = 1.;                      e[N    ] = 0.001;
        p[MU   ] = mu;                      e[MU   ] = h->GetMeanError();
        p[SIGMA] = sigma;                   e[SIGMA] = h->GetStdDevError();
        p[KL   ] = interval.first;          e[KL   ] = sigma;
        p[NL   ] = LHS->GetParameter(1);    e[NL   ] = LHS->GetParError(1);
        p[KR   ] = interval.second;         e[KR   ] = sigma;
        p[NR   ] = RHS->GetParameter(1);    e[NR   ] = RHS->GetParError(1);
        f->SetParameters(p);
        f->SetParErrors (e);

        f->SetParName(N    ,  "N"     );
        f->SetParName(MU   ,  "#mu"   );
        f->SetParName(SIGMA,  "#sigma");
        f->SetParName(KL   ,  "k_{L}" );
        f->SetParName(NL   ,  "N_{L}" );
        f->SetParName(KR   ,  "k_{R}" );
        f->SetParName(NR   ,  "N_{R}" );

        f->SetNpx(2000); // Increase points used to draw TF1 object when saving

        // The Integral of a DSCB function using the DSCB parameters from fit.
        auto IDSCB = make_unique<TF1>("DSCB_Integral", DAS::DoubleCrystalBall::Integral(), 0., 2., NPARS-1);
        IDSCB->SetParameters(p);
        IDSCB->SetLineColor(f->GetLineColor());
        IDSCB->SetLineStyle(f->GetLineStyle());

        IDSCB->Write("DSCB_Integral");
    }

    void gausCore ();
    void SSCB ();
    void DSCB ();

    inline bool good () const override { return AbstractFit::good() && *chi2ndf < 1000; }

private:
    void Write (const char *) override;
};

////////////////////////////////////////////////////////////////////////////////
/// Get Gaussian core parameters
void ResponseFit::gausCore ()
{
    f->SetParLimits(N    , 0.9, 1.1);
    f->SetParLimits(MU   , p[MU]-0.5*p[SIGMA], p[MU]+0.5*p[SIGMA]);
    f->SetParLimits(SIGMA, 0.5*p[SIGMA], 2*p[SIGMA]);
    f->FixParameter(KL, p[KL]);
    f->FixParameter(NL, p[NL]);
    f->FixParameter(KR, p[KR]);
    f->FixParameter(NR, p[NR]);

    fit(interval, __func__);

    if (!chi2ndf) return;
    status |= Status::core;
    Write(__func__);
}

////////////////////////////////////////////////////////////////////////////////
/// Crystal-Ball fit with tail on the LHS
///
/// The fit performance is scanned for all possible LHS boundaries.
void ResponseFit::SSCB ()
{
    f->SetParLimits(N    , min(0.9,p[N]), max(1.1,p[N]));
    f->SetParLimits(MU   , p[MU]-0.1*p[SIGMA], p[MU]+0.1*p[SIGMA]);
    f->SetParLimits(SIGMA, 0.5*p[SIGMA], 1.5*p[SIGMA]);

    auto lastChi2ndf = chi2ndf;
    float R = interval.second;
    int BinR = h->FindBin(R);
    for (int BinL = h->FindBin(interval.first); BinL > 0; --BinL) {
        if (h->GetBinContent(BinL) == 0) continue;
        if (BinR - BinL < f->GetNumberFreeParameters()) continue; // Fit range should include more bins than the number of params we want to fit
        float L = h->GetBinLowEdge(BinL);
        if (L >= p[MU]) continue; // if true, range doesnt make sense, skip fit

        f->SetParLimits(KL, L, p[MU]);
        f->SetParLimits(NL, 1.001, min(1e3,p[NL]*2));
        fit({L, R}, __func__); // Fit response distribution in given range
    }

    if (!chi2ndf || *chi2ndf >= *lastChi2ndf) return;
    status |= Status::LHStail;
    Write(__func__);
}

////////////////////////////////////////////////////////////////////////////////
/// Crystal-Ball fit with tail on the RHS
///
/// The fit performance is scanned for all possible RHS boundaries.
void ResponseFit::DSCB ()
{
    f->SetParLimits(N, min(0.9,p[N]), max(1.1,p[N]));
    if ((status & Status::LHStail) == Status::LHStail)
        f->SetParLimits(KL, p[KL]-0.5*p[SIGMA], p[KL]+0.5*p[SIGMA]);
    else {
        f->FixParameter(KL, p[KL]);
        f->FixParameter(NL, p[NL]);
    }

    auto lastChi2ndf = chi2ndf;
    float L = interval.first;
    int BinL = h->FindBin(L);
    for (int BinR = h->FindBin(interval.second); BinR <= h->GetNbinsX(); ++BinR) {
        if (h->GetBinContent(BinR) == 0) continue;
        if (BinR - BinL < f->GetNumberFreeParameters()) continue; // Fit range should include more bins than the number of params we want to fit
        float R = h->GetBinLowEdge(BinR+1);
        if (R <= p[MU]) continue; // if true, range doesnt make sense, skip fit

        f->SetParLimits(KR, p[MU], R);
        f->SetParLimits(NR, 1.001, min(1e3,p[NR]*2));
        fit({L, R}, __func__); // Fit response distribution in given range
    }

    if (!chi2ndf || *chi2ndf == *lastChi2ndf) return;
    status |= Status::RHStail;
    Write(__func__);
}

////////////////////////////////////////////////////////////////////////////////
/// Write best current estimate in current directory.
///
/// Four color codes are used to categorize fits on their final state
/// (after all 3 fits are performed):
/// - Red for a failed fit (all fit stages failed, or fit quality did not pass some criteria, e.g., chi2/ndf)
/// - Magenta a successful Gaussian core fit (fits at stage 2 and 3 failed)
/// - Orange a successful SSCB fit (stage 1 fit could either succeed or fail, and stage 3 failed)
/// - Green a successful DSCB fit (stage 1 and 2 fits could have either failed or succeded)
void ResponseFit::Write (const char * name)
{
    bool goodCore = (status & Status::core   ) == Status::core   ;
    bool goodLHS  = (status & Status::LHStail) == Status::LHStail;
    bool goodRHS  = (status & Status::RHStail) == Status::RHStail;
    if      (goodLHS &&  goodRHS) f->SetLineColor(kGreen+1);
    else if (goodLHS xor goodRHS) f->SetLineColor(kOrange+1);
    else if (goodCore           ) f->SetLineColor(kMagenta+1);
    else                          f->SetLineColor(kRed+1);

    AbstractFit::Write(name);

    auto dlog = make_unique<TF1>("DLog", DAS::DoubleCrystalBall::DLog(), 0., 2., NPARS-1);
    dlog->SetParameters(p);
    dlog->SetLineColor(f->GetLineColor());
    dlog->SetLineStyle(f->GetLineStyle());

    static TString dlogName = "DLog";
    dlog->Write(dlogName + name);
}

///////////////////////////////////////////////////////////////////////////////
/// Creating cumulative response histogram.
/// Given a response 1D histogram, it adds to
/// a bin the content of all previous bins.
TH1 * CumulativeResponse (TH1 * res1D)
{
    double cont = 0;
    const int NptBins = res1D->GetNbinsX();
    for (int n = 1; n <= NptBins; ++n) {
        cont+=res1D->GetBinContent(n);
        res1D->SetBinContent(n, cont);
    }
    return res1D;
}

////////////////////////////////////////////////////////////////////////////////
/// Projection 2D histograms onto 1D histograms and fit response
///
/// Fit is currently performed in 3 stages:
/// 1. Fit the Gaussian core to get a better estimation of the mean and sigma.
/// 2. Extend the fit right range, transposing to a Single-Sided Crystal Ball fit,
///    using the mean and sigma better define the fit parameters.
/// 3. Extend the fit left range, transposing to a Double-Sided Crystal Ball fit.
///
/// Fit iteratively on each stage by adjusting the fit range so that you minimize
/// the amount of failed fits.
void FitResponseByBin (TDirectory * dir,
                       unique_ptr<TH2> res2D,
                       const int steering)
{
    dir->cd();

    auto h_ptBins = unique_ptr<TH1>( dynamic_cast<TH1*>(res2D->ProjectionX("h_ptBins", 1, -1)) ); // Only for pt binnning extraction
    h_ptBins->GetXaxis()->SetTitle("p_{T}^{gen}");

    // Declaring histograms (control plots)
    map<TString, unique_ptr<TH1>> hists;
    for (TString name: {"meanCore", "sigmaCore", // Obtained from DCB fit
                        "kLtail", "kRtail", // DCB tails transitions
                        "nLtail", "nRtail", // DCB tails slope
                        "Lcore", "Rcore", // estimates from derivative
                        "chi2ndf", "Median"}) {
        // for successful fits
        hists.insert( {name, unique_ptr<TH1>(dynamic_cast<TH1*>( h_ptBins->Clone(name) )) } );
        hists[name]->Reset();
        hists[name]->SetTitle(name);
        hists[name]->SetDirectory(nullptr);

        // for failed fits
        name += "Fail";
        hists.insert( {name, unique_ptr<TH1>(dynamic_cast<TH1*>( h_ptBins->Clone(name) )) } );
        hists[name]->Reset();
        hists[name]->SetTitle(name);
        hists[name]->SetDirectory(nullptr);
    }

    int Npt = res2D->GetXaxis()->GetNbins();
    for (int ptbin = 1; ptbin <= Npt; ++ptbin) { // Repeat fit procedure for all response distributions
        double lowEdge = res2D->GetXaxis()->GetBinLowEdge(ptbin),
               upEdge  = res2D->GetXaxis()->GetBinUpEdge (ptbin);
        const char * level     = TString( res2D->GetXaxis()->GetTitle() ).Contains("gen") ? "gen" : "rec";
        const char * hlt_level = TString( res2D->GetXaxis()->GetTitle() ).Contains("HLT") ? "HLT" : level;
        TString title    = Form("%.0f < p_{T}^{%s} < %.0f", lowEdge, hlt_level, upEdge),
                stdPrint = TString( res2D->GetTitle() );
        stdPrint.ReplaceAll("zx projection", "");
        stdPrint.ReplaceAll(")", "");
        stdPrint += ", " + title + ")";
        cout << stdPrint << def << endl;
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        auto original = unique_ptr<TH1>( dynamic_cast<TH1*>(res2D->ProjectionY("original", ptbin, ptbin)) ); // Response distribution
        if (original->GetEntries() < 100) {
            cout << orange << "Skipped due to low number of events!" << def << endl;
            continue;
        }

        vector<double> edges = GetMergedBinning(original.get());
        auto h = unique_ptr<TH1>( original->Rebin(edges.size()-1, "h", edges.data()) );
        const char * h_title = TString( res2D->GetXaxis()->GetTitle() ).Contains("HLT") ? "HLT bin low edge" : "gen";
        h->GetXaxis()->SetTitle(Form("#frac{p_{T}^{rec}}{p_{T}^{%s}}", h_title));
        //h->Rebin(2);

        TDirectory * d_pt = dir->mkdir(Form("ptbin%d", ptbin), title);
        d_pt->cd();
        h->SetTitle(title);
        h->SetDirectory(d_pt);
        float normalisation = abs( h->Integral() );
        h->Scale(1.f/normalisation, "width");
        //cout << "normalisation = " << h->Integral() << endl;
        h->Write();

        // Calculating median reponse and Interquartile range (IQR) error
        // While the mean has standard error estimates, the median uses the
        // interquartile range to estimate the robustness of the median against outliers or skewed data.
        // https://en.wikipedia.org/wiki/Interquartile_range
        vector<double> probabilities{0.25, 0.5, 0.75};
        const int nQuantiles = probabilities.size();
        vector<double> quantiles(nQuantiles);
        h->GetQuantiles(nQuantiles, quantiles.data(), probabilities.data());
        double& median = quantiles[1];
        double IQRError = M_PI / 2.0 * (quantiles[2] - quantiles[0]) / sqrt(h->GetEntries());

        TString hlt_title = Form("%.0f < p_{T}^{%s} < %.0f", lowEdge, hlt_level, upEdge);

        // Calculating Cumulative response histogram and storing it to directory
        auto hc_tmp = dynamic_cast<TH1*>( h->Clone("hc_tmp") );
        auto hc = CumulativeResponse(std::move(hc_tmp)); // Cumulative response
        hc->SetNameTitle("h_cumulative", hlt_title);
        hc->SetDirectory(d_pt);
        hc->Write();

        // the complex fit is split over several steps using a ResponseFit object
        ResponseFit resp(h, cout);

        hists.at("Lcore")->SetBinContent(ptbin, resp.p[ResponseFit::KL]);
        hists.at("Rcore")->SetBinContent(ptbin, resp.p[ResponseFit::KR]);

        resp.gausCore();
        resp.SSCB();
        resp.DSCB();

        TString suffix = resp.good() ? "" : "Fail";

        // Save fit parameters into control plots
        hists.at( "meanCore" + suffix)->SetBinContent(ptbin, resp.p[ResponseFit::MU]);
        hists.at( "meanCore" + suffix)->SetBinError  (ptbin, resp.e[ResponseFit::MU]);
        hists.at("sigmaCore" + suffix)->SetBinContent(ptbin, resp.p[ResponseFit::SIGMA]);
        hists.at("sigmaCore" + suffix)->SetBinError  (ptbin, resp.e[ResponseFit::SIGMA]);

        hists.at("kLtail" + suffix)->SetBinContent(ptbin, resp.p[ResponseFit::KL]);
        hists.at("kLtail" + suffix)->SetBinError  (ptbin, resp.e[ResponseFit::KL]);
        hists.at("nLtail" + suffix)->SetBinContent(ptbin, resp.p[ResponseFit::NL]);
        hists.at("nLtail" + suffix)->SetBinError  (ptbin, resp.e[ResponseFit::NL]);
        hists.at("kRtail" + suffix)->SetBinContent(ptbin, resp.p[ResponseFit::KR]);
        hists.at("kRtail" + suffix)->SetBinError  (ptbin, resp.e[ResponseFit::KR]);
        hists.at("nRtail" + suffix)->SetBinContent(ptbin, resp.p[ResponseFit::NR]);
        hists.at("nRtail" + suffix)->SetBinError  (ptbin, resp.e[ResponseFit::NR]);

        hists.at("chi2ndf" + suffix)->SetBinContent(ptbin, resp.chi2ndf   .value_or(0));
        hists.at("chi2ndf" + suffix)->SetBinError  (ptbin, resp.chi2ndfErr.value_or(0));

        hists.at("Median" + suffix)->SetBinContent(ptbin, median);
        hists.at("Median" + suffix)->SetBinError(ptbin, IQRError);
    }

    dir->cd();
    TString title = res2D->GetTitle();
    title.ReplaceAll("Response distribution", "");
    title.ReplaceAll("zx projection", "");
    for (auto&& h: hists) {
        h.second->SetDirectory(dir);
        h.second->SetTitle(h.second->GetTitle() + title);
        h.second->Write();
    }
}

////////////////////////////////////////////////////////////////////////////////
/// Copy directory structure.
/// Given an input file dIn recursively loop over all directory
/// hierarchy/contents and reproduce it in output file dOut.
/// For each TH3 instance found containing the response
/// perform a DCB fit by calling FitResponseByBin() function.
void loopDirsFromGetResponse (TDirectory * dIn,
                              TDirectory * dOut,
                              const int steering,
                              const DT::Slice slice)
{
    for (const auto&& obj: *(dIn->GetListOfKeys())) {
        auto const key = dynamic_cast<TKey*>(obj);
        if ( key->IsFolder() ) {
            auto ddIn = dynamic_cast<TDirectory*>( key->ReadObj() );
            if (ddIn == nullptr) continue;
            TDirectory * ddOut = dOut->mkdir(ddIn->GetName(), ddIn->GetTitle());
            ddOut->cd();
            cout << bold << ddIn->GetPath() << def << endl;

            loopDirsFromGetResponse(ddIn, ddOut, steering, slice);
        }
        else if ( TString( key->ReadObj()->ClassName() ).Contains("TH3") ) {
            // Assuming for axis, x: pt bins, y: eta bins, z: resolution bins
            auto hist3D = unique_ptr<TH3>( dynamic_cast<TH3*>(key->ReadObj()) );
            hist3D->SetDirectory(0);
            bool isAbsEta = hist3D->GetYaxis()->GetBinLowEdge(1) >= 0;
            TDirectory * ddOut = dOut->mkdir(key->GetName(), key->ReadObj()->GetTitle());
            for (int etabin = 1; etabin <= hist3D->GetNbinsY(); ++etabin) {
                static int ieta = 0; // Used for parallelisation
                ++ieta;
                if (slice.second != ieta % slice.first) continue;

                float lowedge = hist3D->GetYaxis()->GetBinLowEdge(etabin),
                       upedge = hist3D->GetYaxis()->GetBinUpEdge (etabin);
                TString title = isAbsEta ? Form("%.3f < |#eta| < %.3f", lowedge, upedge)
                                         : Form("%.3f < #eta < %.3f"  , lowedge, upedge);
                hist3D->GetYaxis()->SetRange(etabin, etabin);
                auto hist2D = unique_ptr<TH2>( dynamic_cast<TH2*>(hist3D->Project3D("ZX")) ); // Z vs X, where Z is vertical and X is horizontal axis
                hist2D->SetTitle( TString( hist2D->GetTitle() ).ReplaceAll(") ", ", " + title + ")") );
                ddOut->cd();
                TDirectory * dddOut = ddOut->mkdir(Form("etabin%d", etabin), title);
                cout << bold << dddOut->GetPath() << def << endl;

                FitResponseByBin(dddOut, std::move(hist2D), steering);
            }
        }
        else cout << orange << "Ignoring " << key->ReadObj()->GetName() << " of `"
                  << key->ReadObj()->ClassName() << "` type" << def << endl;
    } // End of for (list of keys) loop
}

////////////////////////////////////////////////////////////////////////////////
/// Fit response distributions from `getJetResponse`
void fitJetResponse
            (const fs::path& input, //!< input ROOT file (histograms)
             const fs::path& output, //!< name of output ROOT (histograms)
             const pt::ptree& config, //!< config file from `DT::Options`
             const int steering, //!< steering parameters from `DT::Options`
             const DT::Slice slice = {1,0} //!< number and index of slice
            )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    auto fIn  = make_unique<TFile>(input .c_str(), "READ"),
         fOut = make_unique<TFile>(output.c_str(), "RECREATE");
    JetEnergy::loopDirsFromGetResponse(fIn.get(), fOut.get(), steering, slice);

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::JetEnergy namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        fs::path input, output;

        auto options = DAS::Options(
                            "Fit jet response in bins of pt and eta. Any 3D histograms "
                            "with axes following the same convention will be parsed in "
                            "the identical way.",
                            DT::split);
        options.input ("input" , &input , "input ROOT file (from `getJetResponse`)")
               .output("output", &output, "output ROOT file");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::JetEnergy::fitJetResponse(input, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}
#endif

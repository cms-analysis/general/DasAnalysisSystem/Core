#pragma once

#include <vector>
#include <list>
#include <optional>

#include <TUnfoldBinning.h>
#include <TH1.h>
#include <TH2.h>

#include "Observable.h"
#include "ZmmYCommon.h"

namespace DAS::Unfolding::ZmmY {

#if !defined(__CLING__) || defined(__ROOTCLING__)
struct BF : public Observable {
    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    BF ();

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructs a filler for the observable
    std::unique_ptr<Filler> getFiller (Darwin::Tools::Flow&) const override;

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Observable::setLmatrix`
    void setLmatrix (const std::unique_ptr<TH1>&, std::unique_ptr<TH2>&) override {}

    std::optional<int> process;
};

template<int PROCESS> struct BFprocessT : public BF {
    BFprocessT () { process = PROCESS; }
};

using DYJetsToMuMu   = BFprocessT<1>;
using DYJetsToTauTau = BFprocessT<2>;
using TTTo2L2Nu      = BFprocessT<3>;
using QCD            = BFprocessT<4>;
using WW             = BFprocessT<5>;
using WZ             = BFprocessT<6>;
using ZZ             = BFprocessT<7>;
static const std::vector<const char *> processes {"Z#to#mu#mu", "Z#to#tau#tau", "t#bar{t}#to2l2#nu", "QCD", "WW", "WZ", "ZZ"};
static const std::vector<const char *> channels {"#mu#mu", "#mu#mu#gamma"};

struct BFFiller final : public Filler {
    BF obs; ///< Backreference to the observable

    std::vector<GenMuon> * genMuons;
    std::vector<RecMuon> * recMuons;
    std::vector<GenPhoton> * genPhotons;
    std::vector<RecPhoton> * recPhotons;
    GenEvent * gEv;
    RecEvent * rEv;

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    BFFiller (const BF&, Darwin::Tools::Flow&);

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Filler::fillRec`
    std::list<int> fillRec (DistVariation&) override;

    ////////////////////////////////////////////////////////////////////////////////
    /// \todo Needed?
    void match () override {}

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Filler::fillMC`
    void fillMC (DistVariation&) override;

private:
    EventInfo<RecMuon, RecPhoton> rInfo;
    std::optional<int> irecbin;
};
#endif

} // end of DAS::Unfolding::ZmmY namespace

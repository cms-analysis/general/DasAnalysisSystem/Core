#pragma once

#include <vector>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Photon.h"
#include "Core/Objects/interface/Di.h"

namespace DAS::Unfolding::ZmmY {

#if !defined(__CLING__) || defined(__ROOTCLING__)
////////////////////////////////////////////////////////////////////////////////
/// Event categories used in the \f$ Z\to\mu\mu\gamma \f$ analysis.
enum class EventCategory {
    NotSelected, ///< The event fails some selection.
    MuMu,        ///< Dimuon candidates close to the Z peak.
    MuMuGamma,   ///< \f$ Z\to\mu\mu\gamma \f$ decay candidates.
};

////////////////////////////////////////////////////////////////////////////////
/// Implements the event selection and categorization common to all ZmmY
/// observables, and saves some relevant intermediate calculations.
///
/// \tparam Muon   Only prepared to handle muons (gen or rec)
/// \tparam Photon Gen or rec photons
template<class Muon, class Photon>
struct EventInfo
{
    // Phase space cuts.
    constexpr static double minMuon0Pt = 20,    ///< Scale factors start at 15 GeV
                            minMuon1Pt = 15,    ///< Dimuon trigger
                            maxMuonEta = 2.4,   ///< Muon chamber acceptance
                            minPhotonPt = 20,   ///< Scale factors start at 20 GeV
                            maxPhotonEta = 2.4, ///< Muon chamber acceptance
                            minZMass = 76,      ///< mZ - 15 GeV
                            maxZMass = 106,     ///< mZ + 15 GeV
                            minMuMuMass = 40;   ///< Trigger efficiency
    static_assert(minMuon0Pt >= minMuon1Pt, "leading pT always >= subleading");
    static_assert(minZMass < maxZMass, "min mass always < max mass");

    /// The event category. This determines which other fields are valid.
    EventCategory category;

    /// The dimuon system. Invalid for NotSelected events.
    Di<const Muon, const Muon> dimuon;

    /// The invariant mass of the dimuon system. Invalid for NotSelected events.
    float mMuMu;

    /// Considered photons. Only valid for MuMuGamma events.
    std::vector<Photon> selectedPhotons;

    /// Mass of the dimuon + photon system. Only valid for MuMuGamma events.
    float mMuMuGamma;

    /// Event weight. Always valid; for NotSelected events, this does not include
    /// the muons nor the photon weights.
    /// \todo Correlation bit.
    float weight;

    ////////////////////////////////////////////////////////////////////////////////
    /// Default constructor to enable use as a variable in fillers.
    EventInfo ()
        : category(EventCategory::NotSelected)
        , selectedPhotons{}
        , mMuMu(-1)
        , mMuMuGamma(-1)
        , weight(0)
    {
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Main constructor.
    EventInfo (const GenEvent* gEv, ///< Null for data.
               const RecEvent* rEv, ///< Null for gen-level.
               const std::vector<Muon>& muons,
               const std::vector<Photon>& photons,
               const Uncertainties::Variation& v);

private:
    ////////////////////////////////////////////////////////////////////////////////
    /// Checks if a mass should be considered close to the Z peak.
    bool IsMassOnZPeak (float mass) const
    {
        return mass > minZMass && mass < maxZMass;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Selects a pair of muons, if any. Returns a null Di if no suitable pair is
    /// found.
    DAS::Di<const Muon, const Muon> SelectMuons (
        const std::vector<Muon>& muons, const Uncertainties::Variation& v) const
    {
        if (muons.size() < 2) return {};

        /// \todo The variation could change which muons are leading.
        const auto& m0 = muons[0],
                  & m1 = muons[1];

        const auto p0 = m0.CorrP4(v),
                   p1 = m1.CorrP4(v);

        if (m0.Q * m1.Q != -1 // opposite charges
            || p0.Pt() < minMuon0Pt
            || p1.Pt() < minMuon1Pt
            || abs(p0.Eta()) >= maxMuonEta
            || abs(p1.Eta()) >= maxMuonEta)
            return {};

        return m0 + m1;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Checks if a reconstructed photon passes the selection criteria.
    ///
    /// \todo Use a \f$ \Delta R \f$ cut w.r.t. muons?
    bool IsGoodPhoton (const RecPhoton& photon,
                       const Uncertainties::Variation& v) const
    {
        auto p4 = photon.CorrP4(v);
        return photon.Weight(v) != 0
            && p4.Pt() > minPhotonPt && std::abs(p4.Eta()) < maxPhotonEta;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Checks if a generated photon passes the selection criteria.
    bool IsGoodPhoton (const GenPhoton& photon,
                       const Uncertainties::Variation& v) const
    {
        auto p4 = photon.CorrP4(v);
        return photon.Weight(v) != 0
            && photon.zAncestor
            && p4.Pt() > minPhotonPt && std::abs(p4.Eta()) < maxPhotonEta;
    }
};

template<class Muon, class Photon>
EventInfo<Muon, Photon>::EventInfo (const GenEvent* gEv,
                                    const RecEvent* rEv,
                                    const std::vector<Muon>& muons,
                                    const std::vector<Photon>& photons,
                                    const Uncertainties::Variation& v)
    : EventInfo()
{
    weight = 1;
    if (rEv) weight *= rEv->Weight(v);
    if (gEv) weight *= gEv->Weight(v);

    // Start by looking at muons.
    dimuon = SelectMuons(muons, v);
    if (!dimuon) return;

    mMuMu = dimuon.CorrP4(v).M();

    // The dimuon system is on the Z peak. Don't consider photons.
    if (IsMassOnZPeak(mMuMu)) {
        category = EventCategory::MuMu;
        weight *= dimuon.Weight(v);
        return;
    }

    // Do not consider muon pairs below minMuMuMass. This reduces
    // backgrounds (e.g. vector meson + gamma).
    // We also remove here events with mMuMu above the Z peak, as there is
    // no way adding photons can bring them back to the peak.
    if (mMuMu < minMuMuMass || mMuMu > minZMass)
        return;

    // Now get photons. We must copy them because the photons collection could
    // be reused with another variation.
    std::copy_if(photons.begin(), photons.end(),
                    std::back_inserter(selectedPhotons),
                    [&] (auto photon) { return IsGoodPhoton(photon, v); });

    if (selectedPhotons.empty()) return;

    /// \todo We may need to reorder photons after the variation.
    /// \todo We only consider the leading photon for now.
    mMuMuGamma = (dimuon.CorrP4(v) + selectedPhotons[0].CorrP4(v)).M();
    if (IsMassOnZPeak(mMuMuGamma)) {
        category = EventCategory::MuMuGamma;
        weight *= dimuon.Weight(v) * selectedPhotons[0].Weight(v);
    }
}
#endif

} // end of DAS::Unfolding::ZmmY namespace

#pragma once

#include <vector>
#include <list>
#include <algorithm>
#include <optional>

#include <TUnfoldBinning.h>
#include <TH1.h>
#include <TH2.h>

#if !defined(__CLING__) || defined(__ROOTCLING__)
#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"
#include "Core/Unfolding/interface/Observable.h"
#endif

namespace DAS::Unfolding::Rij {

static const std::vector<double> genHt_edges  = { 300,  360,  430,  510,  600,  700,  800,
                                                  920, 1060, 1220, 1400, 1600, 1840, 2100,
                                                 2400, 2740, 3140, 3590, 4120, /*4700,*/ 5500 },
                                 recHt_edges  = { 300,  330,  360,  390,  430,  470,  510,
                                                  550,  600,  650,  700,  750,  800,  860,
                                                  920,  980, 1060, 1140, 1220, 1300, 1400,
                                                 1500, 1600, 1720, 1840, 1960, 2100, 2240,
                                                 2400, 2560, 2740, 2940, 3140, 3340, 3590,
                                                 3840, 4120, /*4400,*/ 4700, /*5050,*/ 5500 },
                                 n_jets_edges = { 1.5, 2.5, 3.5, 4.5, 5.5 };

static const int nRecHtBins = recHt_edges.size()-1,
                 nGenHtBins = genHt_edges.size()-1,
                 nJetsBins = n_jets_edges.size()-1;

static const double Htmin    = recHt_edges.front(),
                    Htmax    = recHt_edges.back(),
                    nJetsmin = n_jets_edges.front()+0.5,
                    nJetsmax = n_jets_edges.back()-0.5,
                    ptmin    = 150,
                    ymax     = 3.0; /// \todo finalise phase space selection

#if !defined(__CLING__) || defined(__ROOTCLING__)
struct HTn final : public Observable {
    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    HTn ();

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructs a filler for the observable
    std::unique_ptr<Filler> getFiller (Darwin::Tools::Flow&) const override;

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructs a transformer for the observable
    std::unique_ptr<Transformer> getTransformer (TUnfoldBinning *) const override;

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Observable::getLmatrix`
    void setLmatrix (const std::unique_ptr<TH1>&, std::unique_ptr<TH2>&) override;
};

struct Ratios final : public Observable {
    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    Ratios ();
};

struct HTnFiller final : public Filler {
    HTn obs; ///< Backreference to the observable

    std::vector<GenJet> * genJets;
    std::vector<RecJet> * recJets;
    GenEvent * gEv;
    RecEvent * rEv;

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    HTnFiller (const HTn&, Darwin::Tools::Flow&);

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Filler::fillRec`
    std::list<int> fillRec (DistVariation&) override;

    std::vector<std::pair<GenJet,RecJet>> matches;
    std::vector<GenJet> misses;
    std::vector<RecJet> fakes;

    std::optional<bool> matched;

    ////////////////////////////////////////////////////////////////////////////////
    /// Match the two pairs of leading jets (if any) and set `matched` member.
    void match () override;

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Filler::fillMC`
    void fillMC (DistVariation&) override;
};

struct HTnTransformer final : public Transformer {

    const size_t nHtBins;

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor.
    HTnTransformer (TUnfoldBinning * //!< a priori, gen or rec binning
                   );

    ////////////////////////////////////////////////////////////////////////////////
    /// Destructor.
    ~HTnTransformer () = default;

    ////////////////////////////////////////////////////////////////////////////////
    /// Calculate R32, R43, R42, etc. from the 2-jet, 3-jet, 4-jet, etc. inclusive
    /// jet cross sections in bins of HT.
    void Transform (const Eigen::VectorXd& //!< event in original zero-suppressed flat binning
                   ) const override;

};
#endif

} // end of DAS::Unfolding::Rij namespace

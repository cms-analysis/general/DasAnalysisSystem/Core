#ifndef DOXYGEN_SHOULD_SKIP_THIS
#define DOXYGEN_SHOULD_SKIP_THIS
#define BOOST_TEST_MODULE testUnfold
#include <boost/test/included/unit_test.hpp>
#include "Core/Unfolding/bin/unfold.cc"

#include <darwin.h>

#include <filesystem>
#include <vector>

using namespace std;
namespace fs = filesystem;

BOOST_AUTO_TEST_SUITE( closure )

    BOOST_AUTO_TEST_CASE( prepare_data )
    {
        DT::StandardInit();
        const vector<float> recBins{330,362,395,430,468,507,548,592,638},
                            genBins{330    ,395    ,468    ,548    ,638};

        {
            auto f = make_unique<TFile>("Data.root", "RECREATE");
            auto d = f->mkdir("nominal");
            d->cd();

            const vector<float> gen {6651.39, 2621.78, 1051.84, 446.982},
                rec {4443, 2679.64, 1695.39, 1099.81, 678.544, 437.573, 286.335, 184.408};
            const vector<vector<float>> cov {
        {  108.138,   3.00372,   1.29928,  0.524956,  0.200247, 0.0904205, 0.0443272, 0.0218946},
        {  3.00372,   41.5128,   1.21452,  0.542849,  0.199151,  0.090259, 0.0347899, 0.0175689},
        {  1.29928,   1.21452,   17.1436,  0.531326,  0.223862, 0.0838979, 0.0339368, 0.0162317},
        { 0.524956,  0.542849,  0.531326,   7.17864,  0.224999, 0.0920277, 0.0364688, 0.0144472},
        { 0.200247,  0.199151,  0.223862,  0.224999,   2.82483, 0.0927202, 0.0404473, 0.0157154},
        {0.0904205,  0.090259, 0.0838979, 0.0920277, 0.0927202,   1.21123, 0.0406513, 0.0169662},
        {0.0443272, 0.0347899, 0.0339368, 0.0364688, 0.0404473, 0.0406513,  0.505679, 0.0176721},
        {0.0218946, 0.0175689, 0.0162317, 0.0144472, 0.0157154, 0.0169662, 0.0176721,  0.218359 }};

            TH1 * h_gen  = new TH1F("gen" , "", genBins.size()-1, genBins.data()),
                * h_rec  = new TH1F("rec" , "", recBins.size()-1, recBins.data()),
                * h_cov  = new TH2F("cov" , "", recBins.size()-1, recBins.data(),
                                                recBins.size()-1, recBins.data());

            for (size_t j = 1; j < recBins.size(); ++j)
                h_rec->SetBinContent(j, rec[j-1]);
            for (size_t i = 1; i < recBins.size(); ++i)
            for (size_t j = 1; j < recBins.size(); ++j)
                h_cov->SetBinContent(i,j, cov[i-1][j-1]);

            for (TH1 * h: { h_gen, h_rec, h_cov}) {
                h->SetDirectory(d);
                h->Write();
            }
        }

        {
            auto f = make_unique<TFile>("MC.root", "RECREATE");
            auto d = f->mkdir("nominal");
            d->cd();

            const vector<float> gen  {  6607.1,  2609.2, 1044.67, 441.662 },
                                miss { 1548.09, 47.2368, 10.4246, 63.2799 },
                rec  { 4418.15, 2671.14, 1684.89, 1092.87, 674.859, 432.432, 284.292, 182.416 },
                fake {  2012.1, 311.821, 30.4037, 6.08727, 2.85702, 2.72962, 8.60857, 32.8295 };
            const vector<vector<float>> cov {
                {    8.64223,    0.188038,   0.0785687,   0.0314394,    0.012043,  0.00512699,  0.00214151,  0.00104955 },
                {   0.188038,      3.2715,   0.0673984,   0.0294979,   0.0109737,  0.00414604,  0.00199121, 0.000775939 },
                {  0.0785687,   0.0673984,      1.3916,   0.0285614,   0.0118633,  0.00421753,  0.00157878, 0.000638562 },
                {  0.0314394,   0.0294979,   0.0285614,    0.603268,   0.0113055,  0.00414158,  0.00136819, 0.000564623 },
                {   0.012043,   0.0109737,   0.0118633,   0.0113055,    0.228633,  0.00329566,  0.00120471, 0.000447009 },
                { 0.00512699,  0.00414604,  0.00421753,  0.00414158,  0.00329566,   0.0771141,  0.00107167, 0.000453384 },
                { 0.00214151,  0.00199121,  0.00157878,  0.00136819,  0.00120471,  0.00107167,   0.0270088, 0.000437179 },
                { 0.00104955, 0.000775939, 0.000638562, 0.000564623, 0.000447009, 0.000453384, 0.000437179,  0.00989617 } },
                                        RM {
                { 2291.16,  1899.53,  733.807,  123.198,  9.89329,  1.05721, 0.277702, 0.0833232},
                { 111.343,  450.662,  877.149,  776.032,  292.045,  49.7355,   4.4996,  0.501105},
                { 3.04508,  8.35056,  42.1392,  183.755,  351.442,  303.365,  121.612,   20.5417},
                { 0.49977, 0.777264,  1.38553,  3.79658,  18.6224,  75.5446,  149.295,   128.461} };

            TH1 * h_gen  = new TH1F("gen"    , "", genBins.size()-1, genBins.data()),
                * h_miss = new TH1F("missAll", "", genBins.size()-1, genBins.data()),
                * h_rec  = new TH1F("rec"    , "", recBins.size()-1, recBins.data()),
                * h_fake = new TH1F("fakeAll", "", recBins.size()-1, recBins.data()),
                * h_cov  = new TH2F("cov"    , "", recBins.size()-1, recBins.data(),
                                                   recBins.size()-1, recBins.data()),
                * h_RM   = new TH2F("RM"     , "", genBins.size()-1, genBins.data(),
                                                   recBins.size()-1, recBins.data());

            for (size_t i = 1; i < genBins.size(); ++i) {
                h_gen->SetBinContent(i, gen[i-1]);
                h_miss->SetBinContent(i, miss[i-1]);
            }
            for (size_t j = 1; j < recBins.size(); ++j) {
                h_rec->SetBinContent(j, rec[j-1]);
                h_fake->SetBinContent(j, fake[j-1]);
            }
            for (size_t i = 1; i < genBins.size(); ++i)
            for (size_t j = 1; j < recBins.size(); ++j)
                h_RM->SetBinContent(i,j, RM[i-1][j-1]);
            for (size_t i = 1; i < recBins.size(); ++i)
            for (size_t j = 1; j < recBins.size(); ++j)
                h_cov->SetBinContent(i,j, cov[i-1][j-1]);

            for (auto& h: { h_gen, h_miss, h_rec, h_fake, h_cov, h_RM}) {
                h->SetDirectory(d);
                h->Write();
            }
        }
    }

    BOOST_AUTO_TEST_CASE( closure_tests )
    {
        pt::ptree config;
        DAS::Unfolding::unfold("MC.root", "MC.root", "CT1.root", config, DT::verbose);
        /// \todo expect perfect agreement
        DAS::Unfolding::unfold("Data.root", "MC.root", "CT2.root", config, DT::verbose);
        /// \todo expect agreement within stat unc

        /// \todo play with config
    }

BOOST_AUTO_TEST_SUITE_END()

#endif

#include <cstdlib>

#include <iostream>
#include <vector>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/binnings.h"
#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/CommonTools/interface/toolbox.h"

#include "Core/JEC/interface/JMEmatching.h"

#include <TH2.h>

#include "Math/VectorUtil.h"
#include "Math/GenVector/GenVector_exception.h"

#include <darwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Unfolding::InclusiveJet1D {

static const std::vector<double> recBins{74,84,97,114,133,153,174,196,220,245,272,300,330,362,395,430,468,507,548,592,638,686,737,790,846,905,967,1032,1101,1172,1248,1327,1410,1497,1588,1684,1784,1890,2000,2116,2238,2366,2500,2640,2787,2941,3103,3273,3450,3637,3832};
static const std::vector<double> genBins{74   ,97    ,133    ,174    ,220    ,272    ,330    ,395    ,468    ,548    ,638    ,737    ,846    ,967     ,1101     ,1248     ,1410     ,1588     ,1784     ,2000     ,2238     ,2500     ,2787     ,3103     ,3450,     3832};

static const int nRecBins = recBins.size()-1;
static const int nGenBins = genBins.size()-1;

static const double minpt = recBins.front(), maxpt = recBins.back(), maxy = 2.0;

////////////////////////////////////////////////////////////////////////////////
/// Holds the histograms and indices corresponding to one variation
struct PtVariation {
    const TString name;
    static bool isMC;
    const size_t iJEC, iGenJetWgt, iRecJetWgt,
                       iGenEvtWgt, iRecEvtWgt;
    TH1 * rec, * tmp, * gen,
        * missNoMatch, * missOut,
        * fakeNoMatch, * fakeOut;
    TH2 * cov, * RM;

    PtVariation (TString Name, size_t IJEC = 0,
            size_t IGenJetWgt = 0, size_t IRecJetWgt = 0,
            size_t IGenEvtWgt = 0, size_t IRecEvtWgt = 0) :
        name(Name), iJEC(IJEC),
        iGenJetWgt(IGenJetWgt), iRecJetWgt(IRecJetWgt),
        iGenEvtWgt(IGenEvtWgt), iRecEvtWgt(IRecEvtWgt),
        rec(nullptr), tmp(nullptr), gen(nullptr),
        missNoMatch(nullptr), missOut(nullptr),
        fakeNoMatch(nullptr), fakeOut(nullptr),
        cov(nullptr), RM(nullptr)
    {
        rec = new TH1D(Name + "rec", "detector level", nRecBins, recBins.data()),
        tmp = new TH1D(Name + "tmp", "", nRecBins, recBins.data());
        cov = new TH2D(Name + "cov", "covariance matrix of detector level", nRecBins, recBins.data(), nRecBins, recBins.data());
        if (!isMC) return;
        gen         = new TH1D(Name + "gen"        , "hadron level", nGenBins, genBins.data()),
        missNoMatch = new TH1D(Name + "missNoMatch", "real inefficiency"                                                 , nGenBins, genBins.data()),
        missOut     = new TH1D(Name + "missOut"    , "good gen jets matched to jets reconstructed out of the phase space", nGenBins, genBins.data()),
        fakeNoMatch = new TH1D(Name + "fakeNoMatch", "real background"                                               , nRecBins, recBins.data()),
        fakeOut     = new TH1D(Name + "fakeOut"    , "good rec jets matched to jets generated out of the phase space", nRecBins, recBins.data()),
        RM          = new TH2D(Name + "RM", "migrations of jets in phase space at both levels", nGenBins, genBins.data(), nRecBins, recBins.data());
        /// \todo investigate the migrations out of the phase space with the underflow and overflow
    }

    void Write (TFile * file)
    {
        cout << "Writing " << name << endl;
        file->cd();
        TDirectory * dir = file->mkdir(name);
        dir->cd();
#define PAIR(name) make_pair(dynamic_cast<TH1*>(name), #name)
        for (auto p: { PAIR(gen), PAIR(missNoMatch), PAIR(missOut),
                       PAIR(rec), PAIR(fakeNoMatch), PAIR(fakeOut),
                       PAIR(cov), PAIR(RM) }) {
#undef PAIR
            if (p.first == nullptr) continue;
            p.first->SetDirectory(dir);
            p.first->Write(p.second);
        }
    }
};
bool PtVariation::isMC = false; // just for initialisation

////////////////////////////////////////////////////////////////////////////////
/// Get histograms for unfolding only for pt (in d=1),
/// without using `TUnfoldBinning`.
///
/// Then the output can be unfolded using `unfold`.
void getUnfHistPt
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuple)
         const fs::path& output, //!< output ROOT file (n-tuple)
         const int steering, //!< parameters obtained from explicit options
         const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto [fOut, tOut] = flow.GetOutput(output);

    DT::MetaInfo metainfo(tOut);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    int R = metainfo.Get<int>("flags", "R");

    auto gEv = isMC ? flow.GetBranchReadOnly<GenEvent>("genEvent") : nullptr;
    auto rEv = flow.GetBranchReadOnly<RecEvent>("recEvent");
    auto genJets = isMC ? flow.GetBranchReadOnly <vector<GenJet>>("genJets") : nullptr;
    auto recJets = flow.GetBranchReadWrite<vector<RecJet>>("recJets");

    JMEmatching<>::maxDR = R/10./2.;
    cout << "Radius for matching: " << JMEmatching<>::maxDR << endl;

    cout << "Setting variations" << endl;

    PtVariation::isMC = isMC;
    vector<PtVariation> variations { PtVariation("nominal") };
    if (steering & DT::syst) {
        TList * ev_wgts = metainfo.List("variations", RecEvent::WeightVar);
        for (TObject * obj: *ev_wgts) {
            auto name = dynamic_cast<TObjString*>(obj)->GetString();
            static int i = 0; ++i;    //       JEC GJW RJW GEW REW
            variations.push_back( PtVariation(name, 0, 0, 0, i) );
        }

        TList * JECs = metainfo.List("variations", RecJet::ScaleVar);
        for (TObject * obj: *JECs) {
            auto name = dynamic_cast<TObjString*>(obj)->GetString();
            static int i = 0; ++i;    //       JEC GJW RJW GEW REW
            variations.push_back( PtVariation(name, i, 0, 0, 0) );
        }
    }

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        for (const PtVariation& v: variations) {

            v.tmp->Reset();

            auto evWgt = rEv->weights.at(v.iRecEvtWgt);
            if (isMC) evWgt *= gEv->weights.at(v.iGenEvtWgt);

            vector<int> binIDs; // we save the indices of the filled bins to avoid trivial operations
            for (const RecJet& recjet: *recJets) {
                auto y  = recjet.AbsRap();
                if (y >= maxy) continue;
                auto pt = recjet.CorrPt(v.iJEC);
                if (pt < minpt || pt >= maxpt) continue;

                auto irecbin = v.rec->FindBin(pt);
                if (find(binIDs.begin(), binIDs.end(), irecbin) == binIDs.end()) binIDs.push_back(irecbin);

                auto jWgt = recjet.weights.at(v.iRecJetWgt);

                v.rec->Fill(pt, evWgt * jWgt);
                v.tmp->Fill(pt, evWgt * jWgt); // for cov
            }

            for (auto x: binIDs) for (auto y: binIDs) {
                double cCov = v.cov->GetBinContent(x,y),
                       cTmp = v.tmp->GetBinContent(x)*v.tmp->GetBinContent(y);
                v.cov->SetBinContent(x,y,cCov+cTmp);
            }
        }

        if (!isMC) continue;

        JMEmatching matching(*recJets, *genJets);
        for (const auto& [rec_it,gen_it]: matching.match_its) {

            auto genpt = gen_it->p4.Pt(),
                 geny = gen_it->AbsRap();

            bool LowGenPt  = genpt < minpt,
                 HighGenPt = genpt >= maxpt,
                 HighGenY  =  geny >= maxy;
            bool goodGen = (!LowGenPt) && (!HighGenPt) && (!HighGenY);

            for (const PtVariation& v: variations) {
                auto gEvW = gEv->weights.at(v.iGenEvtWgt),
                     gJetW = gen_it->weights.at(v.iGenJetWgt);

                if (goodGen) v.gen->Fill(genpt, gEvW * gJetW);

                auto rEvW = rEv->weights.at(v.iRecEvtWgt),
                     rJetW = rec_it->weights.at(v.iRecJetWgt);

                auto recpt = rec_it->CorrPt(v.iJEC),
                     recy  = abs(rec_it->Rapidity());

                bool LowRecPt  = recpt < minpt,
                     HighRecPt = recpt >= maxpt,
                     HighRecY  =  recy >= maxy;
                bool goodRec = (!LowRecPt) && (!HighRecPt) && (!HighRecY);

                if ( goodRec &&  goodGen) {    v.RM->Fill(genpt, recpt, gEvW * gJetW *      rEvW * rJetW  );
                                      v.missNoMatch->Fill(genpt,        gEvW * gJetW * (1 - rEvW * rJetW) ); }
           else if (!goodRec &&  goodGen) v.missOut->Fill(genpt,        gEvW * gJetW                      );
           else if ( goodRec && !goodGen) v.fakeOut->Fill(       recpt, gEvW * gJetW *      rEvW * rJetW  );
            }
            /// \todo try to use the underflow and overflow for matching out of the phase space...
        } // end of loop on matched jets

        for (const auto& gen_it: matching.miss_its) {
            for (const PtVariation& v: variations) {

                double y = gen_it->AbsRap();
                if (y >= maxy) continue;
                double pt = gen_it->p4.Pt();
                if (pt < minpt || pt > maxpt) continue;

                auto evWgt = gEv->weights.at(v.iGenEvtWgt);
                auto jWgt = gen_it->weights.at(v.iGenJetWgt);

                v.gen        ->Fill(pt, evWgt * jWgt);
                v.missNoMatch->Fill(pt, evWgt * jWgt);
            }
        } // end of loop on missed jets

        for (const auto& rec_it: matching.fake_its) {
            for (const auto& v: variations) {

                double y = rec_it->AbsRap();
                if (y >= maxy) continue;
                double pt = rec_it->CorrPt(v.iJEC);
                if (pt < minpt || pt > maxpt) continue;

                auto evWgt = rEv->weights.at(v.iRecEvtWgt) * gEv->weights.at(v.iGenEvtWgt);
                auto jWgt = rec_it->weights.at(v.iRecJetWgt);

                v.fakeNoMatch->Fill(pt, evWgt * jWgt);
            }
        } // end of loop on fake jets
    }

    fOut->cd();
    for (PtVariation& v: variations)
        v.Write(fOut);

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::Unfolding::InclusiveJet1D namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options(
                "Fill histograms to run unfolding.", DT::syst | DT::split);

        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file"               );
        options(argc,argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Unfolding::InclusiveJet1D::getUnfHistPt(inputs, output, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif

#include "Core/Unfolding/interface/MNjets.h"
#include "Core/JetObservables/interface/MuellerNavelet.h"

#include "Math/VectorUtil.h"
#include <exceptions.h>

using namespace std;
using namespace DAS;
using namespace DAS::Unfolding::MNjets;
namespace DE = Darwin::Exceptions;

DEtaDPhiFiller::DEtaDPhiFiller (const DEtaDPhi& obs, Darwin::Tools::Flow& flow)
    : obs(obs)
    , genJets(obs.isMC ? flow.GetBranchReadOnly<vector<GenJet>>("genJets") : nullptr)
    , recJets(flow.GetBranchReadOnly<vector<RecJet>>("recJets"))
    , gEv(obs.isMC ? flow.GetBranchReadOnly<GenEvent>("genEvent") : nullptr)
    , rEv(flow.GetBranchReadOnly<RecEvent>("recEvent"))
{
}

////////////////////////////////////////////////////////////////////////////////
/// Mueller-Navelet jet selection
template<typename Jet>
Di<const Jet, const Jet> selection (const vector<Jet>& jets,
                    const Uncertainties::Variation& v = Uncertainties::nominal)
{
    Di<const Jet, const Jet> MNjets;
    for (size_t i = 0; i < jets.size(); ++i) {
        const Jet& jet = jets.at(i);
        if (jet.CorrPt(v) < minpt) continue;
        if (jet.AbsRap() > maxy) continue;

        if (jet.p4.Eta() > (MNjets.first ? MNjets.first->p4.Eta() : 0.))
            MNjets.first = &jet;
        else if (jet.p4.Eta() < (MNjets.second ? MNjets.second->p4.Eta() : 0.))
            MNjets.second = &jet;
    }

    return MNjets;
}

////////////////////////////////////////////////////////////////////////////////
/// Find the bin number.
///
/// \return bin index (0 means out of PS, like `TUnfoldBinning::GetGlobalBinNumber`)
template<typename Jet>
double getBinNumber (const Di<Jet,Jet>& MNjets,
                     const Uncertainties::Variation& v,
                     TUnfoldBinning * bng)
{
    if (!MNjets) return 0;

    float DEta = std::abs(MNjets.DeltaEta(v)),
          DPhi = std::abs(MNjets.DeltaPhi(v));

    return bng->GetGlobalBinNumber(DEta, DPhi);
}

list<int> DEtaDPhiFiller::fillRec (DistVariation& v)
{
    recMNjets = selection(*recJets, v);
    if (!recMNjets) return {};

    int i = getBinNumber(recMNjets, v, obs.recBinning);
    if (i == 0) return {};

    double w = rEv->Weight(v);
    if (obs.isMC) w *= gEv->Weight(v);
    w *= recMNjets.Weight(v);

    v.tmp->Fill(i, w);
    v.rec->Fill(i, w);

    return list<int>{i};
}

void DEtaDPhiFiller::match ()
{
    matched.reset(); /// \todo is `optional` necessary? (taken from MjjYmax...)

    using ROOT::Math::VectorUtil::DeltaR;

    genMNjets = selection(*genJets);

    // matching (swapping leading and subleading is allowed)
    matched = genMNjets && recMNjets
          && DeltaR(genMNjets.first ->p4, recMNjets.first ->p4) < obs.maxDR
          && DeltaR(genMNjets.second->p4, recMNjets.second->p4) < obs.maxDR;
}

void DEtaDPhiFiller::fillMC (DistVariation& v)
{
    if (!obs.isMC)
        BOOST_THROW_EXCEPTION( runtime_error(__func__ + " should only be called for MC"s) );

    auto irec = getBinNumber(recMNjets, v, obs.recBinning);
    double recW = rEv->Weight(v);
    if (irec > 0) recW *= recMNjets.Weight(v);

    auto igen = getBinNumber(genMNjets, v, obs.genBinning);
    double genW = gEv->Weight(v);
    if (igen > 0) genW *= genMNjets.Weight(v);

    if (igen > 0) v.gen->Fill(igen, genW);

    if (*matched) {
        if      (irec > 0  && igen >  0) {    v.RM->Fill(igen, irec, genW *    recW );
                                     v.missNoMatch->Fill(igen,       genW * (1-recW)); }
        else if (irec == 0 && igen >  0) v.missOut->Fill(igen,       genW           );
        else if (irec > 0  && igen == 0) v.fakeOut->Fill(      irec, genW *    recW );
    }
    else {
        if (igen > 0) v.missNoMatch->Fill(igen, genW       );
        if (irec > 0) v.fakeNoMatch->Fill(irec, genW * recW);
    }
}

////////////////////////////////////////////////////////////////////////////////

DEtaDPhi::DEtaDPhi () :
    Observable(__FUNCTION__, "Angular separation of Mueller-Navelet jets")
{
    recBinning->AddAxis("DEta", nRecDEtaBins, 0., 8., false, false);
    genBinning->AddAxis("DEta", nGenDEtaBins, 0., 8., false, false);
    recBinning->AddAxis("DPhi", nRecDPhiBins, 0., M_PI, false, false);
    genBinning->AddAxis("DPhi", nGenDPhiBins, 0., M_PI, false, false);
}

unique_ptr<DAS::Unfolding::Filler> DEtaDPhi::getFiller (Darwin::Tools::Flow& flow) const
{
    return make_unique<DEtaDPhiFiller>(*this, flow);
}

void DEtaDPhi::setLmatrix (const unique_ptr<TH1>& bias, unique_ptr<TH2>& L)
{
    const int N = genBinning->GetDistributionNumberOfBins();
    for (int i = 1; i <= N; ++i) {
        double DEta = genBinning->GetDistributionBinCenter(0,i),
               DPhi = genBinning->GetDistributionBinCenter(1,i);

        // b(in)
        int               bUp     = i-nGenDEtaBins,
             bLeft = i-1, bCenter = i             , bRight  = i+1,
                          bDown   = i+nGenDEtaBins;

        // values (curvature regularisation)
        auto get = [&bias](int i) {
            auto content = bias->GetBinContent(i);
            if (content < 0)
                BOOST_THROW_EXCEPTION( DE::BadInput("Expecting only positive entries", bias) );
            return content > 0 ? 1./content : 0;
        };

        auto cUp    = get(bUp   ),
             cLeft  = get(bLeft ),
             cRight = get(bRight),
             cDown  = get(bDown );

        cout << setw(3) << i << setw(10) << DEta << setw(10) << DPhi
             << setw(5) << bCenter << setw(15) << (cUp+cLeft+cRight+cDown)
             << setw(5) << bUp     << setw(15) << -cUp
             << setw(5) << bLeft   << setw(15) <<     -cLeft
             << setw(5) << bRight  << setw(15) <<           -cRight
             << setw(5) << bDown   << setw(15) <<                  -cDown << '\n';

        // filling L-matrix
                        L->SetBinContent(i, bCenter,  cUp+cLeft+cRight+cDown );
        if (cUp    > 0) L->SetBinContent(i, bUp    , -cUp                    );
        if (cLeft  > 0) L->SetBinContent(i, bLeft  ,     -cLeft              );
        if (cRight > 0) L->SetBinContent(i, bRight ,           -cRight       );
        if (cDown  > 0) L->SetBinContent(i, bDown  ,                  -cDown );
    }
    cout << flush;
}

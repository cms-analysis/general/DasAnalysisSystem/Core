#include "Core/Unfolding/interface/Observable.h"
#include "Core/Unfolding/interface/PtY.h"
#include "Core/Unfolding/interface/MjjYmax.h"
#include "Core/Unfolding/interface/MjjYbYs.h"
#include "Core/Unfolding/interface/HTn.h"
#include "Core/Unfolding/interface/ZPtY.h"
#include "Core/Unfolding/interface/BF.h"
#include "Core/Unfolding/interface/MNjets.h"

#include <TH2.h>
#include <TVectorT.h>

#include <boost/property_tree/ptree.hpp>

#include <boost/preprocessor/seq/elem.hpp>
#include <boost/preprocessor/seq/for_each.hpp>

#include "colours.h"

using namespace std;
using namespace Eigen;
namespace pt = boost::property_tree;

namespace DAS::Unfolding {

Observable::Observable (const char * name, const char * title) :
    recBinning(new TUnfoldBinning(name)), genBinning(new TUnfoldBinning(name))
{
    cout << "|- Defining " << __func__ << " `" << name << "`." << endl;

    recBinning->SetTitle(Form("%s (detector level)", title));
    genBinning->SetTitle(Form("%s (particle level)", title));
}

bool Observable::isMC = false;
double Observable::maxDR = 0.2;

void Observable::setLmatrix (const unique_ptr<TH1>&, unique_ptr<TH2>&)
{
    cerr << orange << "Warning: the default L-matrix is empty\n" << def;
}

vector<Observable *> GetObservables (boost::property_tree::ptree pt)
{
    vector<Observable *> observables; /// \todo not all observables should be fine (e.g. `getUnfHist` should not accept `Rij::Ratios`)
#define OBS_TYPES (InclusiveJet::PtY)(DijetMass::MjjYmax)(DijetMass3D::MjjYbYs)(Rij::HTn)(Rij::Ratios)(DrellYan::ZPtY)(ZmmY::BF)(ZmmY::DYJetsToMuMu)(ZmmY::DYJetsToTauTau)(ZmmY::TTTo2L2Nu)(ZmmY::QCD)(ZmmY::WW)(ZmmY::WZ)(ZmmY::ZZ)(MNjets::DEtaDPhi)
#define IF_OBS(r, data, TYPE) \
    if (pt.find(BOOST_PP_STRINGIZE(TYPE)) != pt.not_found()) { \
        observables.push_back(new TYPE); \
        for (auto it = pt.begin(); it != pt.end(); ++it) { \
            if (it->first != BOOST_PP_STRINGIZE(TYPE)) continue; \
            pt.erase(it); \
            break; \
        } \
    }
    BOOST_PP_SEQ_FOR_EACH(IF_OBS, _, OBS_TYPES)
#undef OBS_TYPES
#undef IF_OBS
    if (!pt.empty()) {
        TString unknown;
        for (auto it = pt.begin(); it != pt.end(); ++it) {
            unknown += it->first;
            unknown += ' ';
        }
        BOOST_THROW_EXCEPTION(invalid_argument(Form("Unknown observable(s): %s", unknown.Data())));
    }
    return observables;
}

VectorXd Transformer::y;

unique_ptr<Filler> Observable::getFiller (Darwin::Tools::Flow&) const
{
    cerr << orange << "Warning: the default `Filler' does nothing\n" << def;
    return make_unique<Filler>();
}

list<int> Filler::fillRec (DistVariation&) { return list<int>{}; }
void Filler::match () { }
void Filler::fillMC (DistVariation&) { }

unique_ptr<Transformer> Observable::getTransformer (TUnfoldBinning * bng) const
{
    return make_unique<Transformer>(bng);
}

void Transformer::AddAxis (TUnfoldBinning * preBinning, TUnfoldBinning * postBinning, int iaxis)
{
    TString axisname = preBinning->GetDistributionAxisLabel(iaxis);
    const TVectorD * binning = preBinning->GetDistributionBinning(iaxis);
    int nbins = binning->GetNrows()-1;
    const double * edges = binning->GetMatrixArray();
    postBinning->AddAxis(axisname, nbins, edges, false, false);
}

Transformer::Transformer (TUnfoldBinning * bng, bool clone_binning) :
    preBinning(bng), postBinning(new TUnfoldBinning(bng->GetName()))
{
    postBinning->SetTitle(bng->GetTitle());

    if (!clone_binning) return;

    int d = bng->GetDistributionDimension();
    for (int i = 0; i < d; ++i)
        AddAxis(preBinning, postBinning, i);
}

namespace {

void EmptyBin (TH1 * h, TH2 * cov, int i)
{
    h->SetBinContent(i,0);
    h->SetBinError  (i,0);
    for (int j = 1; j <= h->GetNbinsX(); ++j) {
        h->SetBinContent(i,j,0);
        h->SetBinContent(j,i,0);
    }
}

} // end of anonymous namespace

void Transformer::RemoveBadInputBins (TH1 * h, TH2 * cov)
{
    for (int i = preBinning->GetStartBin();
             i < preBinning->GetEndBin(); ++i) {
        double element = h->GetBinContent(i);

        // skip empty bins
        if (element == 0) continue;

        // avoid two consecutive bins with identical content
        if (h->GetBinContent(i-1) == h->GetBinContent(i)) {
            EmptyBin(h,cov,i);
            continue;
        }

        // avoid isolated bins
        if (h->GetBinContent(i-1) == 0 &&
            h->GetBinContent(i+1) == 0)
            EmptyBin(h,cov,i);
    }
}

void Transformer::Transform (const VectorXd& x) const
{
    int preOffset =  preBinning->GetStartBin()-1,
       postOffset = postBinning->GetStartBin()-1,
       nBins = preBinning->GetDistributionNumberOfBins();
    y.segment(postOffset, nBins) = x.segment(preOffset, nBins);
}

} // end of namespace DAS::Unfolding

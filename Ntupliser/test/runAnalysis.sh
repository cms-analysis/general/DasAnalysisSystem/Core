#!/bin/zsh
set -e
set -x

folder="$1"
input_file="$(readlink -f "$2")"
isMC="$3"

mkdir -p "$folder"
cd "$folder"

mergeNtuples "${input_file}" mergeNtuples.root /dev/null /dev/null -f

# MET
applyMETfilters mergeNtuples.root applyMETfilters.root $DARWIN_TABLES/MET/2018.info
mergeNtuples ntuple.root mergeNtuples.root /dev/null $DARWIN_TABLES/MET/2018.info -f
## \todo getMETfraction

# jet veto maps
vetomap=$DARWIN_TABLES/jetvetomaps/eff_map_UL18.root
if [[ ! -f $vetomap ]]; then
    echo Getting the maps
    (cd $DARWIN_TABLES/jetvetomaps && ./getULRun2Maps)
fi
applyConservativeVetoMap mergeNtuples.root applyConservativeVetoMap.root $vetomap
mergeNtuples ntuple.root mergeNtuples.root $vetomap $DARWIN_TABLES/MET/2018.info -f

previous_file=mergeNtuples.root

if [ "$isMC" = "yes" ]; then
    # normalisation
    getSumWeights mergeNtuples.root getSumWeights.root signOnly 0
    applyMClumi mergeNtuples.root getSumWeights.root applyMClumi.root 1 -f

    # PU staub
    applyPUcleaning applyMClumi.root applyPUcleaning.root /dev/null 0 -f

    # jet energy scale corrections
    JEC_TABLES=$DARWIN_TABLES/jsonpog-integration/POG/JME/2018_UL/jet_jerc.json.gz
    applyJEScorrections applyPUcleaning.root applyJEScorrections.root $JEC_TABLES Summer19UL18 -f

    # jet energy resolution
    getJetResponse applyJEScorrections.root getJetResponse.root false
    fitJetResponse getJetResponse.root fitJetResponse.root
    fitJetResolution fitJetResponse.root fitJetResolution.root
    applyJERsmearing applyJEScorrections.root applyJERsmearing.root $JEC_TABLES Summer19UL18 1 -f

    getBTagBinnedDiscriminant applyJERsmearing.root getBTagBinnedDiscriminant.root
    getBTagPerformance getBTagBinnedDiscriminant.root getBTagPerformance.root
    getBTagFraction getBTagBinnedDiscriminant.root getBTagFraction.root
    applyBTagSF applyJERsmearing.root applyBTagSF.root getBTagBinnedDiscriminant.root $DARWIN_TABLES/btagging/2018.csv -f

    # PU profile
    triggers=$DARWIN_TABLES/triggers/thresholds/2018/HLT_AK8PFJet.info
    getPUprofile applyBTagSF.root getPUprofile.root $triggers
    applyPUprofCorrection applyBTagSF.root applyPUprofCorrection.root getPUprofile.root getPUprofile.root 4 -f

    previous_file=applyPUprofCorrection.root
fi

# MN observables
getMNobservables $previous_file getMNobservables.root

# muons
applyRochesterCorr $previous_file applyRochesterCorr.root $DARWIN_TABLES/Rochester/RoccoR2016aUL.txt false -f
muonEff=$DARWIN_TABLES/Muons/2016_preVFP/Efficiency_muon_generalTracks_Run2016preVFP_UL_trackerMuon.root
applyMuonSelection applyRochesterCorr.root applyMuonSelection.root $muonEff:NUM_TrackerMuons_DEN_genTracks -f
getDimuonSpectrum applyMuonSelection.root getDimuonSpectrum.root
previous_file=applyMuonSelection.root

#if [ "$isMC" = "yes" ]; then ## \todo
#    wget -nc https://gitlab.cern.ch/DasAnalysisSystem/tables/-/raw/master/triggers/muons/ScaleFactor_DoubleMuonTriggers_UL2018.root
#    applyDimuonTriggerStrategy applyMuonSelection.root applyDimuonTriggerStrategy.root ScaleFactor_DoubleMuonTriggers_UL2018.root:ScaleFactorTight_UL2018 -f
#    previous_file=applyDimuonTriggerStrategy.root
#fi

# meta information
getMetaInfo $previous_file meta.info
cat meta.info

# reproducibility
mergeNtuples ntuple.root mergeNtuples.root -c meta.info -f
previous_file=mergeNtuples.root
if [ "$isMC" = "yes" ]; then
    applyMClumi mergeNtuples.root getSumWeights.root applyMClumi.root -c meta.info -f
    applyPUcleaning applyMClumi.root applyPUcleaning.root -c meta.info -f
    applyJEScorrections applyPUcleaning.root applyJEScorrections.root -c meta.info -f
    applyJERsmearing applyJEScorrections.root applyJERsmearing.root -c meta.info -f
    applyBTagSF applyJERsmearing.root applyBTagSF.root -c meta.info -f
    applyPUprofCorrection applyBTagSF.root applyPUprofCorrection.root -c meta.info -f
    previous_file=applyPUprofCorrection.root
fi
applyRochesterCorr $previous_file applyRochesterCorr.root -c meta.info -f
applyMuonSelection applyRochesterCorr.root applyMuonSelection.root -c meta.info -f
previous_file=applyMuonSelection.root
#if [ "$isMC" = "yes" ]; then ## \todo
#    applyDimuonTriggerStrategy applyMuonSelection.root applyDimuonTriggerStrategy.root -c meta.info -f
#    previous_file=applyDimuonTriggerStrategy.root
#fi
getMetaInfo $previous_file meta2.info
diff meta.info meta2.info

import FWCore.ParameterSet.Config as cms
import FWCore.ParameterSet.VarParsing as VarParsing

import os
import json

# CMSSW command line parser 
options = VarParsing.VarParsing ('analysis')
options.register('configFile',
        '',#default value
        VarParsing.VarParsing.multiplicity.singleton, #the configFile object is a single value
        VarParsing.VarParsing.varType.string, #the configFile object is a string
        "JSON config file"#decription of the configFile object
        )

options.parseArguments()

process = cms.Process('GEN')
process.options = cms.untracked.PSet(
    allowUnscheduled = cms.untracked.bool(True),
    wantSummary      = cms.untracked.bool(False)
)
# load standard configurations into process

process.load('Configuration.StandardSequences.Services_cff')
process.load('SimGeneral.HepPDTESSource.pythiapdt_cfi')
process.load('FWCore.MessageService.MessageLogger_cfi')
#NEED Configuration.EventContent.EventContent_cff
process.load('Configuration.EventContent.EventContent_cff')



process.load('Configuration.StandardSequences.Generator_cff')
#NEED IOMC.EventVertexGenerators.VtxSmearedRealistic50ns13TeVCollision_cfi
process.load('IOMC.EventVertexGenerators.VtxSmearedRealistic50ns13TeVCollision_cfi')
# NEED GeneratorInterface.Core.genFilterSummary_cff
process.load('GeneratorInterface.Core.genFilterSummary_cff')
# NEED Configuration.StandardSequences.EndOfProcess_cff
process.load('Configuration.StandardSequences.EndOfProcess_cff')


from os.path import exists
params = {}
if exists(options.configFile):
    # minimal usage to run: cmsRun python_cfg.py configFile=absolute/path/to/Rivet.json
    os.environ['DAS_RIVET_CONFIG'] = options.configFile
    #options.outputFile is a registered argument by default, which is output.root as default
    os.environ['DAS_RIVET_OUTPUT'] = options.outputFile
    print("\n\nusing %s config file and %s output file \n"% (options.configFile, options.outputFile))
    import json
    with open(options.configFile, "r") as f:
        params = json.load(f)
        

# get number of events from json configs or from "maxEvents" command line argument
maxEvents=int(params['maxEvents']) if params['maxEvents'] else options.maxEvents
process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(maxEvents)
)

process.MessageLogger.cerr.FwkReport.reportEvery = 1000
# Input source. Here we dont have input source since were generating MC
process.source = cms.Source("EmptySource")


# random seed is important for batch jobs: could be an environemnt variable or seed = job id for each crab job... 
#process.RandomNumberGeneratorService.generator.initialSeed = int(os.getenv('SEED'))


# generation_step -> genfiltersummary_step -> endjob_step
process.genstepfilter.triggerConditions=cms.vstring("generation_step")

USE_GLOBAL_TAG='False' #don't need gloab tag for now
if USE_GLOBAL_TAG=="True":
    process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
    from Configuration.AlCa.GlobalTag import GlobalTag
    process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:mc', '')



# generators that don't support concurrent lumi blocks. Can't use multithreading with these common generators:
noConcurrentLumiGenerators = [
    "AMPTGeneratorFilter",
    "BeamHaloProducer",
    "CosMuoGenProducer",
    "ExhumeGeneratorFilter",
    "Herwig7GeneratorFilter",
    "HydjetGeneratorFilter",
    "Hydjet2GeneratorFilter",
    "PyquenGeneratorFilter",
    "Pythia6GeneratorFilter",
    "Pythia8EGun",
    "Pythia8GeneratorFilter",
    "Pythia8HadronizerFilter",
    "Pythia8PtAndDxyGun",
    "Pythia8PtGun",
    "ReggeGribovPartonMCGeneratorFilter",
    "SherpaGeneratorFilter",
]



# MC Generator interface in CMSSW
# See 1) https://github.com/cms-sw/cmssw/tree/master/GeneratorInterface
# 2) https://github.com/cms-sw/cmssw/tree/master/Configuration/Generator/python

# Define MC generator
GENERATOR=params["generator"],

GENERATOR="Pythia8GeneratorFilter"
# For multithreading py8 generator, see https://github.com/cms-sw/cmssw/blob/master/GeneratorInterface/Core/test/Pythia8ConcurrentGeneratorFilter_WZ_TuneCP5_13TeV-pythia8_cfg.py

# if generator supports multithreading, then use multithreading
if GENERATOR not in noConcurrentLumiGenerators:
    
    process.options.numberOfThreads=cms.untracked.uint32(4)
    process.options.numberOfStreams=cms.untracked.uint32(4)

# Need an initial estimate for the total cross section, which can be attained by ~ 100K events. 
# Also see https://dasanalysissystem.docs.cern.ch/md__builds_DasAnalysisSystem_Core_Darwin_tables_xsections_README.html

total_cross_sec_estimate = 2022100000.0

process.generator = cms.EDFilter(GENERATOR,
    PythiaParameters = cms.PSet(
        parameterSets = cms.vstring(
            'pythia8CommonSettings', 
            'pythia8CUEP8M1Settings', 
            'processParameters'),
        processParameters = cms.vstring(
            'HardQCD:all = on',
            # 'PhaseSpace:pTHatMin = 2500', 
            'PhaseSpace:pTHatMin = 25',  
            'PhaseSpace:pTHatMax = 7000', 
            'PhaseSpace:bias2Selection = on', 
            'PhaseSpace:bias2SelectionPow = 4.5', 
            'PhaseSpace:bias2SelectionRef = 15.',
            #'PhaseSpace:bias2SelectionRef = 2500.'
            'PartonLevel:MPI=%s' % params["physics"]["MPI"],
            'HadronLevel:all=%s' % params["physics"]["HAD"]

		), 
        # plug in the pythia tune parameters
        pythia8CUEP8M1Settings = cms.vstring(
            # .CMD file
            'Tune:pp 14', 
            'Tune:ee 7', 
            'MultipartonInteractions:pT0Ref=2.4024', 
            'MultipartonInteractions:ecmPow=0.25208', 
            'MultipartonInteractions:expPow=1.6'),
        pythia8CommonSettings = cms.vstring(
            # whether to print various outputs
            'Init:showProcesses = off',
            'Init:showMultipartonInteractions = off',
            'Init:showChangedParticleData = off',
            'Stat:showProcessLevel = off',
            
            'Tune:preferLHAPDF = 2', 
            # Tune:preferLHAPDF = 2 : Means use LHAPDF6 PDFs, i.e. the ones defined in the tunes
            'Main:timesAllowErrors = 1000000', 
            'Check:epTolErr = 0.01', 
            'Beams:setProductionScalesFromLHEF = off', 
            #'SLHA:keepSM = on', 
            'SLHA:minMassSM = 1000.', 
            'ParticleDecays:limitTau0 = on', 
            'ParticleDecays:tau0Max = 10', 
            'ParticleDecays:allowPhotonRadiation = on')
    ),
    comEnergy = cms.double(13000.0),
    
    crossSection = cms.untracked.double(total_cross_sec_estimate),
    filterEfficiency = cms.untracked.double(1.0),
    maxEventsToPrint = cms.untracked.int32(0),
)



# Path and EndPath definitions
process.generation_step = cms.Path(process.pgen)
process.genfiltersummary_step = cms.EndPath(process.genFilterSummary)
process.endjob_step = cms.EndPath(process.endOfProcess)

# Schedule definition
process.schedule = cms.Schedule(
    process.generation_step,
    process.genfiltersummary_step,
    process.endjob_step,
)
# filter all path with the production filter sequence
for path in process.paths:
	getattr(process,path)._seq = process.generator * getattr(process,path)._seq 

# customisation of the process.

# Automatic addition of the customisation function from Configuration.GenProduction.rivet_customize



	
def Rivet_customise(process):
     process.load('GeneratorInterface.RivetInterface.rivetAnalyzer_cfi')

     #see https://github.com/cms-sw/cmssw/blob/master/GeneratorInterface/RivetInterface/plugins/RivetAnalyzer.cc
     process.rivetAnalyzer.AnalysisNames = cms.vstring(
         'RivetNtupliser')
     process.rivetAnalyzer.CrossSection = cms.double(total_cross_sec_estimate)
    #  process.rivetAnalyzer.OutputFile='' causes errors in rivet
     # disable production of .yoda files, but sometimes crashes
     

     process.generation_step+=process.rivetAnalyzer
     return(process)


#call to our customisation function
process = Rivet_customise(process)



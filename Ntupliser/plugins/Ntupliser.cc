#include <cassert>
#include <cmath>

#include <filesystem>
#include <functional>
#include <regex>
#include <string>

#include "Ntupliser.h"

#include "Core/Objects/interface/Photon.h"
#include "Core/Objects/interface/Format.h"

#include "FWCore/Framework/interface/MakerMacros.h"

#include <boost/property_tree/json_parser.hpp>
#include <boost/exception/all.hpp>

namespace pt = boost::property_tree;

using namespace std;
namespace fs = filesystem;

namespace /* anonymous */ {
// sort by decreasing pt
template<typename DAStype> void Sort (vector<DAStype>*& collection)
{
    sort(collection->begin(), collection->end(),
            [](DAStype &a, DAStype &b) { return a.p4.Pt() > b.p4.Pt(); } );
}
template<> void Sort (vector<DAS::FourVector>*& collection)
{
    sort(collection->begin(), collection->end(),
            [](DAS::FourVector &a, DAS::FourVector &b) { return a.Pt() > b.Pt(); } );
}
} // end of anonymous namespace

////////////////////////////////////////////////////////////////////////////////
/// Constructor, only initialising the members.
Ntupliser::Ntupliser (edm::ParameterSet const& cfg)
try :
    p(cfg, consumesCollector()), h(p),
    tree(fs_->make<TTree>("events","events")),
    metainfo(tree, p.config),
    // jets
    recJets_(p.jets               ? new vector<DAS::RecJet>     : nullptr),
    HLTjets_(p.jets && p.triggers ? new vector<DAS::FourVector> : nullptr),
    genJets_(p.jets && p.isMC     ? new vector<DAS::GenJet>     : nullptr),
    // muons
    recMuons_(           p.muons ? new vector<DAS::RecMuon> : nullptr),
    genMuons_(p.isMC  && p.muons ? new vector<DAS::GenMuon> : nullptr),
    // photons
    recPhotons_(           p.photons ? new vector<DAS::RecPhoton> : nullptr),
    genPhotons_(p.isMC  && p.photons ? new vector<DAS::GenPhoton> : nullptr),
    // event variables
    jetTrigger_    (p.triggers ? new DAS::Trigger : nullptr),
    muonTrigger_   (p.triggers ? new DAS::Trigger : nullptr),
    zbTrigger_     (p.triggers ? new DAS::Trigger : nullptr),
    genEvent_      (p.isMC ? new DAS::GenEvent : nullptr),
    recEvent_      (         new DAS::RecEvent          ),
    met_           (new DAS::MET          ),
    pileup_        (new DAS::PileUp       ),
    primaryvertex_ (new DAS::PrimaryVertex)
{
    cout << __FILE__ << ':' << __func__ << endl;

    using namespace DAS;

    if (p.photons) // photon variations
    for (TString source: RecPhoton::uncs) {
        metainfo.Set<TString>("variations", RecPhoton::ScaleVar, source + SysDown);
        metainfo.Set<TString>("variations", RecPhoton::ScaleVar, source + SysUp);
    }
}
catch (boost::exception& e) {
    throw cms::Exception("Ntupliser") << boost::diagnostic_information(e);
}

//////////////////////////////////////////////////////////////////////////////////////////
/// Finds the generated muons, and applies some hard cuts on the phase space.
void Ntupliser::getGenMuons ()
{
    for (const reco::Candidate &lepton: *genLeptons) {
        if (abs(lepton.pdgId()) != 13) continue;

        // we convert the complicated CMSSW object to our simple DAS object
        DAS::GenMuon genmu = h.GetGenMu(lepton);

        // we keep only get with pt > 15 GeV and |eta| < 3.0 (to have a margin
        // w.r.t. tracker acceptance and treat migrations)
        if (genmu.p4.Pt() < 10 /* GeV */ || abs(genmu.p4.Eta()) > 3.0) continue;

        genMuons_->push_back(genmu);
    }

    Sort<DAS::GenMuon>(genMuons_);
}

namespace /* anonymous */ {
/// Helper function to check if a gen particle has a Z boson in its ancestors
bool hasZAncestor (const reco::Candidate &gp) {
    return gp.pdgId() == 23 || (gp.numberOfMothers() > 0 && hasZAncestor(*gp.mother(0)));
}
} // anonymous namespace

////////////////////////////////////////////////////////////////////////////////
/// Finds the generated photons and applies some hard cuts to the phase space
void Ntupliser::getGenPhotons ()
{
    // Collect charged leptons that would be dressed
    vector<const reco::GenParticle *> charged_leptons;
    for (const reco::GenParticle &gp: *genParticles) {
        auto id = abs(gp.pdgId());
        if (gp.status() == 1 && (id == 11 || id == 13))
            charged_leptons.push_back(&gp);
    }

    for (const reco::GenParticle &gp: *genParticles) {
        // Consider stable (status 1) photons
        if (gp.status() != 1 || gp.pdgId() != 22) continue;

        // Phase space cuts
        const bool zAncestor = hasZAncestor(gp);
        if (zAncestor && gp.pt() < 1) continue;
        if (!zAncestor && (gp.pt() < 10 || abs(gp.eta()) > 3.0)) continue;

        // We store dressed leptons. Remove any photon falling within the
        // dressing cones, since we'd have no way of removing them afterwards.
        if (any_of(charged_leptons.begin(), charged_leptons.end(),
                   [gp](auto lep) { return reco::deltaR(lep->p4(), gp.p4()) < 0.1; }))
            continue;

        genPhotons_->push_back(h.GetGenPhoton(gp, zAncestor));
    }

    Sort<DAS::GenPhoton>(genPhotons_);
}


////////////////////////////////////////////////////////////////////////////////
/// Finds the reconstructed jets, and applies some hard cuts on the phase space.
///
/// `MyJetCollection` is expected to be either `JetCollection` or `JetFlavourInfoMatchingCollection`
/// (according to the flavour flag)
template<typename MyJetCollection> void Ntupliser::getGenJets (edm::Handle<MyJetCollection>& mygenjets)
{
    //for ((typename MyJetCollection)::const_iterator itJet = mygenjets->begin();
    for (auto itJet = mygenjets->begin();
            itJet != mygenjets->end(); ++itJet) {

        // we convert the complicated CMSSW object to our simple DAS object
        DAS::GenJet genjet = h.GetGenJet(*itJet);

        // we keep only jet with pt > 20 GeV and in the tracker acceptance
        //if (genjet.p4.Pt() < 10 /* GeV */ || abs(genjet.p4.Eta()) > 5.0) continue;
        /// \todo externalize option for phase space cut
        /// \note slimmedJets are only defined from 10 GeV

        genJets_->push_back(genjet);
    }

    Sort<DAS::GenJet>(genJets_);
}

////////////////////////////////////////////////////////////////////////////////
/// Finds the reconstructed muons, and applies some hard cuts on the phase space.
void Ntupliser::getRecMuons ()
{
    for(pat::MuonCollection::const_iterator itMu = recmuons->begin();
            itMu != recmuons->end(); ++itMu) {

        // we convert the complicated CMSSW object to our simple DAS object
        DAS::RecMuon recmu = h.GetRecMu(*itMu);

        // we keep only muons with pt > 10 GeV and in the muon chamber acceptance
        if (recmu.p4.Pt() < 10 /* GeV */ || abs(recmu.p4.Eta()) > 2.4) continue;

        recMuons_->push_back(recmu);
    }

    Sort<DAS::RecMuon>(recMuons_);
}

////////////////////////////////////////////////////////////////////////////////
/// Finds the reconstructed photons, and applies some hard cuts on the phase space.
void Ntupliser::getRecPhotons ()
{
    for (pat::PhotonCollection::const_iterator it = recphotons->begin();
         it != recphotons->end(); ++it) {

        // we convert the complicated CMSSW object to our simple DAS object
        DAS::RecPhoton recphoton = h.GetRecPhoton(*it);

        // we keep only photons with pt > 5 GeV
        if (recphoton.p4.Pt() < 5 /* GeV */) continue;

        recPhotons_->push_back(recphoton);
    }

    Sort<DAS::RecPhoton>(recPhotons_);
}


////////////////////////////////////////////////////////////////////////////////
/// Finds the reconstructed jets, and applies some hard cuts on the phase space.
void Ntupliser::getRecJets ()
{
    for(pat::JetCollection::const_iterator itJet = recjets->begin();
            itJet != recjets->end(); ++itJet) {

        // we convert the complicated CMSSW object to our simple DAS object
        DAS::RecJet recjet = h.GetRecJet(*itJet);

        // we keep all jets and give set the weight to 0 for jets not fullfiling the quality criterion
        if (!h.JetID(*itJet)) recjet.weights.front() = 0.;

        // we keep only jet with pt > 20 GeV and in the tracker acceptance
        //if (recjet.p4.Pt() < 10 /* GeV */ || abs(recjet.p4.Eta()) > 5.0) continue;
        /// \todo externalize option for phase space cut
        // (note: slimmedJets are only defined from 10 GeV)

        recJets_->push_back(recjet);
    }

    Sort<DAS::RecJet>(recJets_);
}

//////////////////////////////////////////////////////////////////////////////////////////
/// Called before looping over the events.
///
/// Basically, it set up the branches.
void Ntupliser::beginJob()
{
    cout << __FILE__ << ':' << __func__ << endl;

    if (!p.isMC) {
        const auto luminosity = p.config.get_child_optional("luminosity");
        if (luminosity && luminosity->count("pileup")) {
            auto pileup = luminosity->get<fs::path>("pileup");
            cout << "Extracting pileup normalisation from " << pileup << endl;
            if (p.sandbox) pileup = pileup.filename();
            if (!fs::exists(pileup))
                throw cms::Exception("Ntupliser") << pileup << " could not be found";
            pt::read_json(pileup.string(), pileup_json);
        }
        else cout << "\x1B[33mNo input pileup normalisation\x1B[0m" << endl;
    }

    // event
    if (p.isMC)
        tree->Branch("genEvent",&genEvent_);
    tree->Branch("recEvent",&recEvent_);
    if (p.triggers) {
        tree->Branch("jetTrigger",&jetTrigger_);
        tree->Branch("muonTrigger",&muonTrigger_);
        tree->Branch("zbTrigger",&zbTrigger_);
    }

    tree->Branch("pileup",&pileup_);
    tree->Branch("primaryvertex",&primaryvertex_);
    tree->Branch("met",&met_);

    // jets
    if (p.jets) {
        tree->Branch("recJets",&recJets_);
        if (p.triggers)
            tree->Branch("hltJets", &HLTjets_);
        if (p.isMC)
            tree->Branch("genJets", &genJets_);
    }

    // muons
    if (p.muons) {
        tree->Branch("recMuons",&recMuons_);
        if (p.isMC)
            tree->Branch("genMuons", &genMuons_);
    }

    // photons
    if (p.photons) {
        tree->Branch("recPhotons", &recPhotons_);
        if (p.isMC)
            tree->Branch("genPhotons", &genPhotons_);
    }
}

void Ntupliser::endJob()
{
    cout << __FILE__ << ':' << __func__ << endl;
    metainfo.Set<bool>("git", "complete", true);
}

void Ntupliser::analyze(edm::Event const& iEvent, edm::EventSetup const& iSetup)
{
    // reset and initialise member variables (filled to the tree)
    reset();
    initialise(iEvent);

    // check trigger
    if (p.triggers) {
        bool passTrigger = trigger(iEvent);
        if (!p.isMC && !passTrigger) return;
        if (p.jets)
            getHLTjets(iEvent); //Fill High-Level-Trigger jets
    }
    fillMET(iEvent);

    // event variables
    getEventVariables(iEvent);

    // jet variables
    if (p.jets) {
        getRecJets();
        if (p.isMC) {
            if (p.flavour)
                getGenJets(theJetFlavourInfos);
            else
                getGenJets(genjets);
        }
    }

    // muon variables
    if (p.muons) {
        getRecMuons();
        if (p.isMC)
            getGenMuons();
    }

    // photon variables
    if (p.photons) {
        getRecPhotons();
        if (p.isMC)
            getGenPhotons();
    }

    tree->Fill();
}

//////////////////////////////////////////////////////////////////////////////////////////
/// Just a common method to reset all branches at each new event.
void Ntupliser::reset()
{
    if (p.isMC)
        genEvent_->clear();
    recEvent_->clear();

    if (p.triggers) {
        jetTrigger_->clear();
        muonTrigger_->clear();
    }
    pileup_->clear();
    primaryvertex_->clear();
    met_->clear();

    if (p.jets) {
        if (p.isMC)
            genJets_->clear();
        if (p.triggers)
            HLTjets_->clear();
        recJets_->clear();
    }

    if (p.muons) {
        recMuons_->clear();
        if (p.isMC)
            genMuons_->clear();
    }

    if (p.photons) {
        recPhotons_->clear();
        if (genPhotons_)
            genPhotons_->clear();
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
/// Called at the beginning of each new event.
void Ntupliser::initialise (edm::Event const& iEvent)
{
    // jets
    if (p.isMC) {
#ifdef PS_WEIGHTS
        iEvent.getByToken(p.lheToken);
#endif
        iEvent.getByToken(p.genEvtInfoToken, genEvtInfo);
        if (p.jets) {
            iEvent.getByToken(p.genjetsToken, genjets);
            if (p.flavour)
                iEvent.getByToken(p.jetFlavourInfosToken, theJetFlavourInfos );
        }
        iEvent.getByToken(p.genParticlesToken, genParticles);
    }
    if (p.jets)
        iEvent.getByToken(p.recjetsToken, recjets);

    // muons
    if (p.muons) {
        if (p.isMC)
            iEvent.getByToken(p.genLeptonsToken, genLeptons);
        iEvent.getByToken(p.recmuonsToken, recmuons);
    }

    // photons
    if (p.photons)
        iEvent.getByToken(p.recphotonsToken, recphotons);

    // pile-up
    if (p.isMC)
        iEvent.getByToken(p.pileupInfoToken, pileupInfo);
    iEvent.getByToken(p.rhoToken,rho);

    // vertex
    iEvent.getByToken(p.recVtxsToken,recVtxs);

    // trigger
    if (p.triggers) {
        iEvent.getByToken(p.triggerResultsToken, triggerResults);
        iEvent.getByToken(p.triggerObjectsToken, triggerObjects);

        // HLT
        iEvent.getByToken(p.triggerPrescalesToken,triggerPrescales);

        // L1
        iEvent.getByToken(p.triggerPrescalesl1minToken, triggerPrescalesl1min);
        iEvent.getByToken(p.triggerPrescalesl1maxToken, triggerPrescalesl1max);
    }

    // MET
    iEvent.getByToken(p.metToken,met);
    iEvent.getByToken(p.metResultsToken, metResults);
}

namespace /* anonymous */ {
string DelLastDigits (string n)
{
    while (isdigit(n.back()))
        n.pop_back();
    return n;
}
} // end of anonymous namespace

//////////////////////////////////////////////////////////////////////////////////////////
/// Finds the bits corresponding to the HLT jet pt triggers.
bool Ntupliser::trigger (edm::Event const& iEvent)
{
    const auto &names = iEvent.triggerNames(*triggerResults);

    bool passTrigger(false);
    for (unsigned int k = 0; k < p.triggerNames_.size(); ++k) {
        bool bit(false);
        int preHLT(1), preL1min(1), preL1max(1);
        string name;

        for (unsigned int itrig = 0; itrig < triggerResults->size(); ++itrig) {
            name = names.triggerName(itrig);

            //--- erase the the version number----
            name = DelLastDigits(name);
            if (name != p.triggerNames_[k]) continue;

            bit = triggerResults->accept(itrig);
            // indirect way of testing the CMSSW version is to check the GCC version
#if __GNUC__ < 12 // Run 2 (CMSSW_12, GCC 10)
            preHLT = triggerPrescales->getPrescaleForIndex(itrig);
            preL1min = triggerPrescalesl1min->getPrescaleForIndex(itrig);
            preL1max = triggerPrescalesl1max->getPrescaleForIndex(itrig);
#else // Run 3 (CMSSW_13, GCC 12)
            preHLT = triggerPrescales->getPrescaleForIndex<double>(itrig);
            preL1min = triggerPrescalesl1min->getPrescaleForIndex<double>(itrig);
            preL1max = triggerPrescalesl1max->getPrescaleForIndex<double>(itrig);
#endif
            break;
        }
        //--- if at least one monitored trigger has fired passTrigger becomes true
        passTrigger += bit;

        // Identify trigger collection
        regex jetTriggerpattern("HLT_(AK4|AK8|HI|HIAK4|HIAK8|Di)?PFJet");
        regex muonTriggerpattern("HLT_(Mu|IsoMu)");
        regex zbTriggerpattern("HLT_ZeroBias");
        // example for Run18A (both low & high pt jets)
        // HLT_PFJet40_v    153 153
        // HLT_PFJet60_v    154 154
        // HLT_PFJet80_v    155 155
        // HLT_PFJet140_v   156 156
        // HLT_PFJet200_v   157 157
        // HLT_PFJet260_v   158 158
        // HLT_PFJet320_v   159 159
        // HLT_PFJet400_v   160 160
        // HLT_PFJet450_v   161 161
        // HLT_PFJet500_v   162 162
        // HLT_PFJet550_v   163 163
        if (regex_search(name, jetTriggerpattern)) {
            jetTrigger_->Bit.push_back(bit);
            jetTrigger_->PreHLT.push_back(preHLT);
            jetTrigger_->PreL1min.push_back(preL1min);
            jetTrigger_->PreL1max.push_back(preL1max);
        }
        // HLT_IsoMu24_eta2p1_v
        // HLT_IsoMu24_v
        // HLT_IsoMu27_v
        // HLT_IsoMu30_v
        // HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_v
        // HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_v
        // HLT_Mu19_TrkIsoVVL_Mu9_TrkIsoVVL_DZ_v
        // HLT_Mu19_TrkIsoVVL_Mu9_TrkIsoVVL_v
        else if (regex_search(name, muonTriggerpattern)) {
            muonTrigger_->Bit.push_back(bit);
            muonTrigger_->PreHLT.push_back(preHLT);
            muonTrigger_->PreL1min.push_back(preL1min);
            muonTrigger_->PreL1max.push_back(preL1max);
        }
        else if (regex_search(name, zbTriggerpattern)) {
            zbTrigger_->Bit.push_back(bit);
            zbTrigger_->PreHLT.push_back(preHLT);
            zbTrigger_->PreL1min.push_back(preL1min);
            zbTrigger_->PreL1max.push_back(preL1max);
        }
        else throw cms::Exception("Ntupliser") << "No dedicated collection exists for " << name;
    }
    return passTrigger;
}

//////////////////////////////////////////////////////////////////////////////////////////
/// Fill the MET flags to the array
void Ntupliser::fillMET (edm::Event const& iEvent)
{
    const edm::TriggerNames &namesMet = iEvent.triggerNames(*metResults);
    for(unsigned int k=0; k < p.metNames_.size(); ++k) {
        bool bit(false);
        for(unsigned int itrig=0; itrig<metResults->size(); ++itrig) {
            string met_name = string(namesMet.triggerName(itrig));
            if (met_name == p.metNames_[k]) {
                bit = metResults->accept(itrig);
                break;
            }
        }
        met_->Bit.push_back(bit);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////
/// Finds HLT jets and simply stores them in a `FourVector`.
void Ntupliser::getHLTjets (edm::Event const& iEvent)
{
    const auto &names = iEvent.triggerNames(*triggerResults);

    // loop over all possible triggers and only keep object from the PFJet
    // triggers
    for (pat::TriggerObjectStandAlone obj: *triggerObjects){
        obj.unpackPathNames(names);
        //vector<string> pathNamesAll  = obj.pathNames(false);
        vector<string> pathNamesLast = obj.pathNames(true);

        // first, look if the HLT object is a HLT jet
        bool isHLTjet = false;
        string sNow;
        for (auto s: pathNamesLast) {
            s = DelLastDigits(s); // remove the version of the trigger (i.e. HLT_PFJetXX_v*)
            if (p.HLTjet_triggerNames.count(s) == 0) continue; // typically ignore mu triggers
            isHLTjet = true;
            sNow = s;
            break;
        }

        // then add it unless it has already been added
        if(isHLTjet) {
            DAS::FourVector P4(obj.pt(), obj.eta(), obj.phi(), obj.mass());
            bool isIn = false;
            for(const auto &v : *HLTjets_)
                if(v == P4) {isIn = true; break;}
            if(!isIn)
                HLTjets_->push_back(P4);
        }
    }

    Sort<DAS::FourVector>(HLTjets_);
}

/////////////////////////////////////////////////////////////////////////////////
/// Gets information about the event (run number, etc.), the pile-up, the MET and
/// the primary vertex.
///
/// https://cmsdoxygen.web.cern.ch/cmsdoxygen/CMSSW_10_6_19/doc/html/d9/d53/classPileupSummaryInfo.html
void Ntupliser::getEventVariables (edm::Event const& iEvent)
{
    // event
    recEvent_->runNo = iEvent.id().run(); // note: always 1 for MC
    recEvent_->evtNo = iEvent.id().event();
    recEvent_->lumi = iEvent.id().luminosityBlock();

    recEvent_->weights.resize(1);
    recEvent_->weights.front() = 1;

    if (p.isMC) {
        genEvent_->hard_scale = genEvtInfo->qScale();
#ifdef PS_WEIGHTS
#error "Generator weights haven't been tested yet! Implementation may not be ready"
        // model variations
        auto& weights = lhe->weights();
        for (auto& w: weights) {
            cout << "Weight " << w.id << " " <<  w.wgt << '\n';
            genEvent_->weights.push_back(w.wgt); /// \todo multiply weight by `genEvtInfo->weight()`?
        }
        cout << flush;
#else
        genEvent_->weights.resize(1);
        genEvent_->weights.front() = genEvtInfo->weight();
#endif
    }

    // pile-up
    // https://cmsdoxygen.web.cern.ch/cmsdoxygen/CMSSW_12_4_0/doc/html/d9/d53/classPileupSummaryInfo.html
    pileup_->rho  = *rho;
    pileup_->nVtx = recVtxs->size();
    if (p.isMC)
    for(auto PUI = pileupInfo->begin(); PUI != pileupInfo->end(); ++PUI) {
        if (PUI->getBunchCrossing() != 0) continue; /// \todo check out-of-time pileup

        // for pile-up profile correction
        // Adapted from [TWiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/PileupJSONFileforData):
        // In order to do reasonable comparisons between data and MC, it is necessary to understand exactly what the histogram produced by pileupCalc.py means so that it can be compared with the correct quantity in MC.
        // The way that pileup events are generated in MC, given an input pileup distribution, is as follows:
        pileup_->trpu = PUI->getTrueNumInteractions(); // average pileup conditions under which the event is generated
        pileup_->intpu = PUI->getPU_NumInteractions(); // the number of pileup events for the in-time bunch crossing is selected from a Poisson distribution with a mean equal to the "true" pileup

        // for pile-up staub cleaning
        auto &ptHatVec = PUI->getPU_pT_hats();
        pileup_->pthats = ptHatVec;

        const vector<edm::EventID>& evtIDs = PUI->getPU_EventID();
        for (const auto& evtID: evtIDs)
            pileup_->MBevents.push_back(evtID.event());
    }
    else if (pileup_json.size() > 0) {
        auto runNo = to_string(iEvent.id().run());
        auto run = pileup_json.get_child_optional(runNo);
        if (!run)
            throw cms::Exception("Ntupliser") << runNo << " could not be found in the pileup latest file";
        for (auto& LS: *run) {
            auto it = LS.second.begin();  // ["LS", "inst lumi", "xsec RMS", "av xsec"]
            auto LSno = it->second.get_value<unsigned int>(); // LS
            if (LSno != iEvent.id().luminosityBlock()) continue;
            ++it; //auto instLumi = it->second.get_value<float>(); // inst lumi
            ++it; //auto xsecRMS = it->second.get_value<float>(); // xsec RMS
            ++it; auto avXsec = it->second.get_value<float>(); // av xsec
            pileup_->trpu = avXsec * DAS::PileUp::MBxsec;
            break;
        }
    }

    // primary vertex
    const auto& PV = (*recVtxs)[0];
    primaryvertex_->Rho  = PV.position().Rho();
    primaryvertex_->z    = PV.z();
    primaryvertex_->ndof     = PV.ndof();
    primaryvertex_->chi2     = PV.chi2();
    primaryvertex_->fake     = PV.isFake();
    if (p.isMC) {
        const auto& rv = PV.position();
        const auto& gv = (*genParticles)[0].daughter(0)->vertex();
        primaryvertex_->distGen = hypot( rv.x() - gv.x(),
                                  hypot( rv.y() - gv.y(),
                                         rv.z() - gv.z()) );
    }

    // MET
    met_->Et = (*met)[0].et();
    met_->SumEt = (*met)[0].sumEt();
    met_->Pt = (*met)[0].pt(); /// \todo compare to `(*met)[0].pt()` to `(*met)[0].uncorPt()`
    met_->Phi = (*met)[0].phi();
    // https://twiki.cern.ch/twiki/bin/viewauth/CMS/MissingETRun2Corrections#Type_I_Correction_Propagation_of
    // https://twiki.cern.ch/twiki/bin/view/CMS/MissingETUncertaintyPrescription
}

Ntupliser::~Ntupliser()
{
    cout << __FILE__ << ':' << __func__ << endl;
}

DEFINE_FWK_MODULE(Ntupliser);

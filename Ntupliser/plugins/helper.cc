#include "helper.h"
#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Lepton.h"

#include <iostream>
#include <limits>

using namespace std;

////////////////////////////////////////////////////////////////////////////////
/// Constructor
///
/// Only giving parameters in reference
DAS::Helper::Helper (DAS::Parameters& parameters) : p(parameters) {}

////////////////////////////////////////////////////////////////////////////////
/// \brief Helper to get `DAS::GenJet` from MiniAOD without flavour
DAS::GenJet DAS::Helper::GetGenJet (const reco::Jet &ijet)
{
    DAS::GenJet jjet;

    // kinematics
    jjet.p4 = DAS::FourVector(ijet.p4());

    return jjet;
}

////////////////////////////////////////////////////////////////////////////////
/// \brief Helper to get `DAS::GenJet` from MiniAOD with flavour
DAS::GenJet DAS::Helper::GetGenJet (const reco::JetFlavourInfoMatching & ijet)
{
    const reco::Jet& Jet = *(ijet.first.get());
    const reco::JetFlavourInfo& Info = ijet.second;

    DAS::GenJet jjet = GetGenJet(Jet);

    // parton flavour
    jjet.partonFlavour = Info.getPartonFlavour();

    // heavy-flavour hadrons
    jjet.nBHadrons = Info.getbHadrons().size();
    jjet.nCHadrons = Info.getcHadrons().size();

    return jjet;
}

////////////////////////////////////////////////////////////////////////////////
/// \brief Helper to get `DAS::RecJet` from MiniAOD
///
/// [General instructions](https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideBtagValidation)
/// [DeepJet](https://twiki.cern.ch/twiki/bin/view/CMS/DeepJet#94X_installation_recipe_X_10)
DAS::RecJet DAS::Helper::GetRecJet (const pat::Jet &ijet) 
{
    DAS::RecJet jjet;

    // kinematics
    jjet.p4      = ijet.correctedP4("Uncorrected");

    // JetMET business
    jjet.area    = ijet.jetArea();
    jjet.puID    = p.PUjetID ?  ijet.userFloat("pileupJetId:fullDiscriminant") : 1.;

    if (p.flavour) {
        // parton flavour
        jjet.partonFlavour = ijet.partonFlavour();

        // heavy-flavour hadrons
        jjet.nBHadrons = ijet.jetFlavourInfo().getbHadrons().size();
        jjet.nCHadrons = ijet.jetFlavourInfo().getcHadrons().size();

        // heavy-flavour tagging
        jjet.DeepJet.probb    = ijet.bDiscriminator("pfDeepFlavourJetTags:probb");
        jjet.DeepJet.probbb   = ijet.bDiscriminator("pfDeepFlavourJetTags:probbb"); 
        jjet.DeepJet.problepb = ijet.bDiscriminator("pfDeepFlavourJetTags:problepb"); 
        jjet.DeepJet.probc    = ijet.bDiscriminator("pfDeepFlavourJetTags:probc");
        //jjet.DeepJet.probcc   = ijet.bDiscriminator("pfDeepFlavourJetTags:probcc"); 
        jjet.DeepJet.probuds  = ijet.bDiscriminator("pfDeepFlavourJetTags:probuds"); 
        jjet.DeepJet.probg    = ijet.bDiscriminator("pfDeepFlavourJetTags:probg"); 

        static const auto eps = 10*numeric_limits<float>::epsilon();
        if (abs(jjet.DeepJet.probb + jjet.DeepJet.probbb + jjet.DeepJet.problepb + jjet.DeepJet.probc + /*jjet.DeepJet.probcc +*/ jjet.DeepJet.probuds + jjet.DeepJet.probg - 1) > eps)
            cerr << abs(jjet.DeepJet.probb + jjet.DeepJet.probbb + jjet.DeepJet.problepb + jjet.DeepJet.probc + /*jjet.DeepJet.probcc +*/ jjet.DeepJet.probuds + jjet.DeepJet.probg - 1) << ' '  << eps << endl;
    }

    jjet.scales.resize(1, 1.0);

    return jjet;
}

////////////////////////////////////////////////////////////////////////////////
/// \brief Testing tight ID lepton veto definition for jets (official from JetMET)
///
/// The values for Run 2 correspond to UL and are not expected to change.
/// Instead, the values for Run 3 are only preliminary and are subject to changes.
///
/// \note The formulae in blue from the
///       [original TWiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/JetID)
///       have been copied and their format adapted.
bool DAS::Helper::JetID (const pat::Jet &jet)
{
    const auto eta = jet.eta();

    auto NHF  = jet.neutralHadronEnergyFraction();
    auto NEMF = jet.photonEnergyFraction();
    auto MUF  = jet.muonEnergyFraction();
    auto NumConst = jet.neutralMultiplicity() + jet.chargedMultiplicity();

    auto CHF  = jet.chargedHadronEnergyFraction();
    auto CHM  = jet.chargedHadronMultiplicity();
    auto CEMF = jet.chargedEmEnergyFraction();

    auto NumNeutralParticle = jet.neutralMultiplicity();

    switch (p.year) {
        case 2016:
            return (abs(eta)<=2.4 && CEMF<0.8 && CHM>0 && CHF>0 && NumConst>1 && NEMF<0.9 && MUF <0.8 && NHF < 0.9)
                || (p.CHS && abs(eta)>2.4 && abs(eta)<=2.7 && NEMF<0.99 && NHF < 0.9)
                || (p.CHS && NEMF>0.0 && NEMF<0.99 && NHF<0.9 && NumNeutralParticle>1 && abs(eta)>2.7 && abs(eta)<=3.0)
                || (p.CHS && NEMF<0.90 && NHF>0.2 && NumNeutralParticle>10 && abs(eta)>3.0)
                || (p.PUPPI && abs(eta)>2.4 && abs(eta)<=2.7 && NEMF<0.99 && NHF < 0.98)
                || (p.PUPPI && NumNeutralParticle>=1 && abs(eta)>2.7 && abs(eta)<=3.0)
                || (p.PUPPI && NEMF<0.90 && NumNeutralParticle>2 && abs(eta)>3.0);

        case 2017:
        case 2018:
            return (abs(eta)<=2.6 && CEMF<0.8 && CHM>0 && CHF>0 && NumConst>1 && NEMF<0.9 && MUF <0.8 && NHF < 0.9)
                || (p.CHS && abs(eta)>2.6 && abs(eta)<=2.7 && CEMF<0.8 && CHM>0 && NEMF<0.99 && MUF <0.8 && NHF < 0.9)
                || (p.CHS && NEMF>0.01 && NEMF<0.99 && NumNeutralParticle>1 && abs(eta)>2.7 && abs(eta)<=3.0)
                || (p.CHS && NEMF<0.90 && NHF>0.2 && NumNeutralParticle>10 && abs(eta)>3.0)
                || (p.PUPPI && abs(eta)>2.6 && abs(eta)<=2.7 && CEMF<0.8 && NEMF<0.99 && MUF <0.8 && NHF < 0.9)
                || (p.PUPPI && NHF<0.9999 && abs(eta)>2.7 && abs(eta)<=3.0)
                || (p.PUPPI && NEMF<0.90 && NumNeutralParticle>2 && abs(eta)>3.0);
        case 2022:
        case 2023:
        case 2024:
            return (abs(eta)<=2.6 && CEMF<0.8 && CHM>0 && CHF>0.01 && NumConst>1 && NEMF<0.9 && MUF <0.8 && NHF < 0.99)
                || (p.PUPPI && abs(eta)>2.6 && abs(eta)<=2.7 && CEMF<0.8 && NEMF<0.99 && MUF <0.8 && NHF < 0.9)
                || (p.PUPPI && NHF<0.99 && abs(eta)>2.7 && abs(eta)<=3.0)
                || (p.PUPPI &&NEMF<0.40 && NumNeutralParticle>=2 && abs(eta)>3.0)
                || (p.CHS && abs(eta)>2.6 && abs(eta)<=2.7 && CEMF<0.8 && CHM>0 && NEMF<0.99 && MUF <0.8 && NHF < 0.9)
                || (p.CHS && NEMF<0.99 && NHF<0.99 && NumNeutralParticle>1 && abs(eta)>2.7 && abs(eta)<=3.0)
                || (p.CHS && NEMF<0.40 && NumNeutralParticle>10 && abs(eta)>3.0);
        default:
            auto err_msg = "\x1B[31m\x1B[1mThe JetID has not yet been imlemented in DAS for "s
                                           + to_string(p.year) + "\x1B[0m"s;
            throw cms::Exception("Ntupliser") << err_msg;
    }
}

////////////////////////////////////////////////////////////////////////////////
/// \brief Helper to get `DAS::GenMuon` from MiniAOD
DAS::GenMuon DAS::Helper::GetGenMu (const reco::Candidate &mu)
{
    DAS::GenMuon Mu;

    // kinematics
    Mu.p4 = mu.p4();

    Mu.Q = mu.charge();

    return Mu;
}

////////////////////////////////////////////////////////////////////////////////
/// \brief Helper to get `DAS::RecMuon` from MiniAOD
DAS::RecMuon DAS::Helper::GetRecMu (const pat::Muon &mu) 
{
    DAS::RecMuon Mu;

    // kinematics
    Mu.p4 = mu.p4();

    Mu.selectors = mu.selectors();
    Mu.Q = mu.charge();

    // tracking properties
    Mu.Dxy = mu.dB(pat::Muon::PV2D);
    Mu.Dz  = mu.dB(pat::Muon::PVDZ);
    if (mu.innerTrack().isNonnull())
        Mu.nTkHits = mu.innerTrack()->hitPattern().trackerLayersWithMeasurement();

    return Mu;
}

////////////////////////////////////////////////////////////////////////////////
/// \brief Helper to get `DAS::GenPhoton` from MiniAOD
DAS::GenPhoton DAS::Helper::GetGenPhoton (const reco::GenParticle &photon, bool zAncestor)
{
    DAS::GenPhoton dasPhoton;

    // kinematics
    dasPhoton.p4 = photon.p4();
    dasPhoton.zAncestor = zAncestor;
    dasPhoton.prompt = photon.isPromptFinalState();

    return dasPhoton;
}

////////////////////////////////////////////////////////////////////////////////
/// \brief Helper to get `DAS::RecLep` from MiniAOD
DAS::RecPhoton DAS::Helper::GetRecPhoton (const pat::Photon &photon)
{
    DAS::RecPhoton dasPhoton;

    // kinematics
    dasPhoton.p4 = photon.p4();
    const auto raw_energy = dasPhoton.p4.E();
    dasPhoton.scales = decltype(dasPhoton.scales){{
        photon.userFloat("ecalEnergyPostCorr") / raw_energy,
        photon.userFloat("energyScaleUp")      / raw_energy,
        photon.userFloat("energyScaleDown")    / raw_energy,
        photon.userFloat("energySigmaPhiUp")   / raw_energy,
        photon.userFloat("energySigmaPhiDown") / raw_energy,
        photon.userFloat("energySigmaRhoUp")   / raw_energy,
        photon.userFloat("energySigmaRhoDown") / raw_energy,
    }};
    dasPhoton.ecalEnergyErrPostCorr = photon.userFloat("ecalEnergyErrPostCorr");

    // supercluster
    dasPhoton.scEta = photon.superCluster()->eta();
    dasPhoton.sigmaIEtaIEta = photon.full5x5_sigmaIetaIeta();

    // ID
    dasPhoton.hOverE = photon.hadronicOverEm();

    if (photon.photonID("cutBasedPhotonID-Fall17-94X-V2-loose"))
        dasPhoton.selectors |= DAS::RecPhoton::CutBasedLoose;
    if (photon.photonID("cutBasedPhotonID-Fall17-94X-V2-medium"))
        dasPhoton.selectors |= DAS::RecPhoton::CutBasedMedium;
    if (photon.photonID("cutBasedPhotonID-Fall17-94X-V2-tight"))
        dasPhoton.selectors |= DAS::RecPhoton::CutBasedTight;
    if (photon.photonID("mvaPhoID-RunIIFall17-v2-wp80"))
        dasPhoton.selectors |= DAS::RecPhoton::MVAWorkingPoint80;
    if (photon.photonID("mvaPhoID-RunIIFall17-v2-wp90"))
        dasPhoton.selectors |= DAS::RecPhoton::MVAWorkingPoint90;
    if (photon.passElectronVeto())
        dasPhoton.selectors |= DAS::RecPhoton::ConversionSafeElectronVeto;
    if (!photon.hasPixelSeed())
        dasPhoton.selectors |= DAS::RecPhoton::PixelSeedVeto;

    // Isolation
    dasPhoton.chargedIsolation       = photon.chargedHadronIso();
    dasPhoton.neutralHadronIsolation = photon.neutralHadronIso();
    dasPhoton.photonIsolation        = photon.photonIso();
    dasPhoton.worstChargedIsolation  = photon.chargedHadronWorstVtxIso();

    return dasPhoton;
}


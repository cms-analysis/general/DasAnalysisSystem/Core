#include <cmath>
#include <iostream>

#include <TH1.h>
#include <TH2.h>

#include "Core/Uncertainties/interface/Teddy.h"

#include <Eigen/Eigenvalues>

#include <boost/exception/all.hpp>

using namespace std;
using namespace Eigen;
using namespace DAS;

static const auto deps = numeric_limits<double>::epsilon();

VectorXd identity (const VectorXd& x)
{
    VectorXd y(x);
    return y;
}

VectorXd Teddy::H2V (const unique_ptr<TH1D>& h)
{
    if (verbose) cout << __FILE__ << ':' << __LINE__ << '\t' << __func__ << endl;
    VectorXd v(h->GetNbinsX());
    for(int i = 1; i <= h->GetNbinsX(); ++i)
        v(i-1) = h->GetBinContent(i);
    return v;
}

MatrixXd Teddy::H2M (const unique_ptr<TH2D>& h)
{
    if (verbose) cout << __FILE__ << ':' << __LINE__ << '\t' << __func__ << endl;
    int sizx = h->GetNbinsX(),
        sizy = h->GetNbinsY();
    MatrixXd mat(sizx,sizy);
    for(int j = 1; j <= sizx; ++j)
    for(int i = 1; i <= sizy; ++i)
        mat(i-1,j-1) = h->GetBinContent(j,i);
    return mat;
}

int Teddy::GuessOutDim (int nOldBins, function<VectorXd(const VectorXd &)> f)
{
    if (verbose) cout << __FILE__ << ':' << __LINE__ << '\t' << __func__ << endl;
    VectorXd In(nOldBins);
    VectorXd Out = f(In); // guessing the size from the output of the function
    return Out.size();
}

void Teddy::SanityCheck (const VectorXd& x, VectorXd& eigVals, double tolerance)
{
    int nEigv = 0, nNegEigVals = 0, nEmptyBins = 0;
    for (long k = 0; k < eigVals.size(); ++k) {
        if (eigVals(k) > -tolerance) {
            eigVals(k) = max(eigVals(k), 0.);
            ++nEigv;
        }
        else {
            ++nNegEigVals;
            cout << __FILE__ << ':' << __LINE__ << '\t' << k << ' ' << eigVals(k) << '\n';
        }

        if (x(k) == 0)
            ++nEmptyBins;
    }
    cout << flush;

    if (nNegEigVals > 0)
        BOOST_THROW_EXCEPTION( invalid_argument(Form("%d negative eigenvalues (%d empty bins in input dist)",
                                                    nNegEigVals,                nEmptyBins)) );
}

Teddy::Teddy (const VectorXd& x, const MatrixXd& Cov,
        function<VectorXd(const VectorXd &)> f, int seed) :
    // type
    vec(x),
    cov(Cov),
    // counters
    nOldBins(x.rows()),
    nNewBins(GuessOutDim(nOldBins, f)),
    N(0),
    // rotation
    eigVals(nOldBins),
    rotation(nOldBins,nOldBins),
    sigma(nOldBins),
    // result
    sum(nNewBins), // definition of the size of the output vector/distribution
    sum2(nNewBins, nNewBins), // definition of the size of the output covariance matrix/distribution
    // operations
    func(f),
    random(seed)
{
    if (verbose)
        cout << __FILE__ << ':' << __LINE__ << "\tIn Teddy constructor\n"
             << __FILE__ << ':' << __LINE__ << "\tnNewBins = " << nNewBins << '\n'
             << __FILE__ << ':' << __LINE__ << "\tnOldBins = " << nOldBins << endl;

    if (nOldBins != cov.rows())
        BOOST_THROW_EXCEPTION( invalid_argument("The size of the distribution and of "
                                                "the covariance matrix don't match") );
    if (cov.rows() != cov.cols())
        BOOST_THROW_EXCEPTION( invalid_argument("Input covariance matrix is not square") );

    sum = VectorXd::Zero(nNewBins);
    sum2 = MatrixXd::Zero(nNewBins, nNewBins);

    SelfAdjointEigenSolver<MatrixXd> es(cov);
    rotation = es.eigenvectors();

    eigVals = es.eigenvalues();
    SanityCheck(x, eigVals, 1e-10);
    if (verbose)
        cout << __FILE__ << ':' << __LINE__
             << "\tThe eigenvalues of the cov matrix are:\n" << eigVals << endl;

    sigma = eigVals.cwiseSqrt();
    if (verbose)
        cout << __FILE__ << ':' << __LINE__ << "\tSigma:\n" << sigma << '\n';
}

Teddy::Teddy (const unique_ptr<TH1D>& inputDist, const unique_ptr<TH2D>& inputCov,
        function<VectorXd(const VectorXd &)> f, int seed) :
    Teddy(H2V(inputDist), H2M(inputCov), f, seed)
{ }

void Teddy::play ()
{
    // 1) get vector in rotated basis
    VectorXd deltaP(nOldBins); // P for prime
    for (int k = 0; k < nOldBins; ++k) 
        deltaP(k) = random.Gaus(0, sigma(k));

    // 2) rotate the shift back
    VectorXd delta = rotation*deltaP;
    VectorXd x = vec + delta;

    // 3) compute whatever transformation you like on x
    VectorXd y = func(x); // dimension may change here!
    sum+= y;

    // 4) get its uncertainty
    MatrixXd square = y * y.transpose(); // tensor product
    sum2 += square;

    // 5) increment count for normalisation in Teddy::buy()
    ++N;
}

pair<VectorXd, MatrixXd> Teddy::buy () const
{
    VectorXd outputDist = sum*(1./N);

    MatrixXd avSq = sum2*(1./N);
    MatrixXd outputCov = avSq - outputDist * outputDist.transpose();

    return {outputDist, outputCov};
}

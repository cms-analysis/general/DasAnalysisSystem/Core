#! /usr/bin/env python3

import argparse
from pathlib import Path
from typing import Optional


def find_git_root(location: Path) -> Optional[Path]:
    """
    Finds the root of the git repo to which `location` belongs.
    """

    # Git doesn't discover across filesystems by default
    if location.is_mount():
        return None

    # Found it!
    if (location / '.git').is_dir():
        return location

    # Check the parent folder
    return find_git_root(location.parent)


def deduce_module_name(here: Path, git_root: Path) -> str:
    """
    Deduces the module name from the current directory.
    """

    if here == git_root:
        raise ValueError('Cannot deduce module name.')

    return here.relative_to(git_root).parts[0]


def get_template(git_root: Path, type: str) -> str:
    """
    Loads the code of a template executable.
    """

    with (git_root / 'CommonTools' / 'test' / f'template{type}.cc').open() as f:
        return f.read()


def get_executable_path(git_root: Path, module: str, exec_name: str) -> Path:
    """
    Returns the path at which the executable file will be created.
    """

    return git_root / module / 'bin' / f'{exec_name}.cc'


def patch_main_cmakelists(git_root: Path, module: str) -> None:
    """
    Adds module as a subdirectory of the main CMakeLists.txt, if needed.
    """

    cmakelists = git_root / 'CMakeLists.txt'

    # First check if the add_subdirectory is already there.
    with cmakelists.open() as f:
        if f'add_subdirectory({module}' in f.read():
            return  # Nothing to do

    # Append the new add_subdirectory line.
    with cmakelists.open('a') as f:
        f.write(f'add_subdirectory({module})\n')
    print('Patched CMakeLists.txt')


def patch_module_cmakelists(git_root: Path, module: str, exec_name: str) -> None:
    """
    Adds the executable to the module CMakeLists.txt, creating it if needed.
    """

    cmakelists = git_root / module / 'CMakeLists.txt'

    # First check if the executable is already there.
    action = 'Created'
    if cmakelists.exists():
        action = 'Patched'
        with cmakelists.open() as f:
            if f'core_add_executable({exec_name}' in f.read():
                return  # Nothing to do

    # Create parent folders if needed.
    cmakelists.parent.mkdir(parents=True, exist_ok=True)

    # Append the new core_add_executable line.
    with cmakelists.open('a') as f:
        f.write(f'core_add_executable({exec_name})\n')
    print(f'{action} {module}/CMakeLists.txt')


def create_executable(location: Path, code: str) -> None:
    """
    Creates the executable file.
    """

    # Create parent folders if needed.
    location.parent.mkdir(parents=True, exist_ok=True)
    with location.open('w') as f:
        f.write(code)


if __name__ == '__main__':
    RED = '\033[0;31m'
    GREEN = '\033[0;32m'
    RESET = '\033[0m'

    parser = argparse.ArgumentParser(description='Creates a template executable')
    parser.add_argument('type', metavar='type', choices=('EventLoop', 'SimpleExec'),
                        help='EventLoop or SimpleExec')
    parser.add_argument('module', nargs='?', default=None, type=str,
                        help='Module to create the executable in')
    parser.add_argument('newname',
                        help='Name of the new executable')
    args = parser.parse_args()

    try:
        # Checks
        here = Path().resolve()
        git_root = find_git_root(here)
        if not (git_root / 'CMakeLists.txt').is_file():
            raise ValueError('Cannot find the main CMakeLists.txt')

        module = args.module or deduce_module_name(here, git_root)
        exec_path = get_executable_path(git_root, module, args.newname)
        if exec_path.exists():
            raise ValueError(f'File {exec_path.relative_to(git_root)} already exists')

        # Do it!
        template = get_template(git_root, args.type)

        patch_main_cmakelists(git_root, module)
        patch_module_cmakelists(git_root, module, args.newname)
        create_executable(exec_path, template.replace(args.type, args.newname))

        print(f'Created {exec_path.relative_to(git_root)}')
        print(f"{GREEN}Don't forget to compile and install to be able to run the executable{RESET}")
    except Exception as e:
        print(f'{RED}Error: {e}{RESET}')

// SPDX-License-Identifier: GPLv3-or-later
//
// SPDX-FileCopyrightText: Louis Moureaux <louis.moureaux@cern.ch>

#pragma once

#include <Options.h>

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// Constructs Darwin options with the correct commit information.
Darwin::Tools::Options Options(const char *, int = Darwin::Tools::none);

} // namespace DAS

# SPDX-License-Identifier: GPLv3-or-later
#
# SPDX-FileCopyrightText: Louis Moureaux <louis.moureaux@cern.ch>

core_add_library(
    SOURCES ControlPlots.cc DASOptions.cc
    DEPENDS Darwin::Darwin
    TESTS   templateEventLoop templateSimpleExec testGenericSFApplier
)
install(PROGRAMS scripts/template DESTINATION "${CMAKE_INSTALL_BINDIR}")

# Generate the version header with the current git hash
# This is a bit complicated because we need to run git at build time, and this
# file is executed at configure time. So we need to add a custom target to the
# build system. This is done below using the helper script CreateVersionHeader
# found in the cmake/ directory.
set(version_header "${CMAKE_CURRENT_BINARY_DIR}/version.h")
add_custom_target(CreateVersionHeader
                  COMMENT "Generating version.h"
                  COMMAND "${CMAKE_COMMAND}"
                          -D GIT_EXECUTABLE="${GIT_EXECUTABLE}"
                          -D OUTPUT_PATH="${version_header}"
                          -D MACRO_NAME=DAS_COMMIT_HASH
                          -P "${CMAKE_SOURCE_DIR}/cmake/CreateVersionHeader.cmake"
                  WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
                  BYPRODUCTS "${version_header}")
# Let CommonTools know about the header
target_sources(CommonTools PRIVATE "${version_header}")
set_source_files_properties(
    src/DASOptions.cc PROPERTIES
    INCLUDE_DIRECTORIES "${CMAKE_CURRENT_BINARY_DIR}")

# Documentation
if (DOXYGEN_FOUND)
    include(doc/DoxygenSettings.cmake)
    # TODO This assumes that we've been installed through the Installer, which
    #      is not very elegant.
    doxygen_add_docs(doxygen "${CMAKE_SOURCE_DIR}/..")
endif()

# mergeNtuples
core_add_executable(mergeNtuples)

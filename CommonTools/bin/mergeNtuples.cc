#include <cstdlib>
#include <stdexcept>
#include <iostream>
#include <filesystem>
#include <string>
#include <optional>
#include <memory>
#include <algorithm>

#include "Core/CommonTools/interface/DASOptions.h"

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Lepton.h"
#include "Core/Objects/interface/Photon.h"

#include <TFile.h>
#include <TChain.h>
#include <TH1.h>

#include "Core/JetVetoMaps/interface/Conservative.h"
#include "Core/MET/interface/Filters.h"

#include <darwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// Get fractioned *n*-tuples after the n-tuplisation and merge them into a
/// single file. Complementary information about the pile-up may be included.
/// A minimal selection is applied, e.g. on the primary vertex (PV).
void mergeNtuples
                (const vector<fs::path>& inputs, //!< input ROOT file (n-tuple)
                 const fs::path& output, //!< output ROOT file (n-tuple)
                 const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
                 const int steering, //!< parameters obtained from explicit options
                 const DT::Slice slice = {1,0} //!< number and index of slice
                )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice, "ntupliser/events");
    auto tOut = flow.GetOutputTree(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    bool isMC = metainfo.Get<bool>("flags", "isMC");
    int year = metainfo.Get<int>("flags", "year");

    // event information
    auto recEvt = flow.GetBranchReadWrite<RecEvent>("recEvent");
    auto vtx = flow.GetBranchReadWrite<PrimaryVertex>("primaryvertex");

    // physics object information
    auto recJets = flow.GetBranchReadWrite<vector<RecJet>>("recJets", DT::facultative);
    auto recMuons = flow.GetBranchReadWrite<vector<RecMuon>>("recMuons", DT::facultative);
    auto recPhotons = flow.GetBranchReadWrite<vector<RecPhoton>>("recPhotons", DT::facultative);

    // jet veto business
    const auto& jetveto_file = config.get<fs::path>("corrections.jetvetomap.filename");
    optional<JetVeto::Conservative> jetveto;
    if (jetveto_file != "/dev/null")
        jetveto = make_optional<JetVeto::Conservative>(jetveto_file);
    else
        cout << orange << "No jet veto file was provided." << def << endl;
    metainfo.Set<fs::path>("corrections", "jetvetomap", "filename", jetveto_file);

    // MET filter business
    auto met = flow.GetBranchReadOnly<MET>("met");
    fs::path p_METfilters = config.get<fs::path>("corrections.METfilters");
    optional<MissingET::Filters> metfilters;
    if (p_METfilters != "/dev/null") {
        if (!metainfo.Find("corrections", "METfilters"))
            BOOST_THROW_EXCEPTION( DE::BadInput("No MET filters in this tree", tOut) );
        if (metainfo.Get<fs::path>("corrections", "METfilters") != p_METfilters) {
            cerr << orange << "You are now using a different list of MET filters w.r.t. n-tuplisation.\n" << def;
            metainfo.Set<bool>("git", "reproducible", false);
        }
        metfilters = make_optional<MissingET::Filters>(p_METfilters);
    }
    else
        cout << orange << "No MET filters will be applied." << def << endl;

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        const bool badPV = abs(vtx->z) > 24 /* cm */ || abs(vtx->Rho) > 2 /* cm */ || vtx->fake;
        if (badPV) {
            recEvt->weights *= 0;
            if (recJets) recJets->clear();
            if (recMuons) recMuons->clear();
            if (recPhotons) recPhotons->clear();
        }

        if (recJets && jetveto)
            for (auto& recJet: *recJets)
                (*jetveto)(recJet);

        if (metfilters) (*metfilters)(*met, *recEvt);

        if (steering & DT::fill) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of DAS namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options(
                            "Get fractioned *n*-tuples after the n-tuplisation and merge them into a "
                            "single file. Complementary information about the pile-up may be included. "
                            "A minimal selection is applied, e.g. on the primary vertex (PV).",
                            DT::config | DT::split | DT::Friend);

        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file"               )
               .arg<fs::path>("jetvetomap", "corrections.jetvetomap.filename", "ROOT file containing jet vet maps"
                                                                            "(`/dev/null` to deactivate)")
               .arg<fs::path>("METfilters", "corrections.METfilters", "2-column file containing the MET filters"
                                                                            "(`/dev/null` to deactivate)");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::mergeNtuples(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif /* DOXYGEN_SHOULD_SKIP_THIS */

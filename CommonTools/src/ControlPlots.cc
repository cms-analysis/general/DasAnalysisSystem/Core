#include "Core/CommonTools/interface/binnings.h"
#include "Core/CommonTools/interface/ControlPlots.h"
#include "Core/Objects/interface/Di.h"

using namespace DAS;
using namespace std;

bool ControlPlots::isMC = false;
bool ControlPlots::verbose = false;

ControlPlots::ControlPlots (TString Name) :
    name(Name)
{
    if (verbose)
        cout << __func__ << '\t' << Name << endl;
    recpt = TH1F(Name + "recpt"  , ";p_{T};N^{j}_{eff}", nPtBins, pt_edges.data());
    recpt_y = TH2F(Name + "recpt_y", ";p_{T};y;N^{j}_{eff}", nPtBins, pt_edges.data(), nYbins, y_edges.data());
    recMjj_y = TH2F(Name + "recMjj_y", ";M_{jj};y;N^{j}_{eff}", nMjjBins, Mjj_edges.data(), nYbins, y_edges.data());
    recMjj_ysyb = TH3F(Name + "recMjj_ysyb", ";M_{jj};y_{b};y^{*}", nMjjBins, Mjj_edges.data(), nYbins, y_edges.data(), nYbins, y_edges.data());
    recptav_ysyb = TH3F(Name + "recptav_ysyb", ";p_{T}^{av};y_{b};y^{*}", nPtDijetBins, pt_av_edges.data(), nYbins, y_edges.data(), nYbins, y_edges.data());
    recpt0_N = TH2F(Name + "recpt0_N", ";p^{leading}_{T};N_{jets};N^{j}_{eff}", nPtBins, pt_edges.data(), maxMult, n_edges.data());
    recpt_n = TH2F(Name + "recpt_n", ";p_{T};n^{th} jet;N^{j}_{eff}", nPtBins, pt_edges.data(), maxMult, n_edges.data());
    if (!isMC) return;
    genpt = TH1F(Name + "genpt"  , ";p_{T};N^{j}_{eff}", nPtBins, pt_edges.data());
    genpt_y = TH2F(Name + "genpt_y", ";p_{T};y;N^{j}_{eff}", nPtBins, pt_edges.data(), nYbins, y_edges.data());
    genMjj_y = TH2F(Name + "genMjj_y", ";M_{jj};y;N^{j}_{eff}", nMjjBins, Mjj_edges.data(), nYbins, y_edges.data());
    genMjj_ysyb = TH3F(Name + "genMjj_ysyb", ";M_{jj};y_{b};y^{*}", nMjjBins, Mjj_edges.data(), nYbins, y_edges.data(), nYbins, y_edges.data());
    genptav_ysyb = TH3F(Name + "genptav_ysyb", ";p_{T}^{av};y_{b};y^{*}", nPtDijetBins, pt_av_edges.data(), nYbins, y_edges.data(), nYbins, y_edges.data());
    genpt0_N = TH2F(Name + "genpt0_N", ";p^{leading}_{T};N_{jets};N^{j}_{eff}", nPtBins, pt_edges.data(), maxMult, n_edges.data());
    genpt_n = TH2F(Name + "genpt_n", ";p_{T};n^{th} jet;N^{j}_{eff}", nPtBins, pt_edges.data(), maxMult, n_edges.data());
}

void ControlPlots::operator() (const vector<GenJet>& genjets, const double& evW, size_t iWgt)
{
    assert(isMC);

    // inclusive jet
    for (const auto& genjet: genjets) {
        genpt  ->Fill(genjet.p4.Pt(),                  evW * genjet.weights.at(iWgt));
        genpt_y->Fill(genjet.p4.Pt(), genjet.AbsRap(), evW * genjet.weights.at(iWgt));
    }

    auto inYacceptance = [](const auto& genjet) { return genjet.AbsRap() < ymax; };
    size_t N = count_if(begin(genjets), end(genjets), inYacceptance);

    if (N > 0) {
        auto genjet0 = find_if(begin(genjets), end(genjets), inYacceptance);
        genpt0_N->Fill(genjet0->p4.Pt(), N, evW * genjet0->weights.front());
    }
    
    for (size_t i = 0; i < genjets.size(); ++i) {
        const auto& genjet = genjets.at(i);
        if (genjet.AbsRap() >= ymax) continue;
        genpt_n->Fill(genjet.p4.Pt(), i+1, evW * genjet.weights.front());
    }

    // dijet
    if (genjets.size() < 2) return;
    const auto& j0 = genjets.at(0),
                j1 = genjets.at(1);
    if ( j0.p4.Pt() < 100. || j1.p4.Pt() < 50. ) return;
    auto dijet = j0 + j1;
    auto ymax = max(j0.AbsRap(), j1.AbsRap());
    auto djW = j0.weights.at(iWgt) * j1.weights.at(iWgt);
    genMjj_y->Fill(dijet.CorrP4().M(), ymax, evW * djW);

    auto Y0 = j0.Rapidity(),
         Y1 = j1.Rapidity();
    auto ystar  = 0.5*abs(Y0-Y1),
         yboost = 0.5*abs(Y0+Y1);
    genMjj_ysyb->Fill(dijet.CorrP4().M(), yboost, ystar, evW * djW);
    auto ptav = 0.5*(j0.p4.Pt() + j1.p4.Pt());
    genptav_ysyb->Fill(ptav, yboost, ystar, evW * djW);

}

void ControlPlots::operator() (const vector<RecJet>& recjets, const double& evW, size_t iJEC, size_t iWgt)
{
    // inclusive jet
    for (const auto& recjet: recjets) {
        recpt  ->Fill(recjet.CorrPt(iJEC),                  evW * recjet.weights.at(iWgt));
        recpt_y->Fill(recjet.CorrPt(iJEC), recjet.AbsRap(), evW * recjet.weights.at(iWgt));
    }

    auto inYacceptance = [](const auto& recjet) { return recjet.AbsRap() < ymax; };
    size_t N = count_if(begin(recjets), end(recjets), inYacceptance);

    if (N > 0) {
        auto recjet0 = find_if(begin(recjets), end(recjets), inYacceptance);
        recpt0_N->Fill(recjet0->p4.Pt(), N, evW * recjet0->weights.front());
    }


    for (size_t i = 0; i < recjets.size(); ++i) {
        const auto& recjet = recjets.at(i);
        if (recjet.AbsRap() >= ymax) continue;
        recpt_n->Fill(recjet.p4.Pt(), i+1, evW * recjet.weights.front());
    }
    // dijet
    if (recjets.size() < 2) return;
    const auto& j0 = recjets.at(0),
                j1 = recjets.at(1);
    if ( j0.CorrPt(iJEC) < 100. || j1.CorrPt(iJEC) < 50. ) return;
    auto ymax = max(j0.AbsRap(), j1.AbsRap());
    auto djW = j0.weights.at(iWgt) * j1.weights.at(iWgt);
    auto dijet = j0 + j1;
    recMjj_y->Fill(dijet.CorrP4().M(), ymax, evW * djW);

    auto Y0 = j0.Rapidity(),
         Y1 = j1.Rapidity();
    auto ystar  = 0.5*abs(Y0-Y1),
         yboost = 0.5*abs(Y0+Y1);
    recMjj_ysyb->Fill(dijet.CorrP4().M(), yboost, ystar, evW * djW);
    auto ptav = dijet.HT();
    recptav_ysyb->Fill(ptav, yboost, ystar, evW * djW);
}

void ControlPlots::Write (TDirectory * D)
{
    assert(D);
    auto d = D->mkdir(name);
    d->cd();
    vector<TH1*> hs {&*recpt, dynamic_cast<TH1*>(&*recpt_y), dynamic_cast<TH1*>(&*recMjj_y), dynamic_cast<TH1*>(&*recpt0_N), dynamic_cast<TH1*>(&*recpt_n), dynamic_cast<TH1*>(&*recMjj_ysyb), dynamic_cast<TH1*>(&*recptav_ysyb)};
    if (isMC) {
        vector<TH1*> hs2 {&*genpt, dynamic_cast<TH1*>(&*genpt_y), dynamic_cast<TH1*>(&*genMjj_y), dynamic_cast<TH1*>(&*genpt0_N), dynamic_cast<TH1*>(&*genpt_n), dynamic_cast<TH1*>(&*genMjj_ysyb), dynamic_cast<TH1*>(&*genptav_ysyb)};
        hs.insert(hs.end(), hs2.begin(), hs2.end());
    }
    for (auto& h: hs) {
        TString n = h->GetName();
        n.ReplaceAll(name,"");
        h->SetDirectory(d);
        h->Write(n);
    }
}

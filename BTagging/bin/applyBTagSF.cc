#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/binnings.h"
#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <TFile.h>
#include <TH3.h>

#include "Math/VectorUtil.h"

#include <darwin.h>

#include "Core/BTagging/interface/BTagCalibration.h"

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::BTagging {

////////////////////////////////////////////////////////////////////////////////
/// Calculate the jet weight from the b-tagging calibration following the BTV
/// recommendations.
struct JetWeight {

    BTagCalibrationReader loose, medium, tight; //!< object to call to get SFs

    unique_ptr<TH3> discriminant; //!< in the same convention as in `getBTagBinnedDiscriminant`

    ////////////////////////////////////////////////////////////////////////////
    /// Prepares the objects to retrieve the SFs from the SF tables and the
    /// binned discriminant.
    JetWeight (const int steering,
               const fs::path& SFtables, //!< path to the CSV file (provided by BTV)
               const fs::path& fDisc //!< path to the ROOT file (calculated with DAS)
               ) :
        loose (BTagEntry::OP_LOOSE , "central", {"up", "down"}),
        medium(BTagEntry::OP_MEDIUM, "central", {"up", "down"}),
        tight (BTagEntry::OP_TIGHT , "central", {"up", "down"})
    {
        cout << "Extracting binned discriminant from " << fDisc << endl;
        discriminant = DT::Flow(steering, {fDisc}).GetInputHist<TH3>("discriminant");
        if (discriminant->GetNbinsZ() != 4) {
            const char * what = Form("Found %d working points (expecting 4)",
                                        discriminant->GetNbinsZ());
            BOOST_THROW_EXCEPTION( DE::BadInput(what, discriminant) );
        }

        cout << "Extracting scale factors from " << SFtables << endl;
        BTagCalibration calib("DeepJet", SFtables.c_str());
        for (BTagCalibrationReader * reader: {&loose, &medium, &tight}) {
            reader->load(calib, BTagEntry::FLAV_B   , "comb");
            reader->load(calib, BTagEntry::FLAV_C   , "comb");
            reader->load(calib, BTagEntry::FLAV_UDSG, "incl");
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    /// Takes the discriminat values, true flavour, and kinematics of a jet to
    /// calculate the jet weight.
    double operator() (const RecJet& jet) /// \todo variation
    {
        int ifl; /// \todo standardise flavour indices...
        BTagEntry::JetFlavor fl;
             if (jet.nBHadrons > 0) { ifl = 5; fl = BTagEntry::FLAV_B;    }
        else if (jet.nCHadrons > 0) { ifl = 4; fl = BTagEntry::FLAV_C;    }
        else                        { ifl = 0; fl = BTagEntry::FLAV_UDSG; }

        const char * variation = "central"; /// \todo variations

        float pt = jet.CorrPt(),
              eta = abs(jet.p4.Eta());

        float disc = jet.DeepJet.B();
        int idisc = discriminant->GetZaxis()->FindBin(disc);

        if (idisc == 4) // above the tight WP
            return tight.eval_auto_bounds(variation, fl, eta, pt);

        int ipt = discriminant->GetXaxis()->FindBin(pt);
        double Nt = discriminant->GetBinContent(ipt, ifl, 4),
               Nm = discriminant->GetBinContent(ipt, ifl, 3);
        if (idisc == 3) { // above the medium WP
            double SFt = tight .eval_auto_bounds(variation, fl, eta, pt),
                   SFm = medium.eval_auto_bounds(variation, fl, eta, pt);
            return SFm + (Nt/Nm)*(SFm-SFt);
        }

        double Nl = discriminant->GetBinContent(ipt, ifl, 2);
        if (idisc == 2) { // above the loose WP
            double SFm = medium.eval_auto_bounds(variation, fl, eta, pt),
                   SFl = loose .eval_auto_bounds(variation, fl, eta, pt);
            return SFl + ((Nm+Nt)/Nl)*(SFl-SFm);
        }

        double Nn = discriminant->GetBinContent(ipt, ifl, 1),
               SFl = loose.eval_auto_bounds(variation, fl, eta, pt);
        return 1 + ((Nl+Nm+Nt)/Nn)*(1-SFl);
    }
};

////////////////////////////////////////////////////////////////////////////////
/// Apply the multi-WP calibration with fix-WPs tables.
void applyBTagSF
           (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
            const fs::path& output, //!< output ROOT file (n-tuple)
            const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
            const int steering, //!< parameters obtained from explicit options
            const DT::Slice slice = {1,0} //!< number and index of slice
            )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto tOut = flow.GetOutputTree(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    if (!isMC) BOOST_THROW_EXCEPTION( DE::BadInput("Only MC may be used as input.",
                                      make_unique<TFile>(inputs.front().c_str() )) );
    auto R = metainfo.Get<int>("flags", "R");
    if (R != 4) BOOST_THROW_EXCEPTION( DE::BadInput("Only AK4 jets may be used.",
                                       make_unique<TFile>(inputs.front().c_str() )) );

    auto recJets = flow.GetBranchReadWrite<vector<RecJet>>("recJets");

    if (steering & DT::syst)
        BOOST_THROW_EXCEPTION( logic_error("B-tagging variations not yet implemented\n") );

    auto discriminant = config.get<fs::path>("corrections.btagging.discriminant"),
         SFtables     = config.get<fs::path>("corrections.btagging.SFtables");
    JetWeight w(steering, SFtables, discriminant);

    metainfo.Set<fs::path>("corrections", "btagging", "discriminant", discriminant);
    metainfo.Set<fs::path>("corrections", "btagging", "SFtables"    , SFtables    );

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        for (auto& jet: *recJets)
            jet.weights *= w(jet);

        if (steering & DT::fill) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::BTagging namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options(
                            "Apply the multi-WP calibration with fixed-WP SFs "
                            "tables to B-tagged jets.",
                            DT::config | DT::split | DT::Friend | DT::syst);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("discriminant", "corrections.btagging.discriminant",
                              "location of binned discriminant obtained with  "
                              "`getBTagBinnedDiscriminant`")
               .arg<fs::path>("SFtables", "corrections.btagging.SFtables",
                              "location of fix-WP SF tables from BTV");
        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::BTagging::applyBTagSF(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif

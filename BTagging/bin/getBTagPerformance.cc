#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>
#include <map>

#include <TString.h>
#include <TFile.h>
#include <TH3.h>

#include "Math/VectorUtil.h"

#include "Core/CommonTools/interface/DASOptions.h"

#include <darwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::BTagging {

////////////////////////////////////////////////////////////////////////////////
/// Extract the B-tagged performance from the output of `getBTagBinnedDiscriminant`.
/// The B-tagging efficiency and mistag rates are calculated.
void getBTagPerformance
           (const vector<fs::path>& inputs, //!< input ROOT files (histograms)
            const fs::path& output, //!< output ROOT file (histograms)
            const int steering //!< parameters obtained from explicit options
            )
{
    [[ maybe_unused]]
    static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

    cout << __func__ << " start" << endl;

    auto hIn = DT::Flow(steering, inputs).GetInputHist<TH3>("discriminant");
    if (hIn->GetNbinsZ() != 4) {
        const char * what = Form("Found %d working points (expecting 4)", hIn->GetNbinsZ());
        BOOST_THROW_EXCEPTION( DE::BadInput(what, hIn) );
    }

    auto fOut = DT::GetOutputFile(output);

    map<TString, int> flavours { {"light" , 0}, {"charm" , 4}, {"bottom", 5} };
    map<TString, int> WPs { {"loose", 2}, {"medium", 3}, {"tight", 4} };
    for (const auto& WP: WPs) {

        int tagBin = WP.second;

        auto hIncl = unique_ptr<TH1>( hIn->ProjectionX("inclusive", 0, -1, tagBin, 4) );

        for (const auto& flavour: flavours) {

            TString name = WP.first + '_' + flavour.first;

            int flBin = flavour.second;

            auto h = unique_ptr<TH1>( hIn->ProjectionX(name, flBin, flBin, tagBin, 4) );
            TString title = flavour.first + " for " + WP.first + " working point";
            cout << title << endl;
            h->SetTitle(title);
            h->Divide(h.get(), hIncl.get(), 1, 1, "b");
            h->Write();
        }
    }

    /// \todo crash if under- or overflow non-empty

    cout << __func__ << " stop" << endl;
}

} // end of DAS::BTagging namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options(
                            "Extract efficiency and mistag rates of b-tagged jets "
                            "from output of `getBTagBinnedDiscriminant`.");
        options.inputs("inputs", &inputs, "input ROOT files")
               .output("output", &output, "output ROOT file")
               (argc, argv);
        const int steering = options.steering();

        DAS::BTagging::getBTagPerformance(inputs, output, steering);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif

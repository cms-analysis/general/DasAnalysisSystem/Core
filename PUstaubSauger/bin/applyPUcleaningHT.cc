#include <cstdlib>

#include <filesystem>
#include <iostream>
#include <utility>
#include <unordered_map>
#include <vector>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/binnings.h"
#include "Core/CommonTools/interface/ControlPlots.h"
#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/CommonTools/interface/toolbox.h"

#include "Core/PUstaubSauger/interface/Plots.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

#include <TH1.h>
#include <TString.h>

#include <darwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::PUstaub {

////////////////////////////////////////////////////////////////////////////////
/// Build a map from 2-column file with max gen pt and event IDs from MB sample
unordered_map<unsigned long long, float> GetMaxGenHts (const fs::path& input)
{
    unordered_map<unsigned long long, float> maxgenhts;
    if (input == "/dev/null")
        return maxgenhts;

    if (!fs::exists(input))
        BOOST_THROW_EXCEPTION(fs::filesystem_error("Missing 2-column file with max gen ht.",
                                input, make_error_code(errc::no_such_file_or_directory)));

    // use property tree to read the file
    pt::ptree prop_tree;
    read_info(input.c_str(), prop_tree);

    // but convert to unordered map for event loop
    // --> better optimised
    for (const auto& evtID_ht: prop_tree) {
        auto evtID = stoull(evtID_ht.first);
        auto ht    =        evtID_ht.second.get_value<float>();
        maxgenhts[evtID] = ht;
    }

    return maxgenhts;
}

////////////////////////////////////////////////////////////////////////////////
/// Remove PU staub (i.e. high-weight events due to PU sampling)
void applyPUcleaningHT
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuple)
         const fs::path& output, //!< output ROOT file (n-tuple)
         const pt::ptree& config, //!< config handled with `Darwin::Tools::Options`
         const int steering, //!< parameters obtained from explicit options
         const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto [fOut, tOut] = flow.GetOutput(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    if (!isMC) BOOST_THROW_EXCEPTION( DE::BadInput("Only MC may be used as input.",
                                      make_unique<TFile>(inputs.front().c_str() )) );
    auto R = metainfo.Get<int>("flags", "R");
    const auto maxDR = R/10.;

    auto fMBmaxgenht = config.get<fs::path>("corrections.PUcleaning.MBmaxgenht");
    const auto& maxgenhts = GetMaxGenHts(fMBmaxgenht);
    metainfo.Set<fs::path>("corrections", "PUcleaning", "MBmaxgenht", fMBmaxgenht);

    auto artifacts_minpt = config.get<float>("corrections.PUcleaning.artifacts_minpt");
    metainfo.Set<float>("corrections", "PUcleaning", "artifacts_minpt", artifacts_minpt);

    // branches
    auto genEvt = flow.GetBranchReadOnly<GenEvent>("genEvent");
    auto recEvt = flow.GetBranchReadWrite<RecEvent>("recEvent");
    auto pileup = flow.GetBranchReadWrite<PileUp>("pileup");
    auto genJets = flow.GetBranchReadOnly <vector<GenJet>>("genJets");
    auto recJets = flow.GetBranchReadWrite<vector<RecJet>>("recJets");

    auto pt_logw = make_unique<TH2F>("pt_logw", ";Jet p_{T}^{rec}   [GeV];log(w);N_{eff}^{j}", nPtBins, pt_edges.data(), 400, -30, 20),
         mjj_logw = make_unique<TH2F>("mjj_logw", ";m_{jj}^{rec}   [GeV];log(w);N_{eff}^{jj}", nMjjBins, Mjj_edges.data(), 400, -30, 20);

    ControlPlots::isMC = isMC;
    ControlPlots raw("raw"),
                 genveto("genveto"),
                 corrected("corrected");

    Plots PUbefore("PUbefore"),
          PUafter("PUafter"),
          PUgenveto("PUgenveto");

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        raw(*genJets, genEvt->weights.front());
        raw(*recJets, genEvt->weights.front() * recEvt->weights.front());
        PUbefore(*genJets, *recJets, *genEvt, *recEvt, *pileup);

        bool bad_rec = false;
        float ht = 0;
        for (const auto& jet : *genJets) {
            ht += jet.p4.Pt();
        }

        // 2- removal based on the leading gen pt in the pileup
        for (size_t i = 0; i < pileup->pthats.size(); ++i) {
            unsigned long long evtID = pileup->MBevents.at(i);
            auto it = maxgenhts.find(evtID);
            if (it == maxgenhts.end()) continue;
            bad_rec |= it->second > ht;
            cout << recEvt->evtNo << ' ' << evtID << ' ' << it->second << endl;
            if (bad_rec) break;
        }

        // 3- removal of reconstruction artefacts happening at excessively high pt
        if (recJets->size() > 0 && artifacts_minpt > 0) {
            const auto& recjet = recJets->begin();
            bool matched = false;
            for (const auto& genjet: *genJets)
                matched |= DeltaR(genjet.p4, recjet->p4) < maxDR;
            float maxgenpt = genJets->size() > 0 ? genJets->front().p4.Pt()
                                                 : genEvt->hard_scale;
            if (recjet->p4.Pt() > max(2*maxgenpt, artifacts_minpt))
                genEvt->weights.front() = 0;
        }

        // clean up collections
        if (bad_rec) {
            recJets->clear();
            recEvt->weights *= 0;
            genveto(*genJets, genEvt->weights.front());
            genveto(*recJets, genEvt->weights.front() * recEvt->weights.front());
            PUgenveto(*genJets, *recJets, *genEvt, *recEvt, *pileup);
        }
        pileup->pthats.clear();
        pileup->MBevents.clear();

        corrected(*genJets, genEvt->weights.front());
        corrected(*recJets, genEvt->weights.front() * recEvt->weights.front());
        PUafter(*genJets, *recJets, *genEvt, *recEvt, *pileup);

        if (steering & DT::fill) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);
    fOut->cd();
    raw.Write(fOut);
    genveto.Write(fOut);
    corrected.Write(fOut);
    PUbefore.Write(fOut);
    PUgenveto.Write(fOut);
    PUafter.Write(fOut);

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::PUstaub namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options(
                "Clean up events from anomalous pile-up in MC samples.",
                DT::config | DT::split | DT::Friend);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("MBmaxgenht", "corrections.PUcleaning.MBmaxgenht",
                                "2-column file with MB event ID and max gen ht")
               .arg<float>("artifacts_minpt", "corrections.PUcleaning.artifacts_minpt",
                            "Partily arbitrary value to cut of overweighted reconstruction "
                            "artifacts (0 to deactivate)"); /// \note 156 GeV for Pythia UL18

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::PUstaub::applyPUcleaningHT(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif

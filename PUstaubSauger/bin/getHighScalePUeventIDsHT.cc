#include <cstdlib>

#include <filesystem>
#include <iostream>
#include <vector>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/binnings.h"
#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

#include <TH1.h>
#include <TString.h>

#include "Math/VectorUtil.h"

#include <darwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// Make a 2-column list of MB event IDs with the max genht (if any).
///
/// Unlike most commands, this command should be run on the output of the
/// n-tupliser directly, and only makes sense (even though it would technically
/// work) for MB samples without pileup.
///
/// \todo include test in CI
void getHighScalePUeventIDsHT
           (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
            const fs::path& output, //!< output ROOT file (n-tuple)
            const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
            const int steering, //!< parameters obtained from explicit options
            const DT::Slice slice = {1,0} //!< number and index of slice
            )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice, "ntupliser/events");

    auto genEvt = flow.GetBranchReadOnly<GenEvent>("genEvent");
    auto recEvt = flow.GetBranchReadOnly<RecEvent>("recEvent");
    auto genJets = flow.GetBranchReadOnly<vector<GenJet>>("genJets");

    const auto minht = config.get<float>("corrections.PUstaub.minht");

    unordered_map<unsigned long long, float> hts;

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        if (genJets->size() == 0) continue;

        float ht = 0;
        for (const auto& jet : *genJets) {
            ht += jet.p4.Pt();
        }

        if (ht > minht)
            hts[recEvt->evtNo] = ht;

    }

    pt::ptree prop_tree;
    for (const auto& evtID_ht: hts) {
	    auto evtID = Form("%llu", evtID_ht.first);
	    auto ht    =              evtID_ht.second;
	    prop_tree.put<float>(evtID, ht);
    }
    write_info(output.c_str(), prop_tree);

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options(
                            "Get events with too large genht from pileup "
                            "as a function of the event ID. This command "
                            "should be run on the output of the n-tupliser "
                            "directly" , DT::split | DT::config);
        options.inputs("inputs", &inputs, "output from n-tupliser (expecting MB sample)")
               .output("output", &output, "2-column output text file")
               .arg<int>("minht", "corrections.PUcleaning.minht", "minimum ht to consider event");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::getHighScalePUeventIDsHT(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif

#include <iostream>
#include <string>
#include <system_error>
#include <vector>

#include "Core/CommonTools/interface/binnings.h"
#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include <boost/property_tree/info_parser.hpp>

#include <darwin.h>

namespace DAS::MissingET {

////////////////////////////////////////////////////////////////////////////////
/// MET / noise event filters
///
/// More info at https://indico.cern.ch/event/1305273/#12-status-update-from-hamburg
struct Filters {

    std::vector<std::string> names; //!< full name of MET filter
    std::vector<size_t> bitsToTest; //!< indices of the bits to test in DAS::MET::Bit

    Filters (const std::filesystem::path& file //!< expecting a 2-column file (see $DARWIN_TABLES/MET)
            )
    {
        using namespace std;
        namespace pt = boost::property_tree;
        namespace fs = filesystem;

        if (!fs::exists(file))
            BOOST_THROW_EXCEPTION(fs::filesystem_error("Bad MET filter list", file,
                                  make_error_code(errc::no_such_file_or_directory)));

        pt::ptree tree;
        pt::read_info(file, tree);

        cout << "MET / noise filters:";
        size_t ibit = 0;
        for (auto& [name, child]: tree) {
            auto bit = child.get_value<bool>();

            // printout
            cout << (bit ? green : red) << ' ' << name;

            // save
            names.push_back(name);
            if (bit) bitsToTest.push_back(ibit);
            ++ibit;
        }
        cout << def << endl;
    }

    void operator() (const MET& met, RecEvent& event)
    {
        if (met.Bit.size() != names.size())
            BOOST_THROW_EXCEPTION( std::runtime_error("Inconsistent lengths of MET bits between file and event") );

        for (size_t ibit: bitsToTest) {
            if (met.Bit.at(ibit)) continue;
            event.weights *= 0;
            return;
        }
    }
};

}

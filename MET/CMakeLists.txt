# SPDX-License-Identifier: GPLv3-or-later
#
# SPDX-FileCopyrightText: Louis Moureaux <louis.moureaux@cern.ch>

core_add_executable(applyMETfilters NO_EXAMPLE LIBRARIES CommonTools)
core_add_executable(getMETfraction)

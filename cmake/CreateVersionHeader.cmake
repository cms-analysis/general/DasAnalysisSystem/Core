# SPDX-License-Identifier: GPLv3-or-later
#
# SPDX-FileCopyrightText: Louis Moureaux <louis.moureaux@cern.ch>

#[===[
CreateVersionHeader
===================

This script is run by CMake at build time to create or update the version
header. The following variables are provided by the caller:
* GIT_EXECUTABLE - the path to git
* OUTPUT_PATH - where to write the header
* MACRO_NAME - the name of the macro containing the commit hash

The OUTPUT_PATH is a header file that defines the requested macro to the
current commit hash (hence the need to run at build time). It is only updated
when the hash changes.
#]===]

# Get the commit hash
execute_process(COMMAND "${GIT_EXECUTABLE}" log -n 1 --pretty=format:%H
                OUTPUT_VARIABLE hash OUTPUT_STRIP_TRAILING_WHITESPACE)

# Generate the header contents
set(content "#define ${MACRO_NAME} \"${hash}\"\n")

# Read the current contents the output file
if (EXISTS "${OUTPUT_PATH}")
    file(READ "${OUTPUT_PATH}" current_content)
else()
    set(current_content "")
endif()

# Write the new file if it differs
if (NOT "${content}" STREQUAL "${current_content}")
    file(WRITE "${OUTPUT_PATH}" "${content}")
endif()

# Normalisation

The executables defined in the current subpackages can be used to merge and normalise the samples, right after the *n*-tuple creation,
The procedure is different for data and simulation.

# MC

Since the generation can be done in slices (e.g. MadGraph) or according to a weighted
(more or less) flat distribution, one first needs to normalise the sample to
unity, and then to normalise it to a given cross section.

Both operations are performed by using `getSumWeights` and `applyMClumi` in a row.

# Data

Here, the samples are recorded at different rates (rescales with different
prescales).
For each trigger, we have two possibilities:
 - normalise using prescales and total luminosity (`applyDataPrescales`);
 - normalise using effective luminosities (`applyDataLumi`).
